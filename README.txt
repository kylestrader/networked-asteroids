    ______           __  __       ____       ____                        ______                          
   / ____/___ ______/ /_/ /_     / __ \___  / __/__  ____  ________     / ____/___  _____________  _     
  / __/ / __ `/ ___/ __/ __ \   / / / / _ \/ /_/ _ \/ __ \/ ___/ _ \   / /_  / __ \/ ___/ ___/ _ \(_)    
 / /___/ /_/ / /  / /_/ / / /  / /_/ /  __/ __/  __/ / / (__  )  __/  / __/ / /_/ / /  / /__/  __/       
/_____/\__,_/_/   \__/_/ /_/  /_____/\___/_/  \___/_/ /_/____/\___/  /_/    \____/_/   \___/\___(_)      
   ______      ____   ______         ___                           
  / ____/___ _/ / /  /_  __/___     /   |  _________ ___  _____    
 / /   / __ `/ / /    / / / __ \   / /| | / ___/ __ `__ \/ ___/    
/ /___/ /_/ / / /    / / / /_/ /  / ___ |/ /  / / / / / (__  )     
\____/\__,_/_/_/    /_/  \____/  /_/  |_/_/  /_/ /_/ /_/____/      
                                                                
                                                                                                      														   
                                powered by
   ______      __                    ______            _          
  / ____/___ _/ /___ __  ____  __   / ____/___  ____ _(_)___  ___ 
 / / __/ __ `/ / __ `/ |/_/ / / /  / __/ / __ \/ __ `/ / __ \/ _ \
/ /_/ / /_/ / / /_/ />  </ /_/ /  / /___/ / / / /_/ / / / / /  __/
\____/\__,_/_/\__,_/_/|_|\__, /  /_____/_/ /_/\__, /_/_/ /_/\___/ 
                        /____/               /____/               
************************ Shoot for the Stars ************************

============================================================
||                   Vincent Loignon                      ||
||                     Kyle Strader                       ||
============================================================

============================================================
||                    Repository URL                      ||
============================================================
https://bitbucket.org/zylozs/egp405_finalproj_01_loignon_strader

============================================================
||                    Game Description                    ||
============================================================
Asteroids is an arcade space shooter released in November 1979[1] by Atari, Inc. and designed by Lyle Rains, Ed Logg, and Dominic Walsh. The player controls a spaceship in an asteroid field which is periodically traversed by flying saucers. The object of the game is to shoot and destroy asteroids and saucers while not colliding with either or being hit by the saucers' counter-fire. The game becomes harder as the number of asteroids increases.

- Wikipedia
https://en.wikipedia.org/wiki/Asteroids_(video_game)

Earth Defense Force: Call To Arms is an interesting twist on the traditional asteroids game. It brings in
an element from the popular twin stick shooter genre where the players moves vertically and horizontally relative to the screen while aiming independently of which direction the player is moving. The game is also completely networked which allows players to play from totally different locations around the world. Earth Defense Force: Call To Arms uses an advanced physics framework that is completely networked to other players in the servers game session, giving the players a top of the line asteroids experience that easily surpasses the games predecessor, Asteroids.

============================================================
||                      How to Play                       ||
============================================================
I.   Creating a game
	a. From command line as a server
		1. Run server <ServerPort>
		2. Wait for 2 clients to join. Game will start automatically when both players have connected.
	b. From command line as a client.
		1. Run client <ClientPort> <ServerAddress> <ServerPort>
		2. Assuming info was correct, it will automatically join the server and wait for the 
		   other player (if you are first). Otherwise it will start immediately.
	c. From console as a server
		1. Provide a server port or hit enter to use the default.
		2. Wait for clients to join. Game will start automatically
		   when both players have connected.
	d. From console as a client
		1. Provide a server address or hit enter to use the default.
		2. Provide a server port or hit enter to use the default.
		3. Provide a client port or hit enter to use the default.
		4. Assuming info was correct, it will automatically join the server and wait for the other 
		   player (if you are first). Otherwise it will start immediately.
II.  Gameplay
	1. Press W to move up, S to move down, A to move left, and D to move right. 
	2. Aim your phaser cannons with the mouse
	2. Shoot your phaser cannons with left mouse button (LMB).
III.  Winning and Losing
	1. When both players die the game automatically ends. The player with the most points will 
	   win the game, although the real objective is to survive as long as you can with your partner. 
		
============================================================
||                        Controls                        ||
============================================================
Clients Only:
W - Move Up
S - Move Down
A - Move Left
D - Move Right
Mouse - Aim
LMB - Shoot Phaser Cannon
Insert - Toggle Box2D debug drawing

Server Only:
LMB - Shoot Player 1 phaser blasts (origin is center of screen)
RMB - Shoot Player 2 phaser blasts (origin is center of screen)
R - Destroy random asteroid
T - Destroy all asteroids
S - Start next level
E - Kill all players
		
============================================================
||                     Unique Features                    ||
============================================================
- Dedicated server that allows for players to disconnect and reconnect on the fly
- Instance based ownership of networked game objects with a guaranteed unique network IDs
- Simplified, easily accessible Network Interface
- Several server cheats for demonstration purposes
- Event Driven network messages and game play through a powerful event system
- Custom engine made by us
- Platform independent timer system
- Box2D integration
- Global Texture Lookup
- Networked physics objects which can easily be created with no limit
- Dead reckoning networking
- Unique art style
- Highly abstracted engine setup to allow large amounts of customization on a per game basis
- Everything, including the engine is statically linked (OpenAL isn't because we can't)