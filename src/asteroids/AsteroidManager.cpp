#include "AsteroidManager.h"
#include "galaxyengine/NetworkedGameObject.h"
#include "GameObjectFactory.h"
#include "galaxyengine/EventDispatcher.h"
#include "DestroyAsteroidEvent.h"
#include "galaxyengine/Utils.h"
#include "BreakableAsteroid.h"

AsteroidManager::AsteroidManager(int screenWidth, int screenHeight, int numSmall, int numMedium)
{
    m_ScreenWidth = screenWidth;
    m_ScreenHeight = screenHeight;
    m_NumSmall = numSmall;
    m_NumMedium = numMedium;
}

AsteroidManager::~AsteroidManager()
{

}

void AsteroidManager::AddAsteroid(network_id id, std::shared_ptr<NetworkedGameObject> asteroid)
{
    m_Asteroids[id] = std::dynamic_pointer_cast<BreakableAsteroid>(asteroid);
}

void AsteroidManager::RemoveAsteroid(network_id id)
{
    if (IsAsteroid(id))
    {
        m_Asteroids[id]->SetRemoveFlag(true);
        m_RemoveList.push_back(id);
    }
}

void AsteroidManager::DestroyAsteroid(network_id id)
{
    if (IsAsteroid(id))
    {
        int type = m_Asteroids[id]->GetObjectType();

        // Is it small?
        if (type > (int)GameObjectTypes::ASTEROID_SMALL && type < (int)GameObjectTypes::ASTEROID_MEDIUM)
        {
            // Just delete it
            g_EventDispatcher->DispatchEvent(new DestroyAsteroidEvent(DestroyAsteroidEvent::DESTROY_ASTEROID_SMALL, id));
        }
        // Is it medium?
        else if (type > (int)GameObjectTypes::ASTEROID_MEDIUM && type < (int)GameObjectTypes::ASTEROID_LARGE)
        {
            // Break it into small asteroids
            g_EventDispatcher->DispatchEvent(new DestroyAsteroidEvent(DestroyAsteroidEvent::DESTROY_ASTEROID_MEDIUM, id, m_NumSmall));
        }
        else if (type > (int)GameObjectTypes::ASTEROID_LARGE && type < (int)GameObjectTypes::ASTEROID_MAX)
        {
            // Break it into medium asteroids
            g_EventDispatcher->DispatchEvent(new DestroyAsteroidEvent(DestroyAsteroidEvent::DESTROY_ASTEROID_LARGE, id, m_NumMedium));
        }
    }
}

AsteroidManager::network_id AsteroidManager::DestroyRandomAsteroid()
{
    std::vector<network_id> ids;
    getNetIDs(ids);

    int index = Utils::generateNumber(0, (int)ids.size());

    DestroyAsteroid(ids[index]);

    return ids[index];
}

std::vector<AsteroidManager::network_id> AsteroidManager::DestroyAllAsteroids()
{
    std::vector<network_id> ids;
    getNetIDs(ids);

    for (size_t i = 0; i < ids.size(); i++)
    {
        DestroyAsteroid(ids[i]);
    }

    return ids;
}

std::vector<AsteroidManager::network_id> AsteroidManager::RemoveAllAsteroids()
{
    std::vector<network_id> ids;
    getNetIDs(ids);

    for (size_t i = 0; i < ids.size(); i++)
    {
        RemoveAsteroid(ids[i]);
    }

    return ids;
}

void AsteroidManager::getNetIDs(std::vector<network_id>& ids)
{
    asteroid_map::iterator it = m_Asteroids.begin();

    for (; it != m_Asteroids.end(); it++)
    {
        ids.push_back(it->first);
    }
}

bool AsteroidManager::IsAsteroid(network_id id)
{
    asteroid_map::iterator it = m_Asteroids.find(id);

    if (it != m_Asteroids.end())
        return true;
    else
        return false;
}

void AsteroidManager::Update(double deltaT)
{
    asteroid_map::iterator it = m_Asteroids.begin();
    TopBot topBot;
    LeftRight leftRight;

    for (; it != m_Asteroids.end(); it++)
    {
        if (it->second->GetRemoveFlag())
            continue;

        if (it->second->GetDestroyFlag())
        {
            DestroyAsteroid(it->first);
            continue;
        }

        if (isOutOfBounds(it->second, topBot, leftRight))
        {
            if (!it->second->GetWrappedFlag()) // !it->second->GetSpawnedFlag() // Removed this because it caused asteroids that spawned while slightly off screen to disappear
            {
                wrapAsteroid(it->second, topBot, leftRight);
            }
        }
        else
        {
            // If the asteroid was recently spawned and is no longer out of bounds, mark it as no longer new
            if (it->second->GetSpawnedFlag())
                it->second->SetSpawnedFlag(false);

            // If the asteroid was recently wrapped and is no longer out of bounds, mark it as no longer wrapped
            if (it->second->GetWrappedFlag())
                it->second->SetWrappedFlag(false);
        }
    }

    // Remove all the asteroids marked for removal
    if (m_RemoveList.size() > 0)
    {
        for (size_t i = 0; i < m_RemoveList.size(); i++)
        {
            m_Asteroids.erase(m_RemoveList[i]);
        }

        m_RemoveList.clear();
    }
}

bool AsteroidManager::isOutOfBounds(std::shared_ptr<BreakableAsteroid> asteroid, TopBot& topBot, LeftRight& leftRight)
{
    // Initialize
    topBot = TopBot::NONE;
    leftRight = LeftRight::NONE;
    sf::Vector2f pos = asteroid->GetPosition();
    sf::IntRect rect = asteroid->GetTextureRect();
    sf::Vector2f origin = asteroid->GetOrigin();

    // Check Top / Bottom
    if (pos.y < -rect.height)
        topBot = TopBot::TOP;
    else if (pos.y > m_ScreenHeight + origin.y)
        topBot = TopBot::BOTTOM;

    // Check Left / Right
    if (pos.x < -rect.width)
        leftRight = LeftRight::LEFT;
    else if (pos.x > m_ScreenWidth + origin.x)
        leftRight = LeftRight::RIGHT;

    return ((topBot == TopBot::NONE) && (leftRight == LeftRight::NONE)) ? false : true;
}

void AsteroidManager::wrapAsteroid(std::shared_ptr<BreakableAsteroid> asteroid, const TopBot& topBot, const LeftRight& leftRight)
{
    sf::Vector2f pos = asteroid->GetPosition();
    sf::IntRect rect = asteroid->GetTextureRect();
    sf::Vector2f origin = asteroid->GetOrigin();

    // Wrap to Bottom
    if (topBot == TopBot::TOP)
        pos.y += m_ScreenHeight + rect.height + origin.y;
    else if (topBot == TopBot::BOTTOM) // Wrap to Top
        pos.y -= (m_ScreenHeight + rect.height + origin.y);

    // Wrap to Right
    if (leftRight == LeftRight::LEFT)
        pos.x += m_ScreenWidth + rect.width + origin.x;
    else if (leftRight == LeftRight::RIGHT) // Wrap to Left
        pos.x -= (m_ScreenWidth + rect.width + origin.x);

    // Update the position
    asteroid->SetPosition(pos);
    //asteroid->SetWrapped(true);
}

int AsteroidManager::GetNumSmall() const
{
    return m_NumSmall;
}

int AsteroidManager::GetNumMedium() const
{
    return m_NumMedium;
}

void AsteroidManager::SetNumSmall(int value)
{
    m_NumSmall = value;
}

void AsteroidManager::SetNumMedium(int value)
{
    m_NumMedium = value;
}