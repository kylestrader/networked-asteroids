#pragma once

#include <map>
#include <memory>
#include <vector>

class NetworkedGameObject;
class BreakableAsteroid;

class AsteroidManager
{
public:
    typedef unsigned int network_id;
    typedef std::map<network_id, std::shared_ptr<BreakableAsteroid>> asteroid_map;

private:
    enum class TopBot
    {
        NONE,
        TOP,
        BOTTOM
    };

    enum class LeftRight
    {
        NONE,
        LEFT,
        RIGHT
    };

    std::vector<network_id> m_RemoveList; // The list of asteroids ready to be removed
    asteroid_map m_Asteroids;
    int m_ScreenWidth;
    int m_ScreenHeight;

    int m_NumSmall; // This is how many small asteroids to spawn when you destroy a medium asteroid
    int m_NumMedium; // This is how many medium asteroids to spawn when you destroy a large asteroid

private:
    bool isOutOfBounds(std::shared_ptr<BreakableAsteroid> asteroid, TopBot& topBot, LeftRight& leftRight);
    void wrapAsteroid(std::shared_ptr<BreakableAsteroid> asteroid, const TopBot& topBot, const LeftRight& leftRight);

    void getNetIDs(std::vector<network_id>& ids);

public:
    AsteroidManager(int screenWidth, int screenHeight, int numSmall, int numMedium);
    ~AsteroidManager();

    void AddAsteroid(network_id id, std::shared_ptr<NetworkedGameObject> asteroid);
    void RemoveAsteroid(network_id id);
    void DestroyAsteroid(network_id id);

    network_id DestroyRandomAsteroid();
    std::vector<network_id> DestroyAllAsteroids();
    std::vector<network_id> RemoveAllAsteroids();

    bool IsAsteroid(network_id id);

    int GetNumSmall() const;
    int GetNumMedium() const;

    void SetNumSmall(int value);
    void SetNumMedium(int value);

    void Update(double deltaT);
};