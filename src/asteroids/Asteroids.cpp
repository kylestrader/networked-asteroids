#include "Asteroids.h"
#include "galaxyengine/NetworkedGameObject.h"
#include "galaxyengine/Utils.h"
#include "GameObjectFactory.h"
#include "AsteroidManager.h"
#include "galaxyengine/EventDispatcher.h"
#include "DestroyAsteroidEvent.h"
#include "galaxyengine/Delegate.h"
#include "SpawnAsteroidEvent.h"
#include "LevelManager.h"
#include "BreakableAsteroid.h"
#include "ScoreUI.h"
#include "Player.h"
#include "LevelUI.h"
#include "HealthUI.h"
#include "HealthEvent.h"
#include "PlayerEvent.h"
#include "ProjectileEvent.h"
#include "Projectile.h"
#include "GameMessages.h"
#include "ScoreEvent.h"
#include "LevelEvent.h"
#include "AsteroidsTextureLoader.h"

const int Asteroids::SMALL_ASTEROID_POINTS = 100;
const int Asteroids::MEDIUM_ASTEROID_POINTS = 300;
const int Asteroids::LARGE_ASTEROID_POINTS = 1000;

Asteroids::Asteroids()
{

}

Asteroids::~Asteroids()
{
    g_EventDispatcher->RemoveEventListener(DestroyAsteroidEvent::DESTROY_ASTEROID_SMALL, m_DestroyAsteroidDelegate, false);
    g_EventDispatcher->RemoveEventListener(DestroyAsteroidEvent::DESTROY_ASTEROID_MEDIUM, m_DestroyAsteroidDelegate, false);
    g_EventDispatcher->RemoveEventListener(DestroyAsteroidEvent::DESTROY_ASTEROID_LARGE, m_DestroyAsteroidDelegate);
    g_EventDispatcher->RemoveEventListener(SpawnAsteroidEvent::SPAWN_ASTEROIDS, m_SpawnAsteroidsDelegate);
    g_EventDispatcher->RemoveEventListener(PlayerEvent::DAMAGE_PLAYER, m_PlayerDamageDelegate);
    g_EventDispatcher->RemoveEventListener(ProjectileEvent::DAMAGE_ASTEROID, m_AsteroidDamageDelegate);
    g_EventDispatcher->RemoveEventListener(LevelEvent::UPDATE_LEVEL_UI, m_UpdateLevelDelegate);
    g_EventDispatcher->RemoveEventListener(PlayerEvent::KILL_PLAYER, m_KillPlayerDelegate);
}

void Asteroids::Initialize(std::shared_ptr<sf::RenderWindow> window)
{
    Super::Initialize(window);

    if (IsServer())
    {
        m_AsteroidManager = std::make_unique<AsteroidManager>(window->getSize().x, window->getSize().y, 3, 3);
        m_LevelManager = std::make_unique<LevelManager>(4, 2, 1, 10, 6);
        GetBox2DWorld()->SetContactListener(&m_ContactListener);
    }

    m_Background.setTexture(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::GAME_BACKGROUND), true);

    m_P1ScoreUI = std::make_unique<ScoreUI>(GetFont(), 1);
    m_P2ScoreUI = std::make_unique<ScoreUI>(GetFont(), 2);
    m_LevelUI = std::make_unique<LevelUI>(GetFont());

    m_P1HealthUI = std::make_unique<HealthUI>(1, 3);
    m_P2HealthUI = std::make_unique<HealthUI>(2, 3);

    m_DestroyAsteroidDelegate = Events::Delegate::Create<Asteroids, &Asteroids::onDestroyAsteroid>(this);
    m_SpawnAsteroidsDelegate = Events::Delegate::Create<Asteroids, &Asteroids::onSpawnAsteroids>(this);
    m_PlayerDamageDelegate = Events::Delegate::Create<Asteroids, &Asteroids::onPlayerDamaged>(this);
    m_AsteroidDamageDelegate = Events::Delegate::Create<Asteroids, &Asteroids::onAsteroidDamaged>(this);
    m_UpdateLevelDelegate = Events::Delegate::Create<Asteroids, &Asteroids::onUpdateLevel>(this);
    m_KillPlayerDelegate = Events::Delegate::Create<Asteroids, &Asteroids::onKillPlayer>(this);

    g_EventDispatcher->AddEventListener(DestroyAsteroidEvent::DESTROY_ASTEROID_SMALL, m_DestroyAsteroidDelegate);
    g_EventDispatcher->AddEventListener(DestroyAsteroidEvent::DESTROY_ASTEROID_MEDIUM, m_DestroyAsteroidDelegate);
    g_EventDispatcher->AddEventListener(DestroyAsteroidEvent::DESTROY_ASTEROID_LARGE, m_DestroyAsteroidDelegate);
    g_EventDispatcher->AddEventListener(SpawnAsteroidEvent::SPAWN_ASTEROIDS, m_SpawnAsteroidsDelegate);
    g_EventDispatcher->AddEventListener(PlayerEvent::DAMAGE_PLAYER, m_PlayerDamageDelegate);
    g_EventDispatcher->AddEventListener(ProjectileEvent::DAMAGE_ASTEROID, m_AsteroidDamageDelegate);
    g_EventDispatcher->AddEventListener(LevelEvent::UPDATE_LEVEL_UI, m_UpdateLevelDelegate);
    g_EventDispatcher->AddEventListener(PlayerEvent::KILL_PLAYER, m_KillPlayerDelegate);
}

void Asteroids::Update(double deltaT)
{
    Super::Update(deltaT);

    if (IsServer())
        m_AsteroidManager->Update(deltaT);
}

void Asteroids::Draw()
{
    m_Window->draw(m_Background);

    Super::Draw();

    m_P1ScoreUI->Draw(m_Window);
    m_P2ScoreUI->Draw(m_Window);
    m_P1HealthUI->Draw(m_Window);
    m_P2HealthUI->Draw(m_Window);
    m_LevelUI->Draw(m_Window);
}

void Asteroids::Reset()
{
    Super::Reset();
}

NetworkedGame::network_id Asteroids::CreateProjectile(int type, guid owner, float x /* = 0.f */, float y /* = 0.f */, float velX /* = 0.f */, float velY /* = 0.f */)
{
    return CreateProjectile(GetNewID(), type, owner, x, y, velX, velY);
}

NetworkedGame::network_id Asteroids::CreateProjectile(int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel /* = sf::Vector2f(0.f, 0.f) */)
{
    return CreateProjectile(GetNewID(), type, owner, pos, vel);
}

NetworkedGame::network_id Asteroids::CreateProjectile(network_id id, int type, guid owner, float x /* = 0.f */, float y /* = 0.f */, float velX /* = 0.f */, float velY /* = 0.f */)
{
    std::shared_ptr<NetworkedGameObject> projectile = nullptr;

    projectile = GameObjectFactory::CreateNetworkedGameObjectByType(type, id, owner, GetBox2DWorld());
    projectile->SetPosition(x, y);
    projectile->SetVelocity(velX, velY);

    float dirX, dirY;
    Utils::normalize(velX, velY, dirX, dirY);

    float angle = atan2f(dirY, dirX);
    projectile->SetBox2DRotationRads(angle);

    SetNetworkObject(id, projectile);
    
    return id;
}

NetworkedGame::network_id Asteroids::CreateProjectile(network_id id, int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel /* = sf::Vector2f(0.f, 0.f) */)
{
    return CreateProjectile(id, type, owner, pos.x, pos.y, vel.x, vel.y);
}

NetworkedGame::network_id Asteroids::CreateAsteroid(int type, guid owner, float x /* = 0.f */, float y /* = 0.f */, float velX /* = 0.f */, float velY /* = 0.f */)
{
    return CreateAsteroid(GetNewID(), type, owner, x, y, velX, velY);
}

NetworkedGame::network_id Asteroids::CreateAsteroid(int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel /* = sf::Vector2f(0.f, 0.f) */)
{
    return CreateAsteroid(GetNewID(), type, owner, pos, vel);
}

NetworkedGame::network_id Asteroids::CreateAsteroid(network_id id, int type, guid owner, float x /* = 0.f */, float y /* = 0.f */, float velX /* = 0.f */, float velY /* = 0.f */)
{
    std::shared_ptr<BreakableAsteroid> asteroid = nullptr;

    asteroid = std::dynamic_pointer_cast<BreakableAsteroid>(GameObjectFactory::CreateNetworkedGameObjectByType(type, id, owner, GetBox2DWorld()));
    asteroid->SetPosition(x, y);
    asteroid->SetVelocity(velX, velY);
    asteroid->SetSpawnedFlag(true);

    SetNetworkObject(id, asteroid);

    if (IsServer())
        m_AsteroidManager->AddAsteroid(id, asteroid);

    return id;
}

NetworkedGame::network_id Asteroids::CreateAsteroid(network_id id, int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel /* = sf::Vector2f(0.f, 0.f) */)
{
    return CreateAsteroid(id, type, owner, pos.x, pos.y, vel.x, vel.y);
}

NetworkedGame::network_id Asteroids::CreatePlayer(int type, guid owner, float x /* = 0.f */, float y /* = 0.f */, float velX /* = 0.f */, float velY /* = 0.f */)
{
    return CreatePlayer(GetNewID(), type, owner, x, y, velX, velY);
}

NetworkedGame::network_id Asteroids::CreatePlayer(int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel /* = sf::Vector2f(0.f, 0.f) */)
{
    return CreatePlayer(GetNewID(), type, owner, pos, vel);
}

NetworkedGame::network_id Asteroids::CreatePlayer(network_id id, int type, guid owner, float x /* = 0.f */, float y /* = 0.f */, float velX /* = 0.f */, float velY /* = 0.f */)
{
    std::shared_ptr<NetworkedGameObject> player = nullptr;

    player = GameObjectFactory::CreateNetworkedGameObjectByType(type, id, owner, GetBox2DWorld());
    player->SetPosition(x, y);
    player->SetVelocity(velX, velY);

    SetNetworkObject(id, player);

    if (IsServer())
    {
        //TODO: add to some sort of player manager
    }

    return id;
}

NetworkedGame::network_id Asteroids::CreatePlayer(network_id id, int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel /* = sf::Vector2f(0.f, 0.f) */)
{
    return CreatePlayer(id, type, owner, pos.x, pos.y, vel.x, vel.y);
}

Asteroids::network_id Asteroids::DestroyRandomAsteroid()
{
    if (IsServer())
        return m_AsteroidManager->DestroyRandomAsteroid();

    return -1;
}

std::vector<Asteroids::network_id> Asteroids::DestroyAllAsteroids()
{
    if (IsServer())
    {
        return m_AsteroidManager->DestroyAllAsteroids();
    }

    return std::vector<network_id>();
}

std::vector<Asteroids::network_id> Asteroids::RemoveAllAsteroids()
{
    if (IsServer())
    {
        std::vector<network_id> ids = m_AsteroidManager->RemoveAllAsteroids();

        for (size_t i = 0; i < ids.size(); i++)
            DeleteNetworkObject(ids[i]);

        return ids;
    }

    return std::vector<network_id>();
}

void Asteroids::StartNextLevel()
{
    if (IsServer())
        m_LevelManager->StartNextLevel();
}

void Asteroids::ResetLevels()
{
    if (IsServer())
        m_LevelManager->ResetLevels();
}

void Asteroids::updateNetworkObject(network_id netId, int type, guid owner, float x, float y, float velocityX, float velocityY, float rotation, float angularVelocity, NetworkInterface::timestamp srv_timestamp, float updateInterval, bool lerp)
{
    if (IsNetIdNew(netId))
    {
        // If it is any asteroid, create the correct one
        if (type > (int)GameObjectTypes::ASTEROID_SMALL && type < (int)GameObjectTypes::ASTEROID_MAX)
        {
            CreateAsteroid(netId, type, owner);
        }

        // Handle player creation
        switch (type)
        {
        case (int)GameObjectTypes::PLAYER_1:
        case (int)GameObjectTypes::PLAYER_2:
            CreatePlayer(netId, type, owner);
            break;
        case (int)GameObjectTypes::PLAYER_1_PROJECTILE:
        case (int)GameObjectTypes::PLAYER_2_PROJECTILE:
            CreateProjectile(netId, type, owner);
            break;
        }
    }

    std::shared_ptr<NetworkedGameObject> obj = GetNetworkObject(netId);
    obj->SetSpawnedFlag(false);

    NetworkInterface::timestamp currentTime = NetworkInterface::GetTime();
    NetworkInterface::timestamp drTime = (currentTime - srv_timestamp);
    sf::Vector2f drVel(velocityX, velocityY);
    sf::Vector2f drPos(x, y);
    drPos += sf::Vector2f(Utils::b2_MetersToPixels(drVel.x * ((float)drTime / 1000.0f)) + updateInterval, Utils::b2_MetersToPixels(drVel.y * ((float)drTime / 1000.0f)));

    if (lerp)
    {
        obj->BeginLerp(drPos, (((double)drTime) / 1000.0) + updateInterval, drVel);
    }
    else
    {
        obj->SetPosition(x, y);
        obj->SetVelocity(drVel);
    }

    obj->SetBox2DRotationRads(rotation);
    obj->SetAngularVelocity(angularVelocity);
}

void Asteroids::onDeleteNetworkObject(network_id id)
{
    if (IsServer())
        m_AsteroidManager->RemoveAsteroid(id);
}

void Asteroids::onDestroyAsteroid(Events::Event* e)
{
    DestroyAsteroidEvent* ev = (DestroyAsteroidEvent*)e;

    std::shared_ptr<NetworkedGameObject> old = GetNetworkObject(ev->GetNetID());
    sf::IntRect rect = old->GetTextureRect();
    sf::Vector2f newPos;
    sf::Vector2f newVel;
    int type;
    
    if (ev->GetType() == DestroyAsteroidEvent::DESTROY_ASTEROID_MEDIUM)
    {
        type = (int)GameObjectTypes::ASTEROID_SMALL;
    }
    else if (ev->GetType() == DestroyAsteroidEvent::DESTROY_ASTEROID_LARGE)
    {
        type = (int)GameObjectTypes::ASTEROID_MEDIUM;
    }

    for (int i = 0; i < ev->GetNumAsteroids(); i++)
    {
        newPos = old->GetPosition();
        newPos.x += Utils::generateNumber(-rect.width, rect.width);
        newPos.y += Utils::generateNumber(-rect.height, rect.height);

        newVel = old->GetVelocity();
        Utils::normalize(newVel.x, newVel.y);
        newVel.x *= Utils::generateNumber(0.5f, 4.f);
        newVel.y *= Utils::generateNumber(0.5f, 4.f);

        CreateAsteroid(type, NetworkInterface::GetSessionGuid(), newPos, newVel);
    }

    if (IsServer())
        m_LevelManager->RemoveAsteroids();

    DeleteNetworkObject(ev->GetNetID());
}

void Asteroids::onSpawnAsteroids(Events::Event* e)
{
    SpawnAsteroidEvent* ev = (SpawnAsteroidEvent*)e;

    for (uint i = 0; i < ev->GetNumSmall(); i++)
    {
        spawnRandomAsteroid((int)GameObjectTypes::ASTEROID_SMALL);
    }

    for (uint i = 0; i < ev->GetNumMedium(); i++)
    {
        spawnRandomAsteroid((int)GameObjectTypes::ASTEROID_MEDIUM);
    }

    for (uint i = 0; i < ev->GetNumLarge(); i++)
    {
        spawnRandomAsteroid((int)GameObjectTypes::ASTEROID_LARGE);
    }
}

void Asteroids::onPlayerDamaged(Events::Event* e)
{
    PlayerEvent* ev = (PlayerEvent*)e;

    std::shared_ptr<NetworkedGameObject> tmp = GetNetworkObject(ev->GetBody());
    std::shared_ptr<Player> player = std::dynamic_pointer_cast<Player>(tmp);

    if (player->GetObjectType() == (int)GameObjectTypes::PLAYER_1)
    {
        g_EventDispatcher->DispatchEvent(new HealthEvent(HealthEvent::REMOVE_HEALTH, 1, 1));

        UpdatePlayerHealthMessage msg;
        msg.MsgId = (uchar)GameMessagesEx::ID_UPDATE_PLAYER_HEALTH_MESSAGE;
        msg.player = 1;
        msg.health = m_P1HealthUI->GetHealth();

        NetworkInterface::SendBroadcast(msg);

        // Kill the player if their health is 0
        if (m_P1HealthUI->GetHealth() <= 0)
        {
            g_EventDispatcher->DispatchEvent(new PlayerEvent(1));
        }
    }
    else if (player->GetObjectType() == (int)GameObjectTypes::PLAYER_2)
    {
        g_EventDispatcher->DispatchEvent(new HealthEvent(HealthEvent::REMOVE_HEALTH, 2, 1));

        UpdatePlayerHealthMessage msg;
        msg.MsgId = (uchar)GameMessagesEx::ID_UPDATE_PLAYER_HEALTH_MESSAGE;
        msg.player = 2;
        msg.health = m_P2HealthUI->GetHealth();

        NetworkInterface::SendBroadcast(msg);

        // Kill the player if their health is 0
        if (m_P2HealthUI->GetHealth() <= 0)
        {
            g_EventDispatcher->DispatchEvent(new PlayerEvent(2));
        }
    }
}

void Asteroids::onKillPlayer(Events::Event* e)
{
    PlayerEvent* ev = (PlayerEvent*)e;

    int type;

    if (ev->GetPlayer() == 1)
        type = (int)GameObjectTypes::PLAYER_1;
    else if (ev->GetPlayer() == 2)
        type = (int)GameObjectTypes::PLAYER_2;

    KillPlayerMessage msg;
    msg.MsgId = (uchar)GameMessagesEx::ID_KILL_PLAYER_MESSAGE;
    msg.player = type;

    NetworkInterface::SendBroadcast(msg);

    network_map::iterator it = GetNetworkMapBegin();
    network_map::iterator itEnd = GetNetworkMapEnd();

    for (; it != itEnd; it++)
    {
        if (it->second->GetObjectType() == type)
        {
            it->second->Kill();
            break;
        }
    }
}

void Asteroids::onAsteroidDamaged(Events::Event* e)
{
    ProjectileEvent* ev = (ProjectileEvent*)e;

    std::shared_ptr<NetworkedGameObject> tmp = GetNetworkObject(ev->GetAsteroid());
    std::shared_ptr<BreakableAsteroid> asteroid = std::dynamic_pointer_cast<BreakableAsteroid>(tmp);

    tmp = GetNetworkObject(ev->GetProjectile());
    std::shared_ptr<Projectile> projectile = std::dynamic_pointer_cast<Projectile>(tmp);

    asteroid->ApplyDamage(projectile->GetDamage());
    if (asteroid->GetHealth() <= 0)
    {
        asteroid->Destroy();

        int type = asteroid->GetObjectType();
        int player;

        if (projectile->GetObjectType() == (int)GameObjectTypes::PLAYER_1_PROJECTILE)
            player = 1;
        else if (projectile->GetObjectType() == (int)GameObjectTypes::PLAYER_2_PROJECTILE)
            player = 2;

        if (type > (int)GameObjectTypes::ASTEROID_SMALL && type < (int)GameObjectTypes::ASTEROID_MEDIUM)
        {
            g_EventDispatcher->DispatchEvent(new ScoreEvent(ScoreEvent::ADD_SCORE, player, SMALL_ASTEROID_POINTS));
        }
        else if (type >(int)GameObjectTypes::ASTEROID_MEDIUM && type < (int)GameObjectTypes::ASTEROID_LARGE)
        {
            g_EventDispatcher->DispatchEvent(new ScoreEvent(ScoreEvent::ADD_SCORE, player, MEDIUM_ASTEROID_POINTS));
        }
        else if (type >(int)GameObjectTypes::ASTEROID_LARGE && type < (int)GameObjectTypes::ASTEROID_MAX)
        {
            g_EventDispatcher->DispatchEvent(new ScoreEvent(ScoreEvent::ADD_SCORE, player, LARGE_ASTEROID_POINTS));
        }

        UpdatePlayerScoreMessage msg;
        msg.MsgId = (uchar)GameMessagesEx::ID_UPDATE_PLAYER_SCORE_MESSAGE;

        if (player == 1)
        {
            msg.player = 1;
            msg.score = m_P1ScoreUI->GetScore();
        }
        else if (player == 2)
        {
            msg.player = 2;
            msg.score = m_P2ScoreUI->GetScore();
        }

        NetworkInterface::SendBroadcast(msg);
    }

    projectile->Kill();
}

void Asteroids::onUpdateLevel(Events::Event* e)
{
    LevelEvent* ev = (LevelEvent*)e;

    UpdateLevelMessage msg;
    msg.MsgId = (uchar)GameMessagesEx::ID_UPDATE_LEVEL_MESSAGE;
    msg.level = ev->GetLevel();

    NetworkInterface::SendBroadcast(msg);
}

void Asteroids::spawnRandomAsteroid(int type)
{
    //   TX     TY     BX     BY
    // -60.f, -60.f, 1340.f, 780.f
    const sf::IntRect spawnRect(-60, -60, 1400, 840);
    const sf::IntRect screenRect(20, 20, 1240, 680);
    int x = 0,
        y = 0,
        dirX = 0,
        dirY = 0;
    float velX = 0.f,
          velY = 0.f,
          speed = 0.f;
    sf::Vector2f dirVec;
    int profileSpeed = 0;

    getRandomPointOnPerimeter(spawnRect, x, y);

    // Pick a random point on screen and calculate a direction vector from it
    getRandomPointInRect(screenRect, dirX, dirY);
    dirVec.x = (float)(dirX - x);
    dirVec.y = (float)(dirY - y);
    Utils::normalize(dirVec.x, dirVec.y);
    
    // Pick a random "asteroid profile"
    profileSpeed = Utils::generateNumber<int>(0, 3);

    switch (profileSpeed)
    {
        // Slow
        case 0:
        {
            speed = Utils::generateNumber<float>(0.5f, 1.5f);
            break;
        }
        // Medium
        case 1:
        {
            speed = Utils::generateNumber<float>(1.5f, 3.f);
            break;
        }
        // Fast
        case 2:
        {
            speed = Utils::generateNumber<float>(3.f, 4.5f);
            break;
        }
    }

    // Pick a random velocity speed and apply it to direction vector
    velX = speed * dirVec.x;
    velY = speed * dirVec.y;

    CreateAsteroid(type, NetworkInterface::GetSessionGuid(), (float)x, (float)y, velX, velY);
}

void Asteroids::getRandomPointOnPerimeter(const sf::IntRect& rect, int& outX, int& outY)
{
    getRandomPointOnPerimeter(rect.left, rect.top, rect.left + rect.width, rect.top + rect.height, outX, outY);
}

// Credit to stack overflow answer from user: scgm
// http://stackoverflow.com/questions/9005750/generate-a-random-point-on-a-rectangles-perimeter-with-uniform-distribution
void Asteroids::getRandomPointOnPerimeter(int x1, int y1, int x2, int y2, int& outX, int& outY)
{
    int width = abs(x2 - x1);
    int height = abs(y2 - y1);
    int perimeter = (width * 2) + (height * 2);

    int dist = rand() % perimeter;

    if (dist <= width)
    {
        outX = (rand() % width) + x1;
        outY = y1;
    }
    else if (dist <= width + height)
    {
        outX = x2;
        outY = (rand() % height) + y1;
    }
    else if (dist <= (width * 2) + height)
    {
        outX = (rand() % width) + x1;
        outY = y2;
    }
    else
    {
        outX = x1;
        outY = (rand() % height) + y1;
    }
}

void Asteroids::getRandomPointInRect(const sf::IntRect& rect, int& outX, int& outY)
{
    while (true)
    {
        outX = Utils::generateNumber<int>(rect.left, rect.left + rect.width);
        outY = Utils::generateNumber<int>(rect.top, rect.top + rect.height);

        if (rect.contains(outX, outY))
            return;
    }
}

int Asteroids::GetWinner() const
{
    if (m_P1ScoreUI->GetScore() > m_P2ScoreUI->GetScore())
        return 1;
    else if (m_P1ScoreUI->GetScore() < m_P2ScoreUI->GetScore())
        return 2;
    else
        return 1;
}

int Asteroids::GetWinnerScore() const
{
    int winner = GetWinner();

    if (winner == 1)
    {
        return m_P1ScoreUI->GetScore();
    }
    else if (winner == 2)
    {
        return m_P2ScoreUI->GetScore();
    }

    return -1;
}

int Asteroids::GetLoser() const
{
    if (m_P1ScoreUI->GetScore() > m_P2ScoreUI->GetScore())
        return 2;
    else if (m_P1ScoreUI->GetScore() < m_P2ScoreUI->GetScore())
        return 1;
    else
        return 2;
}

int Asteroids::GetLoserScore() const
{
    int loser = GetLoser();

    if (loser == 1)
    {
        return m_P1ScoreUI->GetScore();
    }
    else if (loser == 2)
    {
        return m_P2ScoreUI->GetScore();
    }

    return -1;
}