#pragma once

#include "galaxyengine/NetworkedGame.h"
#include "AsteroidsContactListener.h"

class AsteroidManager;
class LevelManager;
class ScoreUI;
class LevelUI;
class HealthUI;

namespace Events
{
    class Event;
    class Delegate;
}

class Asteroids : public NetworkedGame
{
public:
    typedef NetworkedGame Super;
    typedef unsigned int uint;
    typedef unsigned char uchar;

    const static int SMALL_ASTEROID_POINTS;
    const static int MEDIUM_ASTEROID_POINTS;
    const static int LARGE_ASTEROID_POINTS;

private:
    std::unique_ptr<AsteroidManager> m_AsteroidManager;
    std::unique_ptr<LevelManager> m_LevelManager;
    std::unique_ptr<ScoreUI> m_P1ScoreUI;
    std::unique_ptr<ScoreUI> m_P2ScoreUI;
    std::unique_ptr<LevelUI> m_LevelUI;
    std::unique_ptr<HealthUI> m_P1HealthUI;
    std::unique_ptr<HealthUI> m_P2HealthUI;

    sf::Sprite m_Background;

    AsteroidsContactListener m_ContactListener;

    Events::Delegate* m_AsteroidDamageDelegate;
    Events::Delegate* m_PlayerDamageDelegate;
    Events::Delegate* m_DestroyAsteroidDelegate;
    Events::Delegate* m_SpawnAsteroidsDelegate;
    Events::Delegate* m_UpdateLevelDelegate;
    Events::Delegate* m_KillPlayerDelegate;

private:
    void onDestroyAsteroid(Events::Event* e);
    void onSpawnAsteroids(Events::Event* e);
    void onPlayerDamaged(Events::Event* e);
    void onAsteroidDamaged(Events::Event* e);
    void onUpdateLevel(Events::Event* e);
    void onKillPlayer(Events::Event* e);

    void spawnRandomAsteroid(int type);

    void getRandomPointInRect(const sf::IntRect& rect, int& outX, int& outY);
    void getRandomPointOnPerimeter(const sf::IntRect& rect, int& outX, int& outY);
    void getRandomPointOnPerimeter(int x1, int y1, int x2, int y2, int& outX, int& outY);

protected:
    virtual void onDeleteNetworkObject(network_id id) override;
    virtual void updateNetworkObject(network_id netId, int type, guid owner, float x, float y, float velocityX, float velocityY, float rotation, float angularVelocity, NetworkInterface::timestamp srv_timestamp, float updateInterval, bool lerp) override;

public:
    Asteroids();
    ~Asteroids();

    network_id DestroyRandomAsteroid(); // Deletes an asteroid and spawns the smaller ones
    std::vector<network_id> DestroyAllAsteroids(); // Deletes all asteroids, still spawns the smaller ones
    std::vector<network_id> RemoveAllAsteroids(); // Deletes all asteroids, does not spawn smaller ones

    void StartNextLevel();
    void ResetLevels();

    int GetWinner() const;
    int GetWinnerScore() const;
    int GetLoser() const;
    int GetLoserScore() const;

    network_id CreateProjectile(int type, guid owner, float x = 0.f, float y = 0.f, float velX = 0.f, float velY = 0.f);
    network_id CreateProjectile(int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel = sf::Vector2f(0.f, 0.f));
    network_id CreateProjectile(network_id id, int type, guid owner, float x = 0.f, float y = 0.f, float velX = 0.f, float velY = 0.f);
    network_id CreateProjectile(network_id id, int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel = sf::Vector2f(0.f, 0.f));

    network_id CreateAsteroid(int type, guid owner,float x = 0.f, float y = 0.f, float velX = 0.f, float velY = 0.f);
    network_id CreateAsteroid(int type, guid owner,const sf::Vector2f& pos, const sf::Vector2f& vel = sf::Vector2f(0.f, 0.f));
    network_id CreateAsteroid(network_id id, int type, guid owner, float x = 0.f, float y = 0.f, float velX = 0.f, float velY = 0.f);
    network_id CreateAsteroid(network_id id, int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel = sf::Vector2f(0.f, 0.f));

    network_id CreatePlayer(int type, guid owner, float x = 0.f, float y = 0.f, float velX = 0.f, float velY = 0.f);
    network_id CreatePlayer(int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel = sf::Vector2f(0.f, 0.f));
    network_id CreatePlayer(network_id id, int type, guid owner, float x = 0.f, float y = 0.f, float velX = 0.f, float velY = 0.f);
    network_id CreatePlayer(network_id id, int type, guid owner, const sf::Vector2f& pos, const sf::Vector2f& vel = sf::Vector2f(0.f, 0.f));

    virtual void Initialize(std::shared_ptr<sf::RenderWindow> window) override;
    virtual void Update(double deltaT) override;
    virtual void Draw() override;
    virtual void Reset() override;
};