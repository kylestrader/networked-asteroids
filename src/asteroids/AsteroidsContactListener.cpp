#include "AsteroidsContactListener.h"
#include "Box2D/Box2D.h"
#include "galaxyengine/EventDispatcher.h"
#include "GameObjectFactory.h"
#include "PlayerEvent.h"
#include "ProjectileEvent.h"

AsteroidsContactListener::AsteroidsContactListener()
{

}

AsteroidsContactListener::~AsteroidsContactListener()
{

}

void AsteroidsContactListener::BeginContact(b2Contact* contact)
{
    b2Fixture* fixtureA = contact->GetFixtureA();
    b2Fixture* fixtureB = contact->GetFixtureB();

    if (fixtureA->GetFilterData().groupIndex == (int16)CollisionGroups::ASTEROID)
    {
        if (fixtureB->GetFilterData().groupIndex == (int16)CollisionGroups::PLAYER)
        {
            printf("Collision between Player and Asteroid detected\n");
            g_EventDispatcher->DispatchEvent(new PlayerEvent(fixtureB->GetBody()));
        }
        else if (fixtureB->GetFilterData().groupIndex == (int16)CollisionGroups::PROJECTILE)
        {
            printf("Collision between Projectile and Asteroid detected\n");
            g_EventDispatcher->DispatchEvent(new ProjectileEvent(fixtureB->GetBody(), fixtureA->GetBody()));
        }
    }
    else if (fixtureB->GetFilterData().groupIndex == (int16)CollisionGroups::ASTEROID)
    {
        if (fixtureA->GetFilterData().groupIndex == (int16)CollisionGroups::PLAYER)
        {
            printf("Collision between Player and Asteroid detected\n");
            g_EventDispatcher->DispatchEvent(new PlayerEvent(fixtureA->GetBody()));
        }
        else if (fixtureA->GetFilterData().groupIndex == (int16)CollisionGroups::PROJECTILE)
        {
            printf("Collision between Projectile and Asteroid detected\n");
            g_EventDispatcher->DispatchEvent(new ProjectileEvent(fixtureA->GetBody(), fixtureB->GetBody()));
        }
    }
}