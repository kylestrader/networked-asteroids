#pragma once

#include "Box2D/Dynamics/b2WorldCallbacks.h"

class AsteroidsContactListener : public b2ContactListener
{
public:
    AsteroidsContactListener();
    ~AsteroidsContactListener();

    virtual void BeginContact(b2Contact* contact) override;
};