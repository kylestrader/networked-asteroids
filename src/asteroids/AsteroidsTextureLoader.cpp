#include "AsteroidsTextureLoader.h"

const std::string AsteroidsTextures::ASTEROID_SMALL_1 = "ASTEROID_SMALL_1";
const std::string AsteroidsTextures::ASTEROID_SMALL_2 = "ASTEROID_SMALL_2";
const std::string AsteroidsTextures::ASTEROID_SMALL_3 = "ASTEROID_SMALL_3";
const std::string AsteroidsTextures::ASTEROID_SMALL_4 = "ASTEROID_SMALL_4";
const std::string AsteroidsTextures::ASTEROID_MEDIUM_1 = "ASTEROID_MEDIUM_1";
const std::string AsteroidsTextures::ASTEROID_MEDIUM_2 = "ASTEROID_MEDIUM_2";
const std::string AsteroidsTextures::ASTEROID_MEDIUM_3 = "ASTEROID_MEDIUM_3";
const std::string AsteroidsTextures::ASTEROID_MEDIUM_4 = "ASTEROID_MEDIUM_4";
const std::string AsteroidsTextures::ASTEROID_LARGE_1 = "ASTEROID_LARGE_1";
const std::string AsteroidsTextures::ASTEROID_LARGE_2 = "ASTEROID_LARGE_2";
const std::string AsteroidsTextures::ASTEROID_LARGE_3 = "ASTEROID_LARGE_3";
const std::string AsteroidsTextures::ASTEROID_LARGE_4 = "ASTEROID_LARGE_4";
const std::string AsteroidsTextures::PLAYER_1 = "PLAYER_1";
const std::string AsteroidsTextures::PLAYER_2 = "PLAYER_2";
const std::string AsteroidsTextures::PLAYER_1_HEALTH = "PLAYER_1_HEALTH";
const std::string AsteroidsTextures::PLAYER_2_HEALTH = "PLAYER_2_HEALTH";
const std::string AsteroidsTextures::PLAYER_1_PROJECTILE = "PLAYER_1_PROJECTILE";
const std::string AsteroidsTextures::PLAYER_2_PROJECTILE = "PLAYER_2_PROJECTILE";
const std::string AsteroidsTextures::GAME_OVER_BACKGROUND = "GAME_OVER_BACKGROUND";
const std::string AsteroidsTextures::GAME_BACKGROUND = "GAME_BACKGROUND";

void AsteroidsTextureLoader::loadAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */)
{
    sf::Texture asteroid_small_1,
                asteroid_small_2,
                asteroid_small_3,
                asteroid_small_4,
                asteroid_medium_1,
                asteroid_medium_2,
                asteroid_medium_3,
                asteroid_medium_4,
                asteroid_large_1,
                asteroid_large_2,
                asteroid_large_3,
                asteroid_large_4,
                player_1,
                player_2,
                player_1_health,
                player_2_health,
                player_1_projectile,
                player_2_projectile,
                game_over_background,
                game_background;

    // Put all asset loading in this try and ALWAYS throw if it fails
    try
    {
        // Small Asteroids
        if (!asteroid_small_1.loadFromFile(assetDir + "asteroid_small_1.png", sf::IntRect(0, 0, 41, 41)))
            throw 0;
        if (!asteroid_small_2.loadFromFile(assetDir + "asteroid_small_2.png", sf::IntRect(0, 0, 41, 41)))
            throw 0;
        if (!asteroid_small_3.loadFromFile(assetDir + "asteroid_small_3.png", sf::IntRect(0, 0, 41, 41)))
            throw 0;
        if (!asteroid_small_4.loadFromFile(assetDir + "asteroid_small_4.png", sf::IntRect(0, 0, 41, 41)))
            throw 0;

        // Medium Asteroids
        if (!asteroid_medium_1.loadFromFile(assetDir + "asteroid_medium_1.png", sf::IntRect(0, 0, 68, 68)))
            throw 0;
        if (!asteroid_medium_2.loadFromFile(assetDir + "asteroid_medium_2.png", sf::IntRect(0, 0, 68, 68)))
            throw 0;
        if (!asteroid_medium_3.loadFromFile(assetDir + "asteroid_medium_3.png", sf::IntRect(0, 0, 68, 68)))
            throw 0;
        if (!asteroid_medium_4.loadFromFile(assetDir + "asteroid_medium_4.png", sf::IntRect(0, 0, 68, 68)))
            throw 0;

        // Large Asteroids
        if (!asteroid_large_1.loadFromFile(assetDir + "asteroid_large_1.png", sf::IntRect(0, 0, 105, 105)))
            throw 0;
        if (!asteroid_large_2.loadFromFile(assetDir + "asteroid_large_2.png", sf::IntRect(0, 0, 105, 105)))
            throw 0;
        if (!asteroid_large_3.loadFromFile(assetDir + "asteroid_large_3.png", sf::IntRect(0, 0, 105, 105)))
            throw 0;
        if (!asteroid_large_4.loadFromFile(assetDir + "asteroid_large_4.png", sf::IntRect(0, 0, 105, 105)))
            throw 0;

        // Players
        if (!player_1.loadFromFile(assetDir + "player_1.png", sf::IntRect(0, 0, 80, 80)))
            throw 0;
        if (!player_2.loadFromFile(assetDir + "player_2.png", sf::IntRect(0, 0, 80, 80)))
            throw 0;
        if (!player_1_health.loadFromFile(assetDir + "player_1_health.png", sf::IntRect(0, 0, 50, 50)))
            throw 0;
        if (!player_2_health.loadFromFile(assetDir + "player_2_health.png", sf::IntRect(0, 0, 50, 50)))
            throw 0;
        if (!player_1_projectile.loadFromFile(assetDir + "player_1_projectile.png", sf::IntRect(0, 0, 32, 16)))
            throw 0;
        if (!player_2_projectile.loadFromFile(assetDir + "player_2_projectile.png", sf::IntRect(0, 0, 32, 16)))
            throw 0;

        // Backgrounds
        if (!game_over_background.loadFromFile(assetDir + "gameover.png", sf::IntRect(0, 0, 1280, 720)))
            throw 0;
        if (!game_background.loadFromFile(assetDir + "background.png", sf::IntRect(0, 0, 1280, 720)))
            throw 0;
    }
    catch (...)
    {
        if (!secondAttempt)
        {
            printf("Unable to find a texture, trying another directory...\n");
            loadAssets("../content/", true);
            return;
        }
        else
        {
            printf("Error: Unable to find a texture on second attempt.\n\a");
            return;
        }
    }

    AddTexture(textures::ASTEROID_SMALL_1, asteroid_small_1);
    AddTexture(textures::ASTEROID_SMALL_2, asteroid_small_2);
    AddTexture(textures::ASTEROID_SMALL_3, asteroid_small_3);
    AddTexture(textures::ASTEROID_SMALL_4, asteroid_small_4);
    AddTexture(textures::ASTEROID_MEDIUM_1, asteroid_medium_1);
    AddTexture(textures::ASTEROID_MEDIUM_2, asteroid_medium_2);
    AddTexture(textures::ASTEROID_MEDIUM_3, asteroid_medium_3);
    AddTexture(textures::ASTEROID_MEDIUM_4, asteroid_medium_4);
    AddTexture(textures::ASTEROID_LARGE_1, asteroid_large_1);
    AddTexture(textures::ASTEROID_LARGE_2, asteroid_large_2);
    AddTexture(textures::ASTEROID_LARGE_3, asteroid_large_3);
    AddTexture(textures::ASTEROID_LARGE_4, asteroid_large_4);
    AddTexture(textures::PLAYER_1, player_1);
    AddTexture(textures::PLAYER_2, player_2);
    AddTexture(textures::PLAYER_1_HEALTH, player_1_health);
    AddTexture(textures::PLAYER_2_HEALTH, player_2_health);
    AddTexture(textures::PLAYER_1_PROJECTILE, player_1_projectile);
    AddTexture(textures::PLAYER_2_PROJECTILE, player_2_projectile);
    AddTexture(textures::GAME_OVER_BACKGROUND, game_over_background);
    AddTexture(textures::GAME_BACKGROUND, game_background);
}