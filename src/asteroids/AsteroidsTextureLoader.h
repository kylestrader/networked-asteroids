#pragma once

#include "galaxyengine/TextureLoader.h"

class AsteroidsTextures
{
public:
    static const std::string ASTEROID_SMALL_1;
    static const std::string ASTEROID_SMALL_2;
    static const std::string ASTEROID_SMALL_3;
    static const std::string ASTEROID_SMALL_4;
    static const std::string ASTEROID_MEDIUM_1;
    static const std::string ASTEROID_MEDIUM_2;
    static const std::string ASTEROID_MEDIUM_3;
    static const std::string ASTEROID_MEDIUM_4;
    static const std::string ASTEROID_LARGE_1;
    static const std::string ASTEROID_LARGE_2;
    static const std::string ASTEROID_LARGE_3;
    static const std::string ASTEROID_LARGE_4;
    static const std::string PLAYER_1;
    static const std::string PLAYER_2;
    static const std::string PLAYER_1_HEALTH;
    static const std::string PLAYER_2_HEALTH;
    static const std::string PLAYER_1_PROJECTILE;
    static const std::string PLAYER_2_PROJECTILE;
    static const std::string GAME_OVER_BACKGROUND;
    static const std::string GAME_BACKGROUND;
};

class AsteroidsTextureLoader : public TextureLoader
{
public:
    typedef AsteroidsTextures textures;

protected:
    virtual void loadAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */) override;
};