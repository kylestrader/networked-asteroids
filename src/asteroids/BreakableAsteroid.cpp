#include "BreakableAsteroid.h"

BreakableAsteroid::BreakableAsteroid(network_id n_id, object_type type, guid owner, int health)
    : Super(n_id, type, owner)
{
    m_WrappedFlag = false;
    m_SpawnedFlag = false;
    m_DestroyFlag = false;
    m_RemoveFlag = false;
    m_Health = health;
}

BreakableAsteroid::~BreakableAsteroid()
{

}

void BreakableAsteroid::Dispose()
{

}

bool BreakableAsteroid::GetSpawnedFlag() const
{
    return m_SpawnedFlag;
}

bool BreakableAsteroid::GetWrappedFlag() const
{
    return m_WrappedFlag;
}

bool BreakableAsteroid::GetDestroyFlag() const
{
    return m_DestroyFlag;
}

bool BreakableAsteroid::GetRemoveFlag() const
{
    return m_RemoveFlag;
}

int BreakableAsteroid::GetHealth() const
{
    return m_Health;
}

void BreakableAsteroid::SetSpawnedFlag(bool value)
{
    m_SpawnedFlag = value;
}

void BreakableAsteroid::SetWrappedFlag(bool value)
{
    m_WrappedFlag = value;
}

void BreakableAsteroid::SetRemoveFlag(bool value)
{
    m_RemoveFlag = value;
}

void BreakableAsteroid::ApplyDamage(int damage)
{
    m_Health -= damage;

    if (m_Health <= 0)
        m_Health = 0;
}

void BreakableAsteroid::Destroy()
{
    m_DestroyFlag = true;
}