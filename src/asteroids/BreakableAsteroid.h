#pragma once

#include "galaxyengine/NetworkedGameObject.h"

class BreakableAsteroid : public NetworkedGameObject
{
public:
    typedef NetworkedGameObject Super;

private:
    bool m_SpawnedFlag;
    bool m_WrappedFlag;
    bool m_DestroyFlag;
    bool m_RemoveFlag;
    int m_Health;

public:
    BreakableAsteroid(network_id n_id, object_type type, guid owner, int health);
    ~BreakableAsteroid();

    void ApplyDamage(int damage);
    void Destroy();

    bool GetSpawnedFlag() const;
    bool GetWrappedFlag() const;
    bool GetDestroyFlag() const;
    bool GetRemoveFlag() const;
    int GetHealth() const;

    void SetSpawnedFlag(bool value);
    void SetWrappedFlag(bool value);
    void SetRemoveFlag(bool value);

    virtual void Dispose() override;
};