#include "DestroyAsteroidEvent.h"

const Events::EventType DestroyAsteroidEvent::DESTROY_ASTEROID_SMALL = "DESTROY_ASTEROID_SMALL";
const Events::EventType DestroyAsteroidEvent::DESTROY_ASTEROID_MEDIUM = "DESTROY_ASTEROID_MEDIUM";
const Events::EventType DestroyAsteroidEvent::DESTROY_ASTEROID_LARGE = "DESTROY_ASTEROID_LARGE";

DestroyAsteroidEvent::DestroyAsteroidEvent(Events::EventType type, network_id id, int numAsteroids /* = 0 */)
    : Super(type)
{
    m_NumAsteroids = numAsteroids;
    m_NetID = id;
}

DestroyAsteroidEvent::~DestroyAsteroidEvent()
{

}

void DestroyAsteroidEvent::Dispose()
{

}

int DestroyAsteroidEvent::GetNumAsteroids() const
{
    return m_NumAsteroids;
}

DestroyAsteroidEvent::network_id DestroyAsteroidEvent::GetNetID() const
{
    return m_NetID;
}