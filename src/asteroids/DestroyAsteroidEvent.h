#pragma once

#include "galaxyengine/Event.h"

class DestroyAsteroidEvent : public Events::Event
{
public:
    typedef Events::Event Super;
    typedef unsigned int network_id;

    static const Events::EventType DESTROY_ASTEROID_SMALL;
    static const Events::EventType DESTROY_ASTEROID_MEDIUM;
    static const Events::EventType DESTROY_ASTEROID_LARGE;

private:
    int m_NumAsteroids;
    int m_NetID;

public:
    DestroyAsteroidEvent(Events::EventType type, network_id id, int numAsteroids = 0);
    ~DestroyAsteroidEvent();

    int GetNumAsteroids() const;
    network_id GetNetID() const;

    virtual void Dispose() override;
};