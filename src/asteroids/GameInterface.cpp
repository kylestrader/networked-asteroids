#include "GameInterface.h"
#include "galaxyengine/NetworkInterface.h"
#include "PongBreak.h"
#include "galaxyengine/Utils.h"
#include "galaxyengine/SoundManager.h"
#include "galaxyengine/Event.h"
#include "galaxyengine/NetworkEvent.h"
#include "galaxyengine/Delegate.h"
#include "galaxyengine/EventDispatcher.h"
#include "galaxyengine/NetworkMessageHelper.h"
#include "PongBreakTextureLoader.h"
#include "GameMessages.h"
#include "BallScoreEvent.h"
#include <iostream>
#include "GameObjectFactory.h"
#include "DeleteEvent.h"

GameInterface::GameInterface()
	: COUNT_DOWN_TIME(3.0f),
	//Source Multiplayer Networking: https://developer.valvesoftware.com/wiki/Source_Multiplayer_Networking
	UPDATE_INTERVAL(.033333f),
	THRESHOLD(5.0)
{
    m_ExitGame = false;
    m_Window = nullptr;
    m_Game = nullptr;
    m_IsServer = false;
    m_GameState = GameState::GAME_ENGINE_LOGO;
    m_NetworkEventDelegate = nullptr;
    m_Player1Assigned = false;
    m_Player2Assigned = false;
    m_PreviousDeltaT = 0.0;
    m_NextUpdateCounter = UPDATE_INTERVAL;

    m_Player1Points = 0;
    m_Player2Points = 0;

	m_HoldingDown = false;
	m_HoldingUp = false;
    m_IsEngineLogoFading = false;

	m_CanSendNetworkedMessages = false;

	m_SoundManager = std::make_unique<SoundManager>();
}

GameInterface::~GameInterface()
{
    if (g_EventDispatcher != nullptr)
    {
        g_EventDispatcher->Dispose();
        delete g_EventDispatcher;
        g_EventDispatcher = nullptr;
    }
}

void GameInterface::Initialize(bool isServer)
{
    g_EventDispatcher = new Events::EventDispatcher();

    m_IsServer = isServer;
    // Setup our event listener for network events
    m_NetworkEventDelegate = Events::Delegate::Create<GameInterface, &GameInterface::onNetworkEvent>(this);
    g_NetworkEventDispatcher->AddEventListener(Events::NETWORK_EVENT, m_NetworkEventDelegate);

    m_BallScoreDelegate = Events::Delegate::Create<GameInterface, &GameInterface::onBallScoreEvent>(this);
    g_EventDispatcher->AddEventListener(Events::BallScoreEvent::PLAYER_SCORED, m_BallScoreDelegate);

    m_DeleteNetworkObjectDelegate = Events::Delegate::Create<GameInterface, &GameInterface::onDeleteNetworkObjectEvent>(this);
    g_EventDispatcher->AddEventListener(Events::DeleteEvent::DELETE_NETWORK_OBJECT, m_DeleteNetworkObjectDelegate);

    OpenWindow();
}

void GameInterface::SetFPS(double fps)
{
    m_FPS = (int)std::ceil(fps);
    if (m_Window && m_Window->isOpen())
        m_Window->setTitle(m_Title + " FPS: " + std::to_string(m_FPS));
}

void GameInterface::UpdateGame(double deltaT)
{
    m_PreviousDeltaT = deltaT; // We store the deltaT so that events can still use it

    if (m_Window && m_Game)
    {
        if (m_Window->isOpen())
		{
			/*
			This will help the GameInterface know if it can send messages, if it should wait to
			send 
			*/
			if (m_NextUpdateCounter  > 0)
			{
				m_NextUpdateCounter -= deltaT;

				if (m_NextUpdateCounter <= 0)
				{
					m_CanSendNetworkedMessages = true;
					m_NextUpdateCounter = UPDATE_INTERVAL;
				}
				else
					m_CanSendNetworkedMessages = false;
			}

			//Input Logging (networked/local Input logic)
			sf::Event event;
			while (m_Window->pollEvent(event))
			{
				switch (event.type)
				{
				case sf::Event::Closed:
					CloseWindow();
					return;
					break;
				case sf::Event::MouseButtonPressed:
					checkMousePress(event.mouseButton.button, deltaT);
					break;
				case sf::Event::MouseButtonReleased:
					checkMouseRelease(event.mouseButton.button, deltaT);
					break;
				case sf::Event::KeyReleased:
					checkKeyRelease(event.key.code, deltaT);
					break;
				case sf::Event::KeyPressed:
					checkKeyPress(event.key.code, deltaT);
					break;
				}
			}

			//Game State: GAME START
			if (m_GameState == GameState::GAME_START)
			{
				if (m_StartCountdown > 0)
				{
					m_StartCountdown -= deltaT;
					printf("Time until start: %f\n", m_StartCountdown);

					if (m_StartCountdown <= 0)
					{
						m_StartCountdown = 0;

						if (m_IsServer)
						{
							sendStartMessage();
                            setState(GameState::GAME_PLAY);
						}
					}

                    updateCountdownText();
				}
			}

			//Game State: GAME_PLAY
            if (m_GameState == GameState::GAME_PLAY || m_GameState == GameState::GAME_START)
            {
				if (m_IsServer && m_CanSendNetworkedMessages)
                    sendUpdateMessage();
            }

            //Game State: GAME ENGINE LOGO
            if (m_GameState == GameState::GAME_ENGINE_LOGO)
            {
                if (m_StartCountdown > 0)
                {
                    m_StartCountdown -= deltaT;

                    if (m_IsEngineLogoFading)
                    {
                        sf::Color color = m_Backgroud.getColor();
                        color.a = (int)Utils::map(m_StartCountdown / (COUNT_DOWN_TIME / 2), 0.0, 1.0, 0.0, 255.0);
                        m_Backgroud.setColor(color);
                    }

                    if (m_StartCountdown <= 0)
                    {
                        m_StartCountdown = 0;

                        if (m_IsEngineLogoFading)
                            setState(GAME_WAITING_TO_START);
                        else
                        {
                            m_IsEngineLogoFading = true;
                            m_StartCountdown = COUNT_DOWN_TIME / 2;
                        }
                    }
                }
            }

            m_Game->Update(deltaT);

            m_Window->clear();

            m_Window->draw(m_Backgroud);

            if (m_GameState != GAME_ENGINE_LOGO)
            {
                m_Game->Draw();

                m_Window->draw(m_Player1ScoreText);
                m_Window->draw(m_Player2ScoreText);

                if (m_CountDownText.getString() != "" && m_CountDownText.getString() != "0.00")
                    m_Window->draw(m_CountDownText);
            }

            m_Window->display();

            if (m_GameState == GAME_RESET)
                resetGame();
        }
    }
}

void GameInterface::OpenWindow()
{
    m_Title = "Pong Break";

    if (m_IsServer)
        m_Title += " - Server";
    else
        m_Title += " - Client";

    m_ExitGame = false;
    m_Player1Assigned = false;

    PongBreakTextureLoader loader;
    loader.LoadTextures();
    loadAssets();
    m_Backgroud.setPosition(0.f, 0.f);

    m_Window = std::make_shared<sf::RenderWindow>();

    m_Window->create(sf::VideoMode(1280, 720), m_Title);

    m_Game = std::make_unique<PongBreak>();
    m_Game->SetFont(m_Font);
    m_Game->Initialize(m_Window);

    if (m_IsServer)
        m_Game->SetDebugMode(true);

    m_StartCountdown = COUNT_DOWN_TIME;
}

void GameInterface::CloseWindow()
{
    m_Window->close();
    m_Window.reset();
    m_Window = nullptr;

    m_Game.reset();
    m_Game = nullptr;

    m_ExitGame = true;
    m_IsServer = false;
    m_GameState = GameState::GAME_WAITING_TO_START;

    NetworkInterface::SendMessageId((NetworkInterface::msg_id)GameMessages::ID_PLAYER_QUIT_MESSAGE, NetworkInterface::GetSessionGuid(), true);
    NetworkInterface::CloseConnection();
}

void GameInterface::onDeleteNetworkObjectEvent(Events::Event* e)
{
    Events::DeleteEvent* ev = (Events::DeleteEvent*)e;

    DeleteNetworkObjectMessage msg;
    msg.MsgId = GameMessagesEx::ID_DELETE_NETWORK_OBJECT_MESSAGE;
    msg.netId = ev->GetNetworkID();

    NetworkInterface::SendMessage(msg, NetworkInterface::GetSessionGuid(), true);
}

void GameInterface::onBallScoreEvent(Events::Event* e)
{
    Events::BallScoreEvent* ev = (Events::BallScoreEvent*)e;

    switch (ev->GetPlayer())
    {
        case (int)GameObjectTypes::LEFT_PLAYER:
            m_Player1Points++;
            printf("Player 1 has %i points\n", m_Player1Points);
            break;
        case (int)GameObjectTypes::RIGHT_PLAYER:
            m_Player2Points++;
            printf("Player 2 has %i points\n", m_Player2Points);
            break;
        default:
            break;
    }

    if (m_IsServer)
    {
        if (m_Player1Points >= 10 || m_Player2Points >= 10)
        {
            NetworkInterface::SendBroadcastId((unsigned char)GameMessagesEx::ID_GAME_RESET_MESSAGE);
            setState(GameState::GAME_RESET);
            return;
        }

        DeleteNetworkObjectMessage msg;
        msg.MsgId = GameMessagesEx::ID_DELETE_NETWORK_OBJECT_MESSAGE;
        msg.netId = ev->GetNetworkID();

        NetworkInterface::SendMessage(msg, NetworkInterface::GetSessionGuid(), true);

        GameScoreMessage msg2;
        msg2.MsgId = GameMessagesEx::ID_GAME_SCORE_MESSAGE;
        msg2.player1Score = m_Player1Points;
        msg2.player2Score = m_Player2Points;

        NetworkInterface::SendMessage(msg2, NetworkInterface::GetSessionGuid(), true);

        updateScoreText();

        m_BallNID = m_Game->CreateBall();
        sendUpdateMessage();
        setState(GameState::GAME_START);
    }
}

void GameInterface::onNetworkEvent(Events::Event* e)
{
    Events::NetworkEvent* ev = (Events::NetworkEvent*)e;

    switch (ev->GetMsgId())
    {
        case GameMessages::ID_PLAYER_CONNECTED_MESSAGE:
            if (m_IsServer)
            {
                printf("New Player has connected\n");

                AssignPlayerMessage msg;
                msg.MsgId = (char)GameMessagesEx::ID_ASSIGN_PLAYER_MESSAGE;

                if (!m_Player1Assigned)
                {
                    msg.player = (int)PlayerPaddle::LEFT;
                    m_Player1GUID = ev->GetGUID();
                    m_Player1Assigned = true;
                    m_Player1NID = m_Game->CreatePlayer(PlayerPaddle::LEFT);
                    msg.netId = m_Player1NID;
                }
                else if (!m_Player2Assigned)
                {
                    msg.player = (int)PlayerPaddle::RIGHT;
                    m_Player2GUID = ev->GetGUID();
                    m_Player2Assigned = true;
                    m_Player2NID = m_Game->CreatePlayer(PlayerPaddle::RIGHT);
                    msg.netId = m_Player2NID;
                }

                NetworkInterface::SendMessage(msg, ev->GetGUID());

                if (m_Player1Assigned && m_Player2Assigned)
                {
                    m_Game->CreatePlayerWall((int)GameObjectTypes::LEFT_WALL);
                    m_Game->CreatePlayerWall((int)GameObjectTypes::RIGHT_WALL);
                    m_BallNID = m_Game->CreateBall();
                    sendUpdateMessage();
                    setState(GameState::GAME_START);
                }
            }
            break;
        case GameMessages::ID_PLAYER_QUIT_MESSAGE:
            if (m_IsServer)
            {
                printf("A Player has just quit.\n");

                if (ev->GetGUID() == m_Player1GUID)
                {
                    m_Player1Assigned = false;
                }
                else if (ev->GetGUID() == m_Player2GUID)
                {
                    m_Player2Assigned = false;
                }

                if (m_GameState == GAME_START || m_GameState == GAME_PLAY)
                {
                    // TODO: Add code to "stop" the game here
                    setState(GAME_WAITING_TO_START);
                }
            }
            break;
        case GameMessagesEx::ID_ASSIGN_PLAYER_MESSAGE:
            if (!m_IsServer)
            {
                AssignPlayerMessage msg = *(AssignPlayerMessage*)ev->GetMsg();
                m_Game->CreatePlayer((PlayerPaddle)msg.player, msg.netId);
                m_Game->SetPlayer(msg.netId);
            }
            break;
        case GameMessagesEx::ID_PLAYER_MOVE_MESSAGE:
        {
            PlayerMoveMessage msg = *(PlayerMoveMessage*)ev->GetMsg();

            if (m_IsServer)
            {
				printf("received move\n");
                if (ev->GetGUID() == m_Player1GUID)
                {
					m_Game->SetPaddleDirection(msg.netId, (ControllerInput)msg.dir);
                }
                else
                {
					m_Game->SetPaddleDirection(msg.netId, (ControllerInput)msg.dir);
                }
            }
            else
				m_Game->SetPaddleDirection(msg.netId, (ControllerInput)msg.dir);

            break;
        }
        case GameMessagesEx::ID_START_COUNTDOWN_MESSAGE:
            setState(GAME_START);
            break;
        case GameMessagesEx::ID_START_GAME_MESSAGE:
        {
            if (!m_IsServer)
            {
                StartGameMessage msg = *(StartGameMessage*)ev->GetMsg();

                m_Game->StartBallClient(msg.ballId, msg.velocityX, msg.velocityY);

                setState(GameState::GAME_PLAY);
            }
            break;
        }
        case GameMessagesEx::ID_UPDATE_NETWORK_OBJECTS_MESSAGE:
            if (!m_IsServer)
            {
				printf("UPDATING NETWORK OBJECTS\n");
                updateNetworkedObjects(ev->GetMsg(), ev->GetSize());
            }
            break;
        case GameMessagesEx::ID_CREATE_NETWORK_OBJECT_MESSAGE:
            if (!m_IsServer)
            {
                CreateNetworkObjectMessage msg = *(CreateNetworkObjectMessage*)ev->GetMsg();

				m_Game->CreateNetworkObject(msg, (float)UPDATE_INTERVAL, (float)THRESHOLD);
                printf("Creating Network Object %i\n", msg.netId);
            }
            break;
        case GameMessagesEx::ID_DELETE_NETWORK_OBJECT_MESSAGE:
            if (!m_IsServer)
            {
                DeleteNetworkObjectMessage msg = *(DeleteNetworkObjectMessage*)ev->GetMsg();

                m_Game->DeleteNetworkObject(msg.netId);
                printf("Deleting Network Object %i\n", msg.netId);
            }            
            break;
        case GameMessagesEx::ID_GAME_SCORE_MESSAGE:
            if (!m_IsServer)
            {
                GameScoreMessage msg = *(GameScoreMessage*)ev->GetMsg();

                m_Player1Points = msg.player1Score;
                m_Player2Points = msg.player2Score;

                updateScoreText();
            }
            break;
        default:
            break;
    }
}

void GameInterface::checkMouseRelease(sf::Mouse::Button button, double deltaT)
{
    if (m_GameState == GameState::GAME_ENGINE_LOGO)
        setState(GAME_WAITING_TO_START);

    if (m_IsServer)
        return;

    if (button == sf::Mouse::Left)
    {
        //sf::Vector2i pos = sf::Mouse::getPosition(*m_Window);
        //sf::IntRect mouseRect(pos.x, pos.y, 1, 1);
    }
}

void GameInterface::checkMousePress(sf::Mouse::Button button, double deltaT)
{
    if (m_IsServer)
        return;

    if (button == sf::Mouse::Left)
    {
        
    }
}

void GameInterface::checkKeyRelease(sf::Keyboard::Key key, double deltaT)
{
    if (m_GameState == GameState::GAME_ENGINE_LOGO)
        setState(GAME_WAITING_TO_START);

    if (m_IsServer)
    {
        switch (key)
        {
            case sf::Keyboard::P:
                setState(GameState::GAME_START);
                break;
            case sf::Keyboard::B:
                sendCreateBallMessage();
                break;
            case sf::Keyboard::W:
                createWalls();
                break;
        }

        return;
    }

    if (!m_Game)
        return;

    if (m_GameState == GAME_PLAY || m_GameState == GAME_START)
    {
        switch (key)
        {
            case sf::Keyboard::Insert:
				m_Game->SetDebugMode(!m_Game->GetDebugMode());
                break;
            case sf::Keyboard::Up:
			case sf::Keyboard::W:
				sendMoveMessage(ControllerInput::ZERO);
				m_HoldingUp = false;
                break;
            case sf::Keyboard::Down:
			case sf::Keyboard::S:
				sendMoveMessage(ControllerInput::ZERO);
				m_HoldingDown = false;
                break;
            case sf::Keyboard::P:
                setState(GameState::GAME_START);
                break;
        }
    }
}

void GameInterface::checkKeyPress(sf::Keyboard::Key key, double deltaT)
{
    if (m_IsServer)
        return;

    if (!m_Game)
        return;

    if (m_GameState == GAME_PLAY || m_GameState == GAME_START)
    {
        switch (key)
        {
            case sf::Keyboard::Up:
            case sf::Keyboard::W:
				if (!m_HoldingUp)
				{
					m_HoldingUp = true;
					sendMoveMessage(ControllerInput::UP);
				}
                break;
            case sf::Keyboard::Down:
            case sf::Keyboard::S:
				if (!m_HoldingDown)
				{
					m_HoldingDown = true;
					sendMoveMessage(ControllerInput::DOWN);
				}
                break;
        }
    }
}

void GameInterface::createWalls()
{
    m_Game->CreatePlayerWall((int)GameObjectTypes::LEFT_WALL);
    m_Game->CreatePlayerWall((int)GameObjectTypes::RIGHT_WALL);
    sendUpdateMessage();
}

void GameInterface::sendMoveMessage(ControllerInput input)
{
    sf::Vector2f pos = m_Game->GetLocalPaddlePosition();
    PlayerMoveMessage msg;
    msg.MsgId = GameMessagesEx::ID_PLAYER_MOVE_MESSAGE;
    msg.netId = m_Game->GetPlayerID();
	msg.dir = input;

	NetworkInterface::SendMessage(msg, NetworkInterface::GetSessionGuid(), true);
}

void GameInterface::sendCreateBallMessage()
{
    float velX, velY;
    PongBreak::network_id id = m_Game->CreateBall();
    m_Game->StartBallServer(id, velX, velY);
    std::shared_ptr<NetworkedGameObject> ball = m_Game->GetNetworkObject(id);

    CreateNetworkObjectMessage msg;
    msg.MsgId = GameMessagesEx::ID_CREATE_NETWORK_OBJECT_MESSAGE;
    msg.netId = id;
    msg.type = ball->GetObjectType();
    msg.x = ball->GetPosition().x;
    msg.y = ball->GetPosition().y;
    msg.velocityX = velX;
    msg.velocityY = velY;

    NetworkInterface::SendMessage(msg, NetworkInterface::GetSessionGuid(), true);
}

// Second Attempt is used to make sure this function doesn't infinitely call itself
// This function should just be called without setting either parameter, it'll handle itself.
void GameInterface::loadAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */)
{
    // Put all asset loading in this try and ALWAYS throw if it fails
    try
    {
        // Fonts
        if (!m_Font.loadFromFile(assetDir + "GALACTIC VANGUARDIAN NCV.ttf"))
            throw 0;
    }
    catch (...)
    {
        if (!secondAttempt)
        {
            printf("Unable to find a texture, trying another directory...\n");
            loadAssets("../content/", true);
            return;
        }
        else
        {
            printf("Error: Unable to find a texture on second attempt.\n\a");
            return;
        }
    }
    m_Backgroud.setTexture(PongBreakTextureLoader::GetTexture(PongBreakTextures::ENGINE_LOGO), true);
    
    m_Player1ScoreText.setFont(m_Font);
    m_Player1ScoreText.setCharacterSize(75);
    m_Player1ScoreText.setPosition(600.f, 115.f);
    m_Player1ScoreText.setColor(sf::Color::White);

    m_Player2ScoreText.setFont(m_Font);
    m_Player2ScoreText.setCharacterSize(75);
    m_Player2ScoreText.setPosition(670.f, 115.f);
    m_Player2ScoreText.setColor(sf::Color::White);

    m_CountDownText.setFont(m_Font);
    m_CountDownText.setCharacterSize(75);
    m_CountDownText.setPosition(640.f, 0.f);
    m_CountDownText.setColor(sf::Color::White);

    updateScoreText();
    updateCountdownText();
}

void GameInterface::setState(GameState state)
{
    m_GameState = state;
    switch (state)
    {
        case GameState::GAME_WAITING_TO_START:
        {
            //Game Waiting to start
            m_StartCountdown = 0.0;
            resetBackground();
            break;
        }
        case GameState::GAME_START:
        {
            resetBackground();

            m_StartCountdown = COUNT_DOWN_TIME;

            if (m_IsServer)
                sendStartCountdownMessage();
            break;
        }
        case GameState::GAME_PLAY:
	    	//Game is in session
            m_StartCountdown = 0.0;
            updateCountdownText();
	    	break;
        case GameState::GAME_OVER:
	    	//Game has ended
	    	break;
        case GameState::GAME_RESET:
            break;
        default:
            break;
    }
}

void GameInterface::resetGame()
{
    m_Player1Points = 0;
    m_Player2Points = 0;
    updateScoreText();

    m_Game.reset();
    m_Game = nullptr;
    m_Game = std::make_unique<PongBreak>();
    m_Game->SetFont(m_Font);
    m_Game->Initialize(m_Window);

    if (m_IsServer)
    {
        m_Game->SetDebugMode(true);
        m_Player1NID = m_Game->CreatePlayer(PlayerPaddle::LEFT);
        m_Player2NID = m_Game->CreatePlayer(PlayerPaddle::RIGHT);
        m_Game->CreatePlayerWall((int)GameObjectTypes::LEFT_WALL);
        m_Game->CreatePlayerWall((int)GameObjectTypes::RIGHT_WALL);
        m_BallNID = m_Game->CreateBall();
        sendUpdateMessage();
        setState(GAME_START);
    }
    else
        setState(GAME_WAITING_TO_START);
}

void GameInterface::resetBackground()
{
    m_Backgroud.setTexture(PongBreakTextureLoader::GetTexture(PongBreakTextures::BACKGROUND));
    sf::Color color = m_Backgroud.getColor();
    color.a = 255;
    m_Backgroud.setColor(color);
}

void GameInterface::restartGame()
{
    m_Game.reset();
    m_Game = nullptr;

    m_Game = std::make_unique<PongBreak>();
    m_Game->Initialize(m_Window);

    setState(GameState::GAME_WAITING_TO_START);
}

void GameInterface::updateScoreText()
{
    m_Player1ScoreText.setString(std::to_string(m_Player1Points));
    sf::FloatRect rect = m_Player1ScoreText.getGlobalBounds();
    m_Player1ScoreText.setPosition(600.f - rect.width, 115.f);

    m_Player2ScoreText.setString(std::to_string(m_Player2Points));
}

void GameInterface::updateCountdownText()
{
    if (m_StartCountdown > 0)
    {
        std::string text = std::to_string(m_StartCountdown);
        size_t index = text.find('.');

        text = text.substr(0, index + 3);

        m_CountDownText.setString(text);
        sf::FloatRect rect = m_CountDownText.getGlobalBounds();
        m_CountDownText.setPosition(640.f - rect.width / 2.f, 0.f);
    }
    else
    {
        m_CountDownText.setString("");
    }
}

void GameInterface::sendStartCountdownMessage()
{
    NetworkInterface::SendBroadcastId((NetworkInterface::msg_id)GameMessagesEx::ID_START_COUNTDOWN_MESSAGE);
}

void GameInterface::sendStartMessage()
{
    float velX = 0.0f,
          velY = 0.0f;

    m_Game->StartBallServer(m_BallNID, velX, velY);

    StartGameMessage msg;
    msg.MsgId = GameMessagesEx::ID_START_GAME_MESSAGE;
    msg.ballId = m_BallNID;
    msg.velocityX = velX;
    msg.velocityY = velY;

	NetworkInterface::SendBroadcast((const char*)&msg, sizeof(StartGameMessage));
}

void GameInterface::sendUpdateMessage()
{
    PongBreak::network_map::iterator it = m_Game->GetNetworkMapBegin();
    PongBreak::network_map::iterator itEnd = m_Game->GetNetworkMapEnd();
    int size = m_Game->GetNetworkMapSize();

    NetworkMessageBuilder builder;
    builder.AddId((char)GameMessagesEx::ID_UPDATE_NETWORK_OBJECTS_MESSAGE);

	std::vector <std::shared_ptr<NetworkedGameObject>> tempDataHolder;

    for (; it != itEnd; it++)
    {
        sf::Vector2f curPos = it->second->GetPosition();
        sf::Vector2f curVel = it->second->GetVelocity();

        tempDataHolder.push_back(it->second);
    }

	builder.AddData(tempDataHolder.size()); // Add array size so we know how much data to pull out

	for (unsigned int i = 0; i < tempDataHolder.size(); i++)
	{
        sf::Vector2f curPos = tempDataHolder[i]->GetPosition();
        sf::Vector2f curVel = tempDataHolder[i]->GetVelocity();

        builder.AddData(tempDataHolder[i]->GetNetworkID());
        builder.AddData(tempDataHolder[i]->GetObjectType());
        builder.AddData(curPos.x);
        builder.AddData(curPos.y);
        builder.AddData(curVel.x);
        builder.AddData(curVel.y);
	}
    tempDataHolder.clear();

    NetworkInterface::SendBroadcast(builder);
}

void GameInterface::updateNetworkedObjects(unsigned char* msg, unsigned int size)
{
    NetworkMessageReader reader(msg, size);

    char id;
    int arraySize;
    reader.GetId(id);
    reader.GetData(arraySize);

    for (int i = 0; i < arraySize; i++)
    {
        UpdateNetworkObjectsMessage delta;
        reader.GetData(delta.netId);
        reader.GetData(delta.type);
        reader.GetData(delta.x);
        reader.GetData(delta.y);
        reader.GetData(delta.velocityX);
        reader.GetData(delta.velocityY);

		m_Game->UpdateNetworkObject(delta, (float)UPDATE_INTERVAL, (float)THRESHOLD);
    }
}