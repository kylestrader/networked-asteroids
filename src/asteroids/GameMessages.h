#pragma once

#include <MessageIdentifiers.h>
#include "galaxyengine/NetworkInterface.h"

// An extension of GameMessages which provides messages specific to this game
enum GameMessagesEx
{
    ID_GAME_MESSAGE_1 = GameMessages::NUM_MESSAGES + 1,
    ID_UPDATE_PLAYER_HEALTH_MESSAGE,
    ID_UPDATE_PLAYER_SCORE_MESSAGE,
    ID_UPDATE_LEVEL_MESSAGE,
    ID_START_COUNTDOWN_MESSAGE,
    ID_START_GAME_MESSAGE,
    ID_KILL_PLAYER_MESSAGE,
    ID_GAME_OVER_MESSAGE,
    ID_REMATCH_MESSAGE,
    ID_RESTART_GAME_MESSAGE,
    ID_UPDATE_PLAYER_ROTATION_MESSAGE
};

enum InputKeys
{
    W = 0,
    A,
    S,
    D,
    ENTER,
    LEFT_CLICK,
    INSERT
};

struct UpdateLevelMessage
{
    unsigned char MsgId;
    int level;
};

struct UpdatePlayerHealthMessage
{
    unsigned char MsgId;
    int player;
    int health;
};

struct UpdatePlayerScoreMessage
{
    unsigned char MsgId;
    int player;
    int score;
};

struct UpdatePlayerRotationMessage
{
    unsigned char MsgId;
    int networkId;
    float rotation;
};

struct KillPlayerMessage
{
    unsigned char MsgId;
    int player;
};

struct GameOverMessage
{
    unsigned char MsgId;
    int winner;
    int winnerScore;
    int loser;
    int loserScore;
};