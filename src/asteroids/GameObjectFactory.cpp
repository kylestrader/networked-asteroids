#include "GameObjectFactory.h"
#include "galaxyengine/NetworkedGameObject.h"
#include "Box2D/Box2D.h"
#include "BreakableAsteroid.h"
#include "Player.h"
#include "AsteroidsTextureLoader.h"
#include "galaxyengine/Utils.h"
#include "Projectile.h"

std::shared_ptr<NetworkedGameObject> GameObjectFactory::CreateNetworkedGameObjectByType(object_type id, network_id n_id, guid owner, class b2World* world)
{
    std::shared_ptr<NetworkedGameObject> object = nullptr;
    const int SMALL_ASTEROID_HEALTH = 3;
    const int MEDIUM_ASTEROID_HEALTH = 6;
    const int LARGE_ASTEROID_HEALTH = 9;

    switch (id)
    {
        #pragma region Choose random asteroid (Small/Medium/Large)
        case (int)GameObjectTypes::ASTEROID_SMALL:
        {
            int num = Utils::generateNumber(0, 4);
            int newID = (int)GameObjectTypes::ASTEROID_SMALL_1;

            switch (num)
            {
                case 0:
                    newID = (int)GameObjectTypes::ASTEROID_SMALL_1;
                    break;
                case 1:
                    newID = (int)GameObjectTypes::ASTEROID_SMALL_2;
                    break;
                case 2:
                    newID = (int)GameObjectTypes::ASTEROID_SMALL_3;
                    break;
                case 3:
                    newID = (int)GameObjectTypes::ASTEROID_SMALL_4;
                    break;
                default:
                    break;
            }

            object = CreateNetworkedGameObjectByType(newID, n_id, owner, world);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_MEDIUM:
        {
            int num = Utils::generateNumber(0, 4);
            int newID = (int)GameObjectTypes::ASTEROID_MEDIUM_1;

            switch (num)
            {
            case 0:
                newID = (int)GameObjectTypes::ASTEROID_MEDIUM_1;
                break;
            case 1:
                newID = (int)GameObjectTypes::ASTEROID_MEDIUM_2;
                break;
            case 2:
                newID = (int)GameObjectTypes::ASTEROID_MEDIUM_3;
                break;
            case 3:
                newID = (int)GameObjectTypes::ASTEROID_MEDIUM_4;
                break;
            default:
                break;
            }

            object = CreateNetworkedGameObjectByType(newID, n_id, owner, world);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_LARGE:
        {
            int num = Utils::generateNumber(0, 4);
            int newID = (int)GameObjectTypes::ASTEROID_LARGE_1;

            switch (num)
            {
            case 0:
                newID = (int)GameObjectTypes::ASTEROID_LARGE_1;
                break;
            case 1:
                newID = (int)GameObjectTypes::ASTEROID_LARGE_2;
                break;
            case 2:
                newID = (int)GameObjectTypes::ASTEROID_LARGE_3;
                break;
            case 3:
                newID = (int)GameObjectTypes::ASTEROID_LARGE_4;
                break;
            default:
                break;
            }

            object = CreateNetworkedGameObjectByType(newID, n_id, owner, world);
            break;
        }
        #pragma endregion

        #pragma region Asteroid Small
        case (int)GameObjectTypes::ASTEROID_SMALL_1:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_SMALL_1, owner, SMALL_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;
            
            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_SMALL_1), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_SMALL_2:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_SMALL_2, owner, SMALL_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;
            
            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_SMALL_2), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_SMALL_3:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_SMALL_3, owner, SMALL_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;
            
            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_SMALL_3), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_SMALL_4:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_SMALL_4, owner, SMALL_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;
            
            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_SMALL_4), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        #pragma endregion

        #pragma region Asteroid Medium
        case (int)GameObjectTypes::ASTEROID_MEDIUM_1:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_MEDIUM_1, owner, MEDIUM_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;
            
            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_MEDIUM_1), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_MEDIUM_2:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_MEDIUM_2, owner, MEDIUM_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;
            
            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_MEDIUM_2), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_MEDIUM_3:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_MEDIUM_3, owner, MEDIUM_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;
            
            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_MEDIUM_3), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_MEDIUM_4:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_MEDIUM_4, owner, MEDIUM_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;
            
            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_MEDIUM_4), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        #pragma endregion

        #pragma region Asteroid Large
        case (int)GameObjectTypes::ASTEROID_LARGE_1:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_LARGE_1, owner, LARGE_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;

            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_LARGE_1), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_LARGE_2:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_LARGE_2, owner, LARGE_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;
            
            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_LARGE_2), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_LARGE_3:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_LARGE_3, owner, LARGE_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;

            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_LARGE_3), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        case (int)GameObjectTypes::ASTEROID_LARGE_4:
        {
            object = std::make_shared<BreakableAsteroid>(n_id, (int)GameObjectTypes::ASTEROID_LARGE_4, owner, LARGE_ASTEROID_HEALTH);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::ASTEROID;

            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::ASTEROID_LARGE_4), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        #pragma endregion

        #pragma region Player
        case (int)GameObjectTypes::PLAYER_1:
        {
            std::shared_ptr<Player> player = std::make_shared<Player>(n_id, (int)GameObjectTypes::PLAYER_1, owner);
            player->InitializeDetails(PlayerMetaData(3, 20, 10));
            object = player;
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_kinematicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::PLAYER;

            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::PLAYER_1), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }

        case (int)GameObjectTypes::PLAYER_2:
        {
            std::shared_ptr<Player> player = std::make_shared<Player>(n_id, (int)GameObjectTypes::PLAYER_2, owner);
            player->InitializeDetails(PlayerMetaData(3, 20, 10));
            object = player;
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_kinematicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::PLAYER;

            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::PLAYER_2), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }

        case (int)GameObjectTypes::PLAYER_1_PROJECTILE:
        {
            object = std::make_shared<Projectile>(n_id, (int)GameObjectTypes::PLAYER_1_PROJECTILE, owner);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::BOX, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::PROJECTILE;
            p_body.m_IsSensor = true;

            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::PLAYER_1_PROJECTILE), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }

        case (int)GameObjectTypes::PLAYER_2_PROJECTILE:
        {
            object = std::make_shared<Projectile>(n_id, (int)GameObjectTypes::PLAYER_2_PROJECTILE, owner);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::BOX, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)CollisionGroups::PROJECTILE;
            p_body.m_IsSensor = true;

            GO_MetaDeta p_obj(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::PLAYER_2_PROJECTILE), sf::Vector2f(0.f, 0.f), sf::Vector2f(0.5f, 0.5f), true);
            object->Initialize(world, p_body, p_obj);
            break;
        }
        #pragma endregion

        default:
            break;
    }

    return object;
}