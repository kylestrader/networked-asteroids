#pragma once

#include <memory>
#include <RakNetTypes.h>

enum class GameObjectTypes
{
    ASTEROID_SMALL = 1, // Use this if you want a random asteroid that is small
    ASTEROID_SMALL_1,
    ASTEROID_SMALL_2,
    ASTEROID_SMALL_3,
    ASTEROID_SMALL_4,
    ASTEROID_MEDIUM, // Use this if you want a random asteroid that is medium
    ASTEROID_MEDIUM_1,
    ASTEROID_MEDIUM_2,
    ASTEROID_MEDIUM_3,
    ASTEROID_MEDIUM_4,
    ASTEROID_LARGE, // Use this if you want a random asteroid that is large
    ASTEROID_LARGE_1,
    ASTEROID_LARGE_2,
    ASTEROID_LARGE_3,
    ASTEROID_LARGE_4,
    ASTEROID_MAX, // Useful for marking the end of the asteroids so you can do ranges easier
    PLAYER_1,
    PLAYER_2,
    PLAYER_1_PROJECTILE,
    PLAYER_2_PROJECTILE
};

enum class CollisionGroups
{
    ASTEROID = 1,
    PLAYER,
    PROJECTILE
};

class GameObjectFactory
{
public:
    typedef int object_type;
    typedef int network_id;
    typedef RakNet::RakNetGUID guid;

public:
    static std::shared_ptr<class NetworkedGameObject> CreateNetworkedGameObjectByType(object_type id, network_id n_id, guid owner, class b2World* world); // Use GameObjectTypes

private:
    GameObjectFactory(){};
    ~GameObjectFactory(){};
};