#include "HealthEvent.h"

const Events::EventType HealthEvent::ADD_HEALTH = "ADD_HEALTH";
const Events::EventType HealthEvent::REMOVE_HEALTH = "REMOVE_HEALTH";
const Events::EventType HealthEvent::UPDATE_HEALTH = "UPDATE_HEALTH";

HealthEvent::HealthEvent(Events::EventType type, int player, int numHealth /* = 1 */)
    : Super(type)
{
    m_Player = player;
    m_NumHealth = numHealth;
}

HealthEvent::~HealthEvent()
{

}

void HealthEvent::Dispose()
{

}

int HealthEvent::GetPlayer() const
{
    return m_Player;
}

int HealthEvent::GetNumHealth() const
{
    return m_NumHealth;
}