#pragma once

#include "galaxyengine/Event.h"

class HealthEvent : public Events::Event
{
public:
    typedef Events::Event Super;

    static const Events::EventType ADD_HEALTH;
    static const Events::EventType REMOVE_HEALTH;
    static const Events::EventType UPDATE_HEALTH;

private:
    int m_Player;
    int m_NumHealth;

public:
    HealthEvent(Events::EventType type, int player, int numHealth = 1);
    ~HealthEvent();

    int GetPlayer() const;
    int GetNumHealth() const;

    virtual void Dispose() override;
};