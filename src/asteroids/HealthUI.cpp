#include "HealthUI.h"
#include "AsteroidsTextureLoader.h"
#include "galaxyengine/EventDispatcher.h"
#include "galaxyengine/Delegate.h"
#include "HealthEvent.h"

HealthUI::HealthUI(int playerNumber, int lives)
{
    m_Player = playerNumber;
    m_Lives = 0;

    m_OnUpdateHealthDelegate = Events::Delegate::Create<HealthUI, &HealthUI::onUpdateHealth>(this);
    g_EventDispatcher->AddEventListener(HealthEvent::ADD_HEALTH, m_OnUpdateHealthDelegate);
    g_EventDispatcher->AddEventListener(HealthEvent::REMOVE_HEALTH, m_OnUpdateHealthDelegate);
    g_EventDispatcher->AddEventListener(HealthEvent::UPDATE_HEALTH, m_OnUpdateHealthDelegate);

    addHealth(lives);
}

HealthUI::~HealthUI()
{
    g_EventDispatcher->RemoveEventListener(HealthEvent::ADD_HEALTH, m_OnUpdateHealthDelegate, false);
    g_EventDispatcher->RemoveEventListener(HealthEvent::REMOVE_HEALTH, m_OnUpdateHealthDelegate, false);
    g_EventDispatcher->RemoveEventListener(HealthEvent::UPDATE_HEALTH, m_OnUpdateHealthDelegate);
}

void HealthUI::Draw(std::shared_ptr<sf::RenderWindow> window)
{
    for (size_t i = 0; i < m_Sprites.size(); i++)
    {
        window->draw(m_Sprites[i]);
    }
}

void HealthUI::addHealth(int value)
{
    for (int i = 0; i < value; i++)
    {
        if (m_Player == 1)
            m_Sprites.push_back(sf::Sprite(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::PLAYER_1_HEALTH)));
        else if (m_Player == 2)
            m_Sprites.push_back(sf::Sprite(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::PLAYER_2_HEALTH)));
    }

    m_Lives += value;

    updateHealthPosition();
}

void HealthUI::removeHealth(int value)
{
    m_Lives -= value;
    if (m_Lives < 0)
        m_Lives = 0;

    for (int i = 0; i < value; i++)
    {
        if (m_Sprites.size() == 0)
        {
            updateHealthPosition();
            return;
        }

        m_Sprites.pop_back();
    }

    updateHealthPosition();
}

void HealthUI::updateHealth(int value)
{
    int diff = m_Lives - value;

    // If it is negative, add health
    if (diff < 0)
    {
        addHealth(abs(diff));
    }
    // If it is positive, remove health
    else if (diff > 0)
    {
        removeHealth(abs(diff));
    }
}

void HealthUI::updateHealthPosition()
{
    const float PADDING = 5.0f;
    float x = 0.f;
    float y = 0.f;
    float diffX = 0.f;
    float diffY = 0.f;

    if (m_Player == 1)
    {
        sf::Vector2u size = AsteroidsTextureLoader::GetTexture(AsteroidsTextures::PLAYER_1_HEALTH).getSize();
        diffX = size.x + PADDING;
        x = PADDING;
        y = 720.f - size.y - PADDING;
    }
    else if (m_Player == 2)
    {
        sf::Vector2u size = AsteroidsTextureLoader::GetTexture(AsteroidsTextures::PLAYER_2_HEALTH).getSize();
        diffX = -(size.x + PADDING);
        x = 1280.f - size.x - PADDING;
        y = 720.f - size.y - PADDING;
    }

    for (size_t i = 0; i < m_Sprites.size(); i++)
    {
        m_Sprites[i].setPosition(x, y);

        x += diffX;
        y += diffY;
    }
}

void HealthUI::onUpdateHealth(Events::Event* e)
{
    HealthEvent* ev = (HealthEvent*)e;

    if (m_Player == ev->GetPlayer())
    {
        if (ev->GetType() == HealthEvent::ADD_HEALTH)
        {
            addHealth(ev->GetNumHealth());
        }
        else if (ev->GetType() == HealthEvent::REMOVE_HEALTH)
        {
            removeHealth(ev->GetNumHealth());
        }
        else if (ev->GetType() == HealthEvent::UPDATE_HEALTH)
        {
            updateHealth(ev->GetNumHealth());
        }
    }
}

int HealthUI::GetHealth() const
{
    return m_Lives;
}