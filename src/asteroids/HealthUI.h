#pragma once

#include <SFML/Graphics.hpp>
#include <memory>

namespace Events
{
    class Event;
    class Delegate;
}

class HealthUI
{
private:
    int m_Player;
    int m_Lives;
    std::vector<sf::Sprite> m_Sprites;
    Events::Delegate* m_OnUpdateHealthDelegate;

private:
    void onUpdateHealth(Events::Event* e);

    void updateHealthPosition();
    void addHealth(int value);
    void removeHealth(int value);
    void updateHealth(int value);

public:
    HealthUI(int playerNumber, int lives);
    ~HealthUI();

    int GetHealth() const;
    
    void Draw(std::shared_ptr<sf::RenderWindow> window);
};