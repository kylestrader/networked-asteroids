#include "LevelEvent.h"

const Events::EventType LevelEvent::UPDATE_LEVEL_UI = "UPDATE_LEVEL_UI";

LevelEvent::LevelEvent(int level)
    : Super(UPDATE_LEVEL_UI)
{
    m_Level = level;
}

LevelEvent::~LevelEvent()
{

}

void LevelEvent::Dispose()
{

}

int LevelEvent::GetLevel() const
{
    return m_Level;
}
