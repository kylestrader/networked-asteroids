#pragma once

#include "galaxyengine/Event.h"

class LevelEvent : public Events::Event
{
public:
    typedef Events::Event Super;

    static const Events::EventType UPDATE_LEVEL_UI;

private:
    int m_Level;

public:
    LevelEvent(int level);
    ~LevelEvent();

    int GetLevel() const;

    virtual void Dispose() override;
};