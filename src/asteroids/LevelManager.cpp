#include "LevelManager.h"
#include "galaxyengine/EventDispatcher.h"
#include "SpawnAsteroidEvent.h"
#include "LevelEvent.h"

LevelManager::LevelManager(uint smallIncrement, uint mediumIncrememnt, uint largeIncrement, uint maxSmall, uint maxMedium)
{
    m_SMALL_INCREMENT = smallIncrement;
    m_MEDIUM_INCREMENT = mediumIncrememnt;
    m_LARGE_INCREMENT = largeIncrement;
    m_MAX_SMALL = maxSmall;
    m_MAX_MEDIUM = maxMedium;
    m_CurrentLevel = 0;
    m_CurrentSmall = 0;
    m_CurrentMedium = 0;
    m_CurrentLarge = 0;
}

LevelManager::~LevelManager()
{

}

void LevelManager::StartNextLevel()
{
    m_RemainingAsteroids = 0;
    m_CurrentLevel++;

    printf("Starting Level %i\n", m_CurrentLevel);

    m_CurrentSmall += m_SMALL_INCREMENT;
    m_CurrentMedium += m_MEDIUM_INCREMENT;
    m_CurrentLarge += m_LARGE_INCREMENT;

    checkMaxValues();
    spawnAsteroids();

    g_EventDispatcher->DispatchEvent(new LevelEvent(m_CurrentLevel));
}

void LevelManager::ResetLevels()
{
    m_CurrentLevel = 0;
    m_CurrentSmall = 0;
    m_CurrentMedium = 0;
    m_CurrentLarge = 0;
}

void LevelManager::checkMaxValues()
{
    uint diff = 0;
    if (m_CurrentSmall > m_MAX_SMALL)
    {
        diff = m_CurrentSmall - m_MAX_SMALL;
        m_CurrentSmall -= diff;

        m_CurrentMedium += diff / 2;
    }

    if (m_CurrentMedium > m_MAX_MEDIUM)
    {
        diff = m_CurrentMedium - m_MAX_MEDIUM;
        m_CurrentMedium -= diff;

        m_CurrentLarge += diff / 2;
    }
}

void LevelManager::spawnAsteroids()
{
    addRemainingAsteroids();
    g_EventDispatcher->DispatchEvent(new SpawnAsteroidEvent(m_CurrentSmall, m_CurrentMedium, m_CurrentLarge));
}

void LevelManager::addRemainingAsteroids()
{
    // Add the small
    m_RemainingAsteroids += m_CurrentSmall;

    // Add the medium + medium * 3 (for when they split)
    m_RemainingAsteroids += m_CurrentMedium + m_CurrentMedium * 3;

    // Add the large + large * 3 (mediums created) + large * 9 (smalls created)
    m_RemainingAsteroids += m_CurrentLarge + m_CurrentLarge * 3 + m_CurrentLarge * 9;
}

void LevelManager::RemoveAsteroids(int num /* = 1 */)
{
    m_RemainingAsteroids -= num;

    printf("%i Asteroids Remaining in Level %i\n", m_RemainingAsteroids, m_CurrentLevel);

    if (m_RemainingAsteroids <= 0)
    {
        StartNextLevel();
    }
}

LevelManager::uint LevelManager::GetMaxSmall() const
{
    return m_MAX_SMALL;
}

LevelManager::uint LevelManager::GetMaxMedium() const
{
    return m_MAX_MEDIUM;
}

LevelManager::uint LevelManager::GetSmallIncrement() const
{
    return m_SMALL_INCREMENT;
}

LevelManager::uint LevelManager::GetMediumIncrement() const
{
    return m_MEDIUM_INCREMENT;
}

LevelManager::uint LevelManager::GetLargeIncrement() const
{
    return m_LARGE_INCREMENT;
}

LevelManager::uint LevelManager::GetCurrentSmall() const
{
    return m_CurrentSmall;
}

LevelManager::uint LevelManager::GetCurrentMedium() const
{
    return m_CurrentMedium;
}

LevelManager::uint LevelManager::GetCurrentLarge() const
{
    return m_CurrentLarge;
}

LevelManager::uint LevelManager::GetCurrentLevel() const
{
    return m_CurrentLevel;
}

void LevelManager::SetMaxSmall(uint value)
{
    m_MAX_SMALL = value;
}

void LevelManager::SetMaxMedium(uint value)
{
    m_MAX_MEDIUM = value;
}

void LevelManager::SetSmallIncrement(uint value)
{
    m_SMALL_INCREMENT = value;
}

void LevelManager::SetMediumIncrement(uint value)
{
    m_MEDIUM_INCREMENT = value;
}

void LevelManager::SetLargeIncrement(uint value)
{
    m_LARGE_INCREMENT = value;
}

void LevelManager::SetCurrentSmall(uint value)
{
    m_CurrentSmall = value;
}

void LevelManager::SetCurrentMedium(uint value)
{
    m_CurrentMedium = value;
}

void LevelManager::SetCurrentLarge(uint value)
{
    m_CurrentLarge = value;
}