#pragma once

class LevelManager
{
public:
    typedef unsigned int uint;

private:
    // The maximum number of small and medium asteroids before you just increment
    // the next highest asteroid type
    uint m_MAX_SMALL;
    uint m_MAX_MEDIUM;

    // How much to increment the number of asteroids per level
    uint m_SMALL_INCREMENT;
    uint m_MEDIUM_INCREMENT;
    uint m_LARGE_INCREMENT;

    // Number of asteroids for the current level
    uint m_CurrentSmall;
    uint m_CurrentMedium;
    uint m_CurrentLarge;

    uint m_CurrentLevel;
    uint m_RemainingAsteroids;

private:
    void checkMaxValues();
    void spawnAsteroids();
    void addRemainingAsteroids();

public:
    LevelManager(uint smallIncrement, uint mediumIncrememnt, uint largeIncrement, uint maxSmall, uint maxMedium);
    ~LevelManager();

    void StartNextLevel();
    void ResetLevels();

    void RemoveAsteroids(int num = 1);

    // Getters
    uint GetMaxSmall() const;
    uint GetMaxMedium() const;

    uint GetSmallIncrement() const;
    uint GetMediumIncrement() const;
    uint GetLargeIncrement() const;

    uint GetCurrentSmall() const;
    uint GetCurrentMedium() const;
    uint GetCurrentLarge() const;

    uint GetCurrentLevel() const;
    uint GetRemainingAsteroids() const;

    // Setters
    void SetMaxSmall(uint value);
    void SetMaxMedium(uint value);

    void SetSmallIncrement(uint value);
    void SetMediumIncrement(uint value);
    void SetLargeIncrement(uint value);

    void SetCurrentSmall(uint value);
    void SetCurrentMedium(uint value);
    void SetCurrentLarge(uint value);
};