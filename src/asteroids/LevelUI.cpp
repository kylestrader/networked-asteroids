#include "LevelUI.h"
#include "galaxyengine/EventDispatcher.h"
#include "LevelEvent.h"
#include "galaxyengine/Delegate.h"

LevelUI::LevelUI(const sf::Font& font)
{
    m_Font = font;

    m_Text.setFont(m_Font);
    m_Text.setString("Level - 0");
    m_Text.setPosition(5.f, 0.f);
    m_Text.setColor(sf::Color::White);

    m_UpdateDelegate = Events::Delegate::Create<LevelUI, &LevelUI::onUpdateLevel>(this);
    g_EventDispatcher->AddEventListener(LevelEvent::UPDATE_LEVEL_UI, m_UpdateDelegate);

    centerText();
}

LevelUI::~LevelUI()
{
    g_EventDispatcher->RemoveEventListener(LevelEvent::UPDATE_LEVEL_UI, m_UpdateDelegate);
}

void LevelUI::Draw(std::shared_ptr<sf::RenderWindow> window)
{
    window->draw(m_Text);
}

void LevelUI::centerText()
{
    sf::FloatRect rect = m_Text.getGlobalBounds();
    m_Text.setPosition(640.f - rect.width / 2, 0.f);
}

void LevelUI::onUpdateLevel(Events::Event* e)
{
    LevelEvent* ev = (LevelEvent*)e;

    m_Text.setString("Level - " + std::to_string(ev->GetLevel()));

    centerText();
}