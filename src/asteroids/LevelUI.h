#pragma once

#include <SFML/Graphics.hpp>
#include <memory>

namespace Events
{
    class Event;
    class Delegate;
}

class LevelUI
{
private:
    sf::Font m_Font;
    sf::Text m_Text;
    Events::Delegate* m_UpdateDelegate;

private:
    void centerText();
    void onUpdateLevel(Events::Event* e);

public:
    LevelUI(const sf::Font& font);
    ~LevelUI();

    void Draw(std::shared_ptr<sf::RenderWindow> window);
};