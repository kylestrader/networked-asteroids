#include "Player.h"
#include "galaxyengine/utils.h"
#include "GameMessages.h"
#include "GameObjectFactory.h"
#include "galaxyengine/EventDispatcher.h"
#include "ProjectileEvent.h"

Player::Player(network_id n_id, object_type type, guid owner) 
    : Super(n_id, type, owner)
{
    m_IsFiring = false;
    m_ShootDelay = 0.0;
    m_FireRate = 0.f;
}

Player::~Player()
{

}

void Player::InitializeDetails(const PlayerMetaData& data)
{
    m_FireRate = data.m_FireRate;
    m_Health = data.m_Health;
    m_PlayerSpeed = data.m_PlayerSpeed;
}

void Player::Update(double deltaT)
{
    Super::Update(deltaT);

    if (m_IsFiring)
    {
        m_ShootDelay -= deltaT;

        if (m_ShootDelay <= 0)
        {
            double newDelay = 1 / m_FireRate;
            
            // Spawn X number of projectiles where X is how many times shoot delay has been passed
            while (m_ShootDelay <= 0)
            {
                m_ShootDelay += newDelay;

                spawnAsteroid();
            }
        }
    }
}

void Player::spawnAsteroid()
{
    int type;
    guid owner = NetworkInterface::GetSessionGuid();

    if (GetObjectType() == (int)GameObjectTypes::PLAYER_1)
    {
        type = (int)GameObjectTypes::PLAYER_1_PROJECTILE;
    }
    else if (GetObjectType() == (int)GameObjectTypes::PLAYER_2)
    {
        type = (int)GameObjectTypes::PLAYER_2_PROJECTILE;
    }

    const float SPEED = 15.f;
    const float RADIUS = 30.f;

    // Get center pos of player
    sf::Vector2f centerPos = GetPosition();

    // Get rotation
    float rotation = GetBox2DRotationRads();

    // Calculate position of a point R distance away from center based on rotation
    sf::Vector2f spawnPos(centerPos.x + cosf(rotation) * RADIUS,
        centerPos.y + sinf(rotation) * RADIUS);

    // Calculate direction vector from center to new point
    sf::Vector2f dirVec;
    dirVec.x = spawnPos.x - centerPos.x;
    dirVec.y = spawnPos.y - centerPos.y;
    Utils::normalize(dirVec.x, dirVec.y);

    // Calculate a velocity
    sf::Vector2f velVec;
    velVec.x = SPEED * dirVec.x;
    velVec.y = SPEED * dirVec.y;

    g_EventDispatcher->DispatchEvent(new ProjectileEvent(type, owner, spawnPos, velVec));
}

void Player::Dispose()
{
    
}

int Player::GetHealth()
{
    return m_Health;
}
PlayerState Player::GetPlayerState()
{
    return m_PlayerState;
}

float Player::GetPlayerSpeed()
{
    return m_PlayerSpeed;
}

float Player::GetFireRate()
{
    return m_FireRate;
}

bool Player::GetIsFiring()
{
    return m_IsFiring;
}

sf::Vector2f Player::GetLookAtPosition()
{
    return m_LookAtPosition;
}

void Player::SetHealth(int health)
{
    m_Health = health;
}

void Player::SetPlayerState(PlayerState state)
{
    m_PlayerState = state;
}

void Player::SetPlayerSpeed(float playerSpeed)
{
    m_PlayerSpeed = playerSpeed;
}

void Player::SetFireRate(float fireRate)
{
    m_FireRate = fireRate;
}

void Player::SetIsFiring(bool isFiring)
{
    m_IsFiring = isFiring;

    if (m_IsFiring)
    {
        m_ShootDelay = 0;
    }
}

void Player::SetLookAtPosition(const sf::Vector2f& lookAt)
{
    m_LookAtPosition = lookAt;
}
