#pragma once

#include "galaxyengine/NetworkedGameobject.h"

enum class PlayerState
{
    NOT_SPAWNED = 0,
    ALIVE,
    DEAD
};

enum PlayerDirection
{
    UP = 0,
    DOWN,
    LEFT,
    RIGHT
};

struct PlayerMetaData
{
    PlayerMetaData(int health, float playerSpeed, float fireRate)
    {
        m_Health = health;
        m_PlayerSpeed = playerSpeed;
        m_FireRate = fireRate;
    }

    int m_Health;
    float m_PlayerSpeed;
    float m_FireRate;
};

class Player : public NetworkedGameObject
{
public:
    typedef NetworkedGameObject Super;

private:
    void spawnAsteroid();

public:

    //Constructors/Destructors
    Player(network_id n_id, object_type type, guid owner);
    ~Player();

    //Utils
    virtual void InitializeDetails(const PlayerMetaData& data);
    virtual void Update(double deltaT) override;
    virtual void Dispose() override;

    //Getters
    int GetHealth();
    PlayerState GetPlayerState();
    float GetPlayerSpeed();
    float GetFireRate();
    bool GetIsFiring();
    sf::Vector2f GetLookAtPosition();

    //Setters
    void SetHealth(int health);
    void SetPlayerState(PlayerState state);
    void SetPlayerSpeed(float playerSpeed);
    void SetFireRate(float fireRate);
    void SetIsFiring(bool isFiring);
    void SetLookAtPosition(const sf::Vector2f& lookAt);

private:
    int m_Health;
    PlayerState m_PlayerState;
    float m_PlayerSpeed;
    float m_FireRate; // Bullets per second
    bool m_IsFiring;
    sf::Vector2f m_LookAtPosition;
    double m_ShootDelay;
};