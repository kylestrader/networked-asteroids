#include "PlayerEvent.h"

const Events::EventType PlayerEvent::DAMAGE_PLAYER = "DAMAGE_PLAYER";
const Events::EventType PlayerEvent::KILL_PLAYER = "KILL_PLAYER";

PlayerEvent::PlayerEvent(int player)
    : Super(KILL_PLAYER)
{
    m_Player = player;
    m_Body = nullptr;
}

PlayerEvent::PlayerEvent(b2Body* body)
    :Super(DAMAGE_PLAYER)
{
    m_Body = body;
    m_Player = -1;
}

PlayerEvent::~PlayerEvent()
{

}

void PlayerEvent::Dispose()
{

}

b2Body* PlayerEvent::GetBody() const
{
    return m_Body;
}

int PlayerEvent::GetPlayer() const
{
    return m_Player;
}