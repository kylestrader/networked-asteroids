#pragma once

#include "galaxyengine/Event.h"

class b2Body;

class PlayerEvent : public Events::Event
{
public:
    typedef Events::Event Super;

    static const Events::EventType DAMAGE_PLAYER;
    static const Events::EventType KILL_PLAYER;

private:
    b2Body* m_Body;
    int m_Player;

public:
    PlayerEvent(int player);
    PlayerEvent(b2Body* body);
    ~PlayerEvent();

    b2Body* GetBody() const;
    int GetPlayer() const;

    virtual void Dispose() override;
};