#include "Projectile.h"

const double Projectile::PROJECTILE_LIFETIME = 5.0;

Projectile::Projectile(network_id id, object_type type, guid owner, int damage /* = 1 */)
    : Super(id, type, owner)
{
    m_Damage = damage;
    m_Lifetime = PROJECTILE_LIFETIME;
}

Projectile::~Projectile()
{

}

void Projectile::Dispose()
{

}

void Projectile::Update(double deltaT)
{
    Super::Update(deltaT);

    m_Lifetime -= deltaT;
    if (m_Lifetime <= 0)
        Kill();
}

int Projectile::GetDamage() const
{
    return m_Damage;
}