#pragma once

#include "galaxyengine/NetworkedGameObject.h"

class Projectile : public NetworkedGameObject
{
public:
    typedef NetworkedGameObject Super;

    const static double PROJECTILE_LIFETIME;

private:
    int m_Damage;
    double m_Lifetime;

public:
    Projectile(network_id id, object_type type, guid owner, int damage = 1);
    ~Projectile();

    int GetDamage() const;

    virtual void Update(double deltaT) override;
    virtual void Dispose() override;
};