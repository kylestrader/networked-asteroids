#include "ProjectileEvent.h"

const Events::EventType ProjectileEvent::DAMAGE_ASTEROID = "DAMAGE_ASTEROID";
const Events::EventType ProjectileEvent::SPAWN_PROJECTILE = "SPAWN_PROJECTILE";

ProjectileEvent::ProjectileEvent(b2Body* projectile, b2Body* asteroid)
    :Super(DAMAGE_ASTEROID)
{
    m_Projectile = projectile;
    m_Asteroid = asteroid;
}

ProjectileEvent::ProjectileEvent(int type, guid owner, const sf::Vector2f& spawnPos, const sf::Vector2f& velVec)
    :Super(SPAWN_PROJECTILE)
{
    m_Type = type;
    m_Owner = owner;
    m_SpawnPos = spawnPos;
    m_VelVec = velVec;
}

ProjectileEvent::~ProjectileEvent()
{

}

void ProjectileEvent::Dispose()
{

}

b2Body* ProjectileEvent::GetProjectile() const
{
    return m_Projectile;
}

b2Body* ProjectileEvent::GetAsteroid() const
{
    return m_Asteroid;
}

int ProjectileEvent::GetType() const
{
    return m_Type;
}

ProjectileEvent::guid ProjectileEvent::GetOwner() const
{
    return m_Owner;
}

const sf::Vector2f& ProjectileEvent::GetSpawnPos() const
{
    return m_SpawnPos;
}

const sf::Vector2f& ProjectileEvent::GetVelocity() const
{
    return m_VelVec;
}