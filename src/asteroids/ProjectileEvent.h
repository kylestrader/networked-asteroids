#pragma once

#include "galaxyengine/Event.h"
#include <RakNetTypes.h>
#include <SFML/Graphics.hpp>

class b2Body;

class ProjectileEvent : public Events::Event
{
public:
    typedef Events::Event Super;
    typedef unsigned char uchar;
    typedef RakNet::RakNetGUID guid;

    static const Events::EventType DAMAGE_ASTEROID;
    static const Events::EventType SPAWN_PROJECTILE;

private:
    b2Body* m_Projectile;
    b2Body* m_Asteroid;
    int m_Type;
    guid m_Owner;
    sf::Vector2f m_SpawnPos;
    sf::Vector2f m_VelVec;

public:
    ProjectileEvent(int type, guid owner, const sf::Vector2f& spawnPos, const sf::Vector2f& velVec);
    ProjectileEvent(b2Body* projectile, b2Body* asteroid);
    ~ProjectileEvent();

    b2Body* GetProjectile() const;
    b2Body* GetAsteroid() const;

    uchar GetMsgId() const;
    int GetType() const;
    guid GetOwner() const;
    const sf::Vector2f& GetSpawnPos() const;
    const sf::Vector2f& GetVelocity() const;

    virtual void Dispose() override;
};