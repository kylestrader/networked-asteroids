#include "ScoreEvent.h"

const Events::EventType ScoreEvent::UPDATE_SCORE = "UPDATE_SCORE";
const Events::EventType ScoreEvent::ADD_SCORE = "ADD_SCORE";

ScoreEvent::ScoreEvent(Events::EventType type, int player, int score)
    :Super(type)
{
    m_Player = player;
    m_Score = score;
}

ScoreEvent::~ScoreEvent()
{

}

void ScoreEvent::Dispose()
{

}

int ScoreEvent::GetPlayer() const
{
    return m_Player;
}

int ScoreEvent::GetScore() const
{
    return m_Score;
}