#pragma once

#include "galaxyengine/Event.h"

class ScoreEvent : public Events::Event
{
public:
    typedef Events::Event Super;

    static const Events::EventType UPDATE_SCORE;
    static const Events::EventType ADD_SCORE;

private:
    int m_Player;
    int m_Score;

public:
    ScoreEvent(Events::EventType type, int player, int score);
    ~ScoreEvent();

    int GetPlayer() const;
    int GetScore() const;

    virtual void Dispose() override;
};