#include "ScoreUI.h"
#include "galaxyengine/EventDispatcher.h"
#include "galaxyengine/Delegate.h"
#include "ScoreEvent.h"

ScoreUI::ScoreUI(const sf::Font& font, int playerNumber)
{
    m_Font = font;
    m_Player = playerNumber;
    m_Score = 0;

    m_Text.setFont(m_Font);
    m_Text.setColor(sf::Color::White);

    if (playerNumber == 1)
        m_Prefix = "P1 Score - ";
    else if (playerNumber == 2)
        m_Prefix = "P2 Score - ";

    updateText();

    m_OnUpdateScoreDelegate = Events::Delegate::Create<ScoreUI, &ScoreUI::onUpdateScore>(this);
    g_EventDispatcher->AddEventListener(ScoreEvent::ADD_SCORE, m_OnUpdateScoreDelegate);
    g_EventDispatcher->AddEventListener(ScoreEvent::UPDATE_SCORE, m_OnUpdateScoreDelegate);
}

ScoreUI::~ScoreUI()
{
    g_EventDispatcher->RemoveEventListener(ScoreEvent::ADD_SCORE, m_OnUpdateScoreDelegate, false);
    g_EventDispatcher->RemoveEventListener(ScoreEvent::UPDATE_SCORE, m_OnUpdateScoreDelegate);
}

void ScoreUI::Draw(std::shared_ptr<sf::RenderWindow> window)
{
    window->draw(m_Text);
}

void ScoreUI::updateTextPosition()
{
    if (m_Player == 1)
        m_Text.setPosition(5.f, 0.f);
    else
    {
        sf::FloatRect rect = m_Text.getGlobalBounds();
        m_Text.setPosition(1275.f - rect.width, 0.f);
    }
}

void ScoreUI::onUpdateScore(Events::Event* e)
{
    ScoreEvent* ev = (ScoreEvent*)e;

    if (m_Player == ev->GetPlayer())
    {
        if (ev->GetType() == ScoreEvent::ADD_SCORE)
        {
            addScore(ev->GetScore());
        }
        else if (ev->GetType() == ScoreEvent::UPDATE_SCORE)
        {
            updateScore(ev->GetScore());
        }
    }
}

void ScoreUI::addScore(int value)
{
    m_Score += value;
    updateText();
}

void ScoreUI::updateText()
{
    m_Text.setString(m_Prefix + std::to_string(m_Score));

    updateTextPosition();
}

void ScoreUI::updateScore(int value)
{
    m_Score = value;
    updateText();
}

int ScoreUI::GetScore() const
{
    return m_Score;
}