#pragma once

#include <SFML/Graphics.hpp>
#include <memory>
#include <string>

namespace Events
{
    class Delegate;
    class Event;
}

class ScoreUI
{
private:
    sf::Font m_Font;
    sf::Text m_Text;
    std::string m_Prefix;
    int m_Score;
    int m_Player;
    Events::Delegate* m_OnUpdateScoreDelegate;

private:
    void updateText();
    void updateTextPosition();
    void onUpdateScore(Events::Event* e);
    void addScore(int value);
    void updateScore(int value);

public:
    ScoreUI(const sf::Font& font, int playerNumber);
    ~ScoreUI();

    int GetScore() const;

    void Draw(std::shared_ptr<sf::RenderWindow> window);
};