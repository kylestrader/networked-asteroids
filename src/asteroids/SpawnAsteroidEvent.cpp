#include "SpawnAsteroidEvent.h"

const Events::EventType SpawnAsteroidEvent::SPAWN_ASTEROIDS = "SPAWN_ASTEROIDS";

SpawnAsteroidEvent::SpawnAsteroidEvent(uint numSmall, uint numMedium, uint numLarge)
    : Super(SPAWN_ASTEROIDS)
{
    m_NumSmall = numSmall;
    m_NumMedium = numMedium;
    m_NumLarge = numLarge;
}

SpawnAsteroidEvent::~SpawnAsteroidEvent()
{

}

void SpawnAsteroidEvent::Dispose()
{

}

SpawnAsteroidEvent::uint SpawnAsteroidEvent::GetNumSmall() const
{
    return m_NumSmall;
}

SpawnAsteroidEvent::uint SpawnAsteroidEvent::GetNumMedium() const
{
    return m_NumMedium;
}

SpawnAsteroidEvent::uint SpawnAsteroidEvent::GetNumLarge() const
{
    return m_NumLarge;
}