#pragma once

#include "galaxyengine/Event.h"

class SpawnAsteroidEvent : public Events::Event
{
public:
    typedef Events::Event Super;
    typedef unsigned int uint;

    static const Events::EventType SPAWN_ASTEROIDS;

private:
    uint m_NumSmall;
    uint m_NumMedium;
    uint m_NumLarge;

public:
    SpawnAsteroidEvent(uint numSmall, uint numMedium, uint numLarge);
    ~SpawnAsteroidEvent();

    uint GetNumSmall() const;
    uint GetNumMedium() const;
    uint GetNumLarge() const;

    virtual void Dispose() override;
};