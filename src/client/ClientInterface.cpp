
#include "ClientInterface.h"
#include "asteroids/AsteroidsTextureLoader.h"
#include "asteroids/Asteroids.h"
#include "asteroids/GameObjectFactory.h"
#include "galaxyengine/Event.h"
#include "galaxyengine/NetworkEvent.h"
#include "galaxyengine/Delegate.h"
#include "galaxyengine/EventDispatcher.h"
#include "galaxyengine/Utils.h"
#include "asteroids/GameMessages.h"
#include "asteroids/HealthEvent.h"
#include "asteroids/ScoreEvent.h"
#include "asteroids/LevelEvent.h"
#include "galaxyengine/NetworkedGameObject.h"
#include "asteroids/Player.h"
#include "asteroids/ProjectileEvent.h"

ClientInterface::ClientInterface()
    : COUNT_DOWN_TIME(3.0)
{
    
}

ClientInterface::~ClientInterface()
{

}

void ClientInterface::initializeTextures()
{
    Super::initializeTextures();

    AsteroidsTextureLoader textures;
    textures.LoadTextures();
}

void ClientInterface::createGame()
{
    m_Game = std::make_shared<Asteroids>();
    m_Game->SetFont(GetDefaultFont());
    m_Game->ClearNetworkID();
    //SetThreshold(4.66);
    SetThreshold(10.0);
}

void ClientInterface::initializeGame()
{
    Super::initializeGame();
    m_ClientState = SPECTATING;

    m_CountDownText.setFont(GetDefaultFont());
    m_CountDownText.setCharacterSize(75);
    m_CountDownText.setPosition(640.f, 20.f);
    m_CountDownText.setColor(sf::Color::White);
    updateCountdownText();

    m_LoseBackground.setTexture(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::GAME_OVER_BACKGROUND));
    m_LoseBackground.setPosition(0.f, 0.f);

    m_RetryText.setFont(GetDefaultFont());
    m_RetryText.setCharacterSize(24);
    m_RetryText.setColor(sf::Color::White);

    m_LoserText.setFont(GetDefaultFont());
    m_LoserText.setCharacterSize(36);
    m_LoserText.setColor(sf::Color(0, 255, 246, 255));

    m_WinnerText.setFont(GetDefaultFont());
    m_WinnerText.setCharacterSize(36);
    m_WinnerText.setColor(sf::Color(0, 255, 246, 255));

    m_InputDirection = sf::Vector2i(0, 0);
    m_PlayerSpeed = 6.f;

    m_StartCountdown = 0.0;
    m_PlayerScreenOffset = 200;
    m_PlayerNetworkId = -1;
    m_IsDead = false;
    m_GameLost = false;
    m_IsFiring = false;

    m_SpawnProjectileDelegate = Events::Delegate::Create<ClientInterface, &ClientInterface::onSpawnProjectile>(this);
    g_EventDispatcher->AddEventListener(ProjectileEvent::SPAWN_PROJECTILE, m_SpawnProjectileDelegate);
}

void ClientInterface::loadAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */)
{

}

void ClientInterface::disposeGame()
{
    Super::disposeGame();

    g_EventDispatcher->RemoveEventListener(ProjectileEvent::SPAWN_PROJECTILE, m_SpawnProjectileDelegate);
}

void ClientInterface::checkMousePress(Button button, double deltaT)
{
    Super::checkMousePress(button, deltaT);

    if (m_IsDead)
        return;

    if (m_PlayerNetworkId == -1)
        return;

    if (button == Button::Left)
    {
        if (!m_InputState.LEFT_CLICK_PRESSED)
        {
            m_InputState.LEFT_CLICK_PRESSED = true;
            if (!m_IsFiring)
            {
                m_IsFiring = true;
                printf("Start Firing\n");
                std::dynamic_pointer_cast<Player>(m_Game->GetNetworkObject(m_PlayerNetworkId))->SetIsFiring(m_IsFiring);
            }
        }
    }
}

void ClientInterface::checkMouseRelease(Button button, double deltaT)
{
    Super::checkMouseRelease(button, deltaT);

    if (m_IsDead)
        return;

    if (m_PlayerNetworkId == -1)
        return;

    if (button == Button::Left)
    {
        if (m_InputState.LEFT_CLICK_PRESSED)
        {
            m_InputState.LEFT_CLICK_PRESSED = false;
            m_IsFiring = false;
            printf("Stop Firing\n");
            std::dynamic_pointer_cast<Player>(m_Game->GetNetworkObject(m_PlayerNetworkId))->SetIsFiring(m_IsFiring);
        }
    }
}

void ClientInterface::checkKeyRelease(Key key, double deltaT)
{
    Super::checkKeyRelease(key, deltaT);

    if (m_IsDead)
    {
        if (key == Key::Return)
        {
            updateRetryText("Waiting on other player...", sf::Vector2f(14.f, 0.f));
            NetworkInterface::SendBroadcastId((NetworkInterface::msg_id)GameMessagesEx::ID_REMATCH_MESSAGE);
        }
        return;
    }

    if (m_PlayerNetworkId == -1)
        return;

    switch (key)
    {
    case Key::W:
        if (m_InputState.W_KEY_PRESSED)
        {
            m_InputState.W_KEY_PRESSED = false;
            m_InputDirection.y += 1;
            setNetGameObjectVelocityViaInput(m_InputDirection, m_PlayerNetworkId, m_PlayerSpeed);
        }
        break;
    case Key::S:
        if (m_InputState.S_KEY_PRESSED)
        {
            m_InputState.S_KEY_PRESSED = false;
            m_InputDirection.y += -1;
            setNetGameObjectVelocityViaInput(m_InputDirection, m_PlayerNetworkId, m_PlayerSpeed);
        }
        break;
    case Key::D:
        if (m_InputState.D_KEY_PRESSED)
        {
            m_InputState.D_KEY_PRESSED = false;
            m_InputDirection.x += -1;
            setNetGameObjectVelocityViaInput(m_InputDirection, m_PlayerNetworkId, m_PlayerSpeed);
        }
        break;
    case Key::A:
        if (m_InputState.A_KEY_PRESSED)
        {
            m_InputState.A_KEY_PRESSED = false;
            m_InputDirection.x += 1;
            setNetGameObjectVelocityViaInput(m_InputDirection, m_PlayerNetworkId, m_PlayerSpeed);
        }
        break;
    case Key::Insert:
        if (m_InputState.INSERT_KEY_PRESSED)
        {
            m_InputState.INSERT_KEY_PRESSED = false;
            m_Game->SetDebugMode(!m_Game->GetDebugMode());
        }
        break;
    default:
        break;
    }
}

void ClientInterface::checkKeyPress(Key key, double deltaT)
{
    Super::checkKeyPress(key, deltaT);

    if (m_IsDead)
        return;

    if (m_PlayerNetworkId == -1)
        return;

    // Movement data

    sf::Vector2f dir;

    switch (key)
    {
    case Key::W:
        if (!m_InputState.W_KEY_PRESSED)
        {
            m_InputState.W_KEY_PRESSED = true;
            m_InputDirection.y += -1;
            setNetGameObjectVelocityViaInput(m_InputDirection, m_PlayerNetworkId, m_PlayerSpeed);
        }
        break;
    case Key::S:
        if (!m_InputState.S_KEY_PRESSED)
        {
            m_InputState.S_KEY_PRESSED = true;
            m_InputDirection.y += 1;
            setNetGameObjectVelocityViaInput(m_InputDirection, m_PlayerNetworkId, m_PlayerSpeed);
        }
        break;
    case Key::D:
        if (!m_InputState.D_KEY_PRESSED)
        {
            m_InputState.D_KEY_PRESSED = true;
            m_InputDirection.x += 1;
            setNetGameObjectVelocityViaInput(m_InputDirection, m_PlayerNetworkId, m_PlayerSpeed);
        }
        break;
    case Key::A:
        if (!m_InputState.A_KEY_PRESSED)
        {
            m_InputState.A_KEY_PRESSED = true;
            m_InputDirection.x += -1;
            setNetGameObjectVelocityViaInput(m_InputDirection, m_PlayerNetworkId, m_PlayerSpeed);
        }
        break;
    }
}


void ClientInterface::checkMousePos(double deltaT)
{
    if (m_PlayerNetworkId == -1)
        return;

    sf::Vector2i mousePos = sf::Mouse::getPosition(*m_Window);

    sf::Vector2f dir = sf::Vector2f((float)mousePos.x, (float)mousePos.y) - m_Game->GetNetworkObject(m_PlayerNetworkId)->GetPosition();

    Utils::normalize(dir.x, dir.y);

    m_Game->GetNetworkObject(m_PlayerNetworkId)->SetBox2DRotationRads(atan2f(dir.y, dir.x));

    UpdatePlayerRotationMessage msg;
    msg.MsgId = (uchar)GameMessagesEx::ID_UPDATE_PLAYER_ROTATION_MESSAGE;
    msg.rotation = m_Game->GetNetworkObject(m_PlayerNetworkId)->GetBox2DRotationRads();
    msg.networkId = m_PlayerNetworkId;
    NetworkInterface::SendBroadcast(msg);
}


void ClientInterface::onNetworkEvent(Events::Event* e)
{
    Super::onNetworkEvent(e);
    Events::NetworkEvent* ev = (Events::NetworkEvent*)e;

    switch (ev->GetMsgId())
    {
    case GameMessages::ID_SEND_NETWORK_OBJECT_ID_MESSAGE:
    {
        PassNetworkObjectMessage msg;
        getNetworkedGameObjectID(ev->GetMsg(), ev->GetSize(), msg);
        m_PlayerNumber = msg.type;

        uint offset = m_PlayerScreenOffset;

        m_PlayerNetworkId = msg.netId;
        m_ClientState = WAITING_FOR_INITIAL_SPAWN;

        switch (msg.type)
        {
        case (int)GameObjectTypes::PLAYER_1:
        {
            offset *= -1;
            m_Game->SetNetworkID(1000000000);
            break;
        }
        case (int)GameObjectTypes::PLAYER_2:
        {
            m_Game->SetNetworkID(2000000000);
            break;
        }
        }

        GetGame<Asteroids>()->CreatePlayer(m_PlayerNetworkId, msg.type, NetworkInterface::GetSessionGuid(), (float)((m_Window->getSize().x / 2) + offset), (float)(m_Window->getSize().y / 2));
        break;
    }
    case GameMessagesEx::ID_UPDATE_PLAYER_HEALTH_MESSAGE:
    {
        UpdatePlayerHealthMessage msg = *(UpdatePlayerHealthMessage*)ev->GetMsg();
        
        g_EventDispatcher->DispatchEvent(new HealthEvent(HealthEvent::UPDATE_HEALTH, msg.player, msg.health));
        break;
    }
    case GameMessagesEx::ID_UPDATE_PLAYER_SCORE_MESSAGE:
    {
        UpdatePlayerScoreMessage msg = *(UpdatePlayerScoreMessage*)ev->GetMsg();

        g_EventDispatcher->DispatchEvent(new ScoreEvent(ScoreEvent::UPDATE_SCORE, msg.player, msg.score));
        break;
    }
    case GameMessagesEx::ID_UPDATE_LEVEL_MESSAGE:
    {
        UpdateLevelMessage msg = *(UpdateLevelMessage*)ev->GetMsg();

        g_EventDispatcher->DispatchEvent(new LevelEvent(msg.level));
        break;
    }
    case GameMessagesEx::ID_START_GAME_MESSAGE:
    {
        m_StartCountdown = 0.0;
        updateCountdownText();
        break;
    }
    case GameMessagesEx::ID_START_COUNTDOWN_MESSAGE:
    {
        m_StartCountdown = COUNT_DOWN_TIME;
        break;
    }
    case GameMessagesEx::ID_KILL_PLAYER_MESSAGE:
    {
        KillPlayerMessage msg = *(KillPlayerMessage*)ev->GetMsg();

        if (m_PlayerNumber == msg.player)
        {
            m_IsDead = true;
            m_PlayerNetworkId = -1;
        }
        break;
    }
    case GameMessagesEx::ID_GAME_OVER_MESSAGE:
    {
        m_GameLost = true;

        GameOverMessage msg = *(GameOverMessage*)ev->GetMsg();

        updateWinnerText(msg.winner, msg.winnerScore);
        updateLoserText(msg.loser, msg.loserScore);

        break;
    }
    case GameMessagesEx::ID_REMATCH_MESSAGE:
    {
        updateRetryText("The other player wants to play more.", sf::Vector2f(4.5f, 0.f));
        break;
    }
    case GameMessagesEx::ID_RESTART_GAME_MESSAGE:
    {
        updateRetryText("");
        RestartGame();
        break;
    }
    case GameMessagesEx::ID_UPDATE_PLAYER_ROTATION_MESSAGE:
    {
        UpdatePlayerRotationMessage msg = *(UpdatePlayerRotationMessage*)ev->GetMsg();

        if (m_Game->IsNetIdNew(msg.networkId))
            break;

        m_Game->GetNetworkObject(msg.networkId)->SetBox2DRotationRads(msg.rotation);
        break;
    }
    }
}

void ClientInterface::update(double deltaT)
{
    Super::update(deltaT);

    if (m_StartCountdown > 0)
    {
        m_StartCountdown -= deltaT;
        printf("Time until start: %f\n", m_StartCountdown);

        if (m_StartCountdown <= 0)
        {
            m_StartCountdown = 0;
        }

        updateCountdownText();
    }

    switch (m_ClientState)
    {
    case ClientState::SPECTATING:
        break;
    case ClientState::WAITING_FOR_INITIAL_SPAWN:
        if (m_Game->NetworkMapContains(m_PlayerNetworkId))
        {
            m_ClientState = ClientState::ALIVE;
        }
        break;
    case ClientState::ALIVE:       
        break;
    }
}

void ClientInterface::preDraw()
{
    Super::preDraw();
}

void ClientInterface::postDraw()
{
    Super::postDraw();

    if (m_CountDownText.getString() != "" && m_CountDownText.getString() != "0.00")
        m_Window->draw(m_CountDownText);

    if (m_GameLost)
    {
        m_Window->draw(m_LoseBackground);
        m_Window->draw(m_RetryText);
        m_Window->draw(m_WinnerText);
        m_Window->draw(m_LoserText);
    }
}

void ClientInterface::updateCountdownText()
{
    if (m_StartCountdown > 0)
    {
        std::string text = std::to_string(m_StartCountdown);
        size_t index = text.find('.');

        text = text.substr(0, index + 3);

        m_CountDownText.setString(text);
        sf::FloatRect rect = m_CountDownText.getGlobalBounds();
        m_CountDownText.setPosition(640.f - rect.width / 2.f, 20.f);
    }
    else
    {
        m_CountDownText.setString("");
    }
}

void ClientInterface::updateRetryText(std::string text, const sf::Vector2f& offset)
{
    m_RetryText.setString(text);
    sf::FloatRect rect = m_RetryText.getGlobalBounds();
    m_RetryText.setPosition(640.f - rect.width / 2.f + offset.x, 640.f + offset.y);
}

void ClientInterface::updateLoserText(int loser, int score)
{
    std::string value = "Player " + std::to_string(loser) + " - " + std::to_string(score);
    m_LoserText.setString(value);
    sf::FloatRect rect = m_LoserText.getGlobalBounds();
    m_LoserText.setPosition(640.f - rect.width / 2.f, 172.0f);
}

void ClientInterface::updateWinnerText(int winner, int score)
{
    std::string value = "Player " + std::to_string(winner) + " - " + std::to_string(score);
    m_WinnerText.setString(value);
    sf::FloatRect rect = m_WinnerText.getGlobalBounds();
    m_WinnerText.setPosition(640.f - rect.width / 2.f, 68.0f);
}

void ClientInterface::onSpawnProjectile(Events::Event* e)
{
    ProjectileEvent* ev = (ProjectileEvent*)e;

    GetGame<Asteroids>()->CreateProjectile(ev->GetType(), ev->GetOwner(), ev->GetSpawnPos(), ev->GetVelocity());
}