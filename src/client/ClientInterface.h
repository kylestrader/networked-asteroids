#pragma once

#include "galaxyengine/GameInterface.h"

enum ClientState
{
    SPECTATING = 0,
    WAITING_FOR_INITIAL_SPAWN,
    ALIVE,
    DEAD
};

namespace Events
{
    class Event;
    class Delegate;
}

class ClientInterface : public GameInterface
{
public:
    typedef GameInterface Super;
    typedef sf::Keyboard::Key Key;
    typedef sf::Mouse::Button Button;

    const double COUNT_DOWN_TIME; // In Seconds

private:
    sf::Text m_CountDownText;
    double m_StartCountdown;
    bool m_IsDead;
    int m_PlayerNumber;
    uint m_PlayerScreenOffset;

    Events::Delegate* m_SpawnProjectileDelegate;

    bool m_IsFiring;

    bool m_GameLost;
    sf::Sprite m_LoseBackground;
    sf::Text m_RetryText;
    sf::Text m_WinnerText;
    sf::Text m_LoserText;

private:
    void updateCountdownText();
    void updateRetryText(std::string text, const sf::Vector2f& offset = sf::Vector2f(0.f, 0.f));
    void updateWinnerText(int winner, int score);
    void updateLoserText(int loser, int score);

    void onSpawnProjectile(Events::Event* e);

protected:
    virtual void createGame() override; // Instantiate m_Game here
    virtual void initializeTextures() override; // Setup the texture loader here
    virtual void initializeGame() override; // Initialize your game here
    virtual void loadAssets(std::string assetDir = "content/", bool secondAttempt = false) override;

    virtual void disposeGame() override; // Dispose of your game here

    virtual void checkMousePress(Button button, double deltaT) override;
    virtual void checkMouseRelease(Button button, double deltaT) override;
    virtual void checkKeyRelease(Key key, double deltaT) override;
    virtual void checkKeyPress(Key key, double deltaT) override;
    virtual void checkMousePos(double deltaT) override;
    virtual void onNetworkEvent(Events::Event* e) override;

    virtual void update(double deltaT) override;
    virtual void preDraw() override;
    virtual void postDraw() override;

private:
    void CalculatePlayerVelocity(sf::Vector2f dirVel);

public:
    ClientInterface();
    ~ClientInterface();

private:
    network_id m_PlayerNetworkId;
    ClientState m_ClientState;
    sf::Vector2i m_InputDirection;
};