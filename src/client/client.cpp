#include "galaxyengine/Game.h"
#include <memory>
#include "ClientInterface.h"

int main(int args, char** argv)
{
    Game game;
    std::shared_ptr<GameInterface> gameInterface;

    gameInterface = std::make_shared<ClientInterface>();
    gameInterface->SetGameName("asteroids");
    
    game.SetFPS(60.0);
    game.SetRakNetFrameDelay(1);

    game.InitializeClient(args, argv, gameInterface);

    game.Update();

    game.Dispose();

    system("pause");
    return 0;
}