/*
    Copyright (C) 2010 Allen Jordan ()
    Copyright (C) 2011 Xabier Larrakoetxea (slok)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    (
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DebugDraw.h"
#include <Box2D/Box2D.h>
#include <iostream>
#include <SFML/System.hpp>
#include "Utils.h"

DebugDraw::DebugDraw(std::shared_ptr<sf::RenderWindow> window, sf::Font font)
{
    m_Window = window;
    m_Font = font;
}

DebugDraw::~DebugDraw()
{

}

//convert a Box2D (float 0.0f - 1.0f range) color to a SFML color (uint8 0 - 255 range)
sf::Color DebugDraw::B2SFColor(const b2Color &color, int alpha = 255)
{
    sf::Color result((sf::Uint8)(color.r * 255), (sf::Uint8)(color.g * 255), (sf::Uint8)(color.b * 255), (sf::Uint8) alpha);
    return result;
}

void DebugDraw::DrawAABB(b2AABB* aabb, const b2Color& color)
{
    sf::ConvexShape polygon;
    polygon.setPointCount(4);

    polygon.setPoint(0, sf::Vector2f(Utils::b2_MetersToPixels(aabb->lowerBound.x), Utils::b2_MetersToPixels(aabb->lowerBound.y)));
    polygon.setPoint(1, sf::Vector2f(Utils::b2_MetersToPixels(aabb->upperBound.x), Utils::b2_MetersToPixels(aabb->lowerBound.y)));
    polygon.setPoint(2, sf::Vector2f(Utils::b2_MetersToPixels(aabb->upperBound.x), Utils::b2_MetersToPixels(aabb->upperBound.y)));
    polygon.setPoint(3, sf::Vector2f(Utils::b2_MetersToPixels(aabb->lowerBound.x), Utils::b2_MetersToPixels(aabb->upperBound.y)));
    polygon.setOutlineColor(B2SFColor(color));
    polygon.setFillColor(B2SFColor(color, 50));
    polygon.setOutlineThickness(1.0f);

    m_Window->draw(polygon);
    //std::cout << "DrawAABB\n";
}


void DebugDraw::DrawString(int x, int y, const char* string)
{
    sf::Text fpsText;
    fpsText.setFont(m_Font);
    fpsText.setCharacterSize(15);
    fpsText.setPosition((float)x, (float)y);
    fpsText.setString(string);
    m_Window->draw(fpsText);

    //std::cout << "DrawString\n";
}

void DebugDraw::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color)
{
    //std::cout << "DrawPoint\n";
}

void DebugDraw::DrawTransform(const b2Transform& xf)
{
    float x, 
          y, 
          lineProportion;

    x = Utils::b2_MetersToPixels(xf.p.x);
    y = Utils::b2_MetersToPixels(xf.p.y);
    lineProportion = 0.15f; // 0.15 ~ 10 pixels
    b2Vec2 p1 = xf.p, p2;

    b2Vec2 col1,
           col2;
    col1.x = xf.q.c;
    col1.y = xf.q.s;
    col2.x = -xf.q.s;
    col2.y = xf.q.c;

    //red (X axis)
    p2 = p1 + (lineProportion *col1);

    sf::Vertex redLine[] =
    {
        sf::Vertex(sf::Vector2f(Utils::b2_MetersToPixels(p1.x), Utils::b2_MetersToPixels(p1.y)), sf::Color::Red),
        sf::Vertex(sf::Vector2f(Utils::b2_MetersToPixels(p2.x), Utils::b2_MetersToPixels(p2.y)), sf::Color::Red)
    };

    //green (Y axis)
    p2 = p1 - (lineProportion * col2);

    sf::Vertex greenLine[] =
    {
        sf::Vertex(sf::Vector2f(Utils::b2_MetersToPixels(p1.x), Utils::b2_MetersToPixels(p1.y)), sf::Color::Green),
        sf::Vertex(sf::Vector2f(Utils::b2_MetersToPixels(p2.x), Utils::b2_MetersToPixels(p2.y)), sf::Color::Green)
    };

    m_Window->draw(greenLine, 2, sf::Lines);
    m_Window->draw(redLine, 2, sf::Lines);

    //std::cout << "DrawTransform\n";
}

void DebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{
    sf::Vertex line[] =
    {
        sf::Vertex(sf::Vector2f(Utils::b2_MetersToPixels(p1.x), Utils::b2_MetersToPixels(p1.y)), B2SFColor(color)),
        sf::Vertex(sf::Vector2f(Utils::b2_MetersToPixels(p2.x), Utils::b2_MetersToPixels(p2.y)), B2SFColor(color))
    };

    m_Window->draw(line, 2, sf::Lines);

    //std::cout << "DrawSegment\n";
}

void DebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{
    sf::CircleShape circle(Utils::b2_MetersToPixels(radius));
    circle.setPosition(Utils::b2_MetersToPixels(center.x), Utils::b2_MetersToPixels(center.y));
    circle.setFillColor(B2SFColor(color, 50));
    circle.setOutlineColor(B2SFColor(color));
    circle.setOutlineThickness(1.0f);
    circle.setOrigin(Utils::b2_MetersToPixels(radius), Utils::b2_MetersToPixels(radius));

    // line of the circle which shows the angle
    b2Vec2 p = center + (radius * axis);

    sf::Vertex line[] =
    {
        sf::Vertex(sf::Vector2f(Utils::b2_MetersToPixels(center.x), Utils::b2_MetersToPixels(center.y)), B2SFColor(color)),
        sf::Vertex(sf::Vector2f(Utils::b2_MetersToPixels(p.x), Utils::b2_MetersToPixels(p.y)), B2SFColor(color))
    };

    m_Window->draw(circle);
    m_Window->draw(line, 2, sf::Lines);

    //std::cout << "DrawSolidCircle\n";
}

void DebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
    sf::CircleShape circle(Utils::b2_MetersToPixels(radius));
    circle.setPosition(Utils::b2_MetersToPixels(center.x), Utils::b2_MetersToPixels(center.y));
    circle.setFillColor(B2SFColor(color, 0));
    circle.setOutlineColor(B2SFColor(color));
    circle.setOutlineThickness(1.0f);
    circle.setOrigin(Utils::b2_MetersToPixels(radius), Utils::b2_MetersToPixels(radius));

    m_Window->draw(circle);

    //std::cout << "DrawCircle\n";
}

void DebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
    sf::ConvexShape polygon;
    polygon.setPointCount(vertexCount);
    for (int32 i = 0; i < vertexCount; i++)
    {
        b2Vec2 vertex = vertices[i];
        polygon.setPoint(i, sf::Vector2f(Utils::b2_MetersToPixels(vertex.x), Utils::b2_MetersToPixels(vertex.y)));
    }

    polygon.setFillColor(B2SFColor(color, 50));
    polygon.setOutlineColor(B2SFColor(color));
    polygon.setOutlineThickness(1.0f);
    m_Window->draw(polygon);

    //std::cout << "DrawSolidPolygon\n";
}

void DebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
    sf::ConvexShape polygon;
    polygon.setPointCount(vertexCount);
    for (int32 i = 0; i < vertexCount; i++)
    {
        b2Vec2 vertex = vertices[i];
        polygon.setPoint(i, sf::Vector2f(Utils::b2_MetersToPixels(vertex.x), Utils::b2_MetersToPixels(vertex.y)));
    }

    polygon.setFillColor(B2SFColor(color, 0));
    polygon.setOutlineColor(B2SFColor(color));
    polygon.setOutlineThickness(1.0f);
    m_Window->draw(polygon);

    //std::cout << "DrawPolygon\n";
}


void DebugDraw::DrawMouseJoint(b2Vec2& p1, b2Vec2& p2, const b2Color &boxColor, const b2Color &lineColor)
{
    sf::ConvexShape polygon;
    sf::ConvexShape polygon2;
    float p1x = Utils::b2_MetersToPixels(p1.x);
    float p1y = Utils::b2_MetersToPixels(p1.y);
    float p2x = Utils::b2_MetersToPixels(p2.x);
    float p2y = Utils::b2_MetersToPixels(p2.y);
    float size = 4.0f;

    sf::Color boxClr = this->B2SFColor(boxColor);
    sf::Color lineClr = this->B2SFColor(lineColor);

    //first green box for the joint
    polygon.setPointCount(4);
    polygon.setPoint(0, sf::Vector2f(p1x - size / 2, p1y - size / 2));
    polygon.setPoint(1, sf::Vector2f(p1x + size / 2, p1y - size / 2));
    polygon.setPoint(2, sf::Vector2f(p1x + size / 2, p1y + size / 2));
    polygon.setPoint(3, sf::Vector2f(p1x - size / 2, p1y + size / 2));
    polygon.setFillColor(boxClr);

    //second green box for the joint
    polygon2.setPointCount(4);
    polygon2.setPoint(0, sf::Vector2f(p2x - size / 2, p2y - size / 2));
    polygon2.setPoint(1, sf::Vector2f(p2x + size / 2, p2y - size / 2));
    polygon2.setPoint(2, sf::Vector2f(p2x + size / 2, p2y + size / 2));
    polygon2.setPoint(3, sf::Vector2f(p2x - size / 2, p2y + size / 2));
    polygon2.setFillColor(boxClr);

    sf::Vertex line[] =
    {
        sf::Vertex(sf::Vector2f(p1x, p1y), lineClr),
        sf::Vertex(sf::Vector2f(p2x, p2y), lineClr)
    };

    m_Window->draw(polygon);
    m_Window->draw(polygon2);
    m_Window->draw(line, 2, sf::Lines);

    //std::cout << "DrawMouseJoint\n";
}