/*
    Copyright (C) 2011 Allen Jordan ()
    Copyright (C) 2011 Xabier Larrakoetxea (slok)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    (
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * I pulled this code from github here: https://github.com/slok/Box2D-and-SFML-demo
 * I extensively modified it to improve it. It now uses forward declaration and my utility functions
 * to make the code more readable and easily changeable down the road. I also updated some old SFML and
 * Box2D code to be up to date with current versions of these libraries.
 * I take no credit in the implementation.
 * - Vincent Loignon
 **/

#pragma once

#include <SFML/Graphics.hpp>
#include <memory>
#include <Box2D/Common/b2Draw.h>

struct b2Vec2;
struct b2Color;
struct b2Transform;
struct b2AABB;

class DebugDraw : public b2Draw
{
public:
    DebugDraw(std::shared_ptr<sf::RenderWindow> window, sf::Font font);
    virtual ~DebugDraw();

    void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
    void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
    void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
    void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
    void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
    void DrawTransform(const b2Transform& xf);
    void DrawPoint(const b2Vec2& p, float32 size, const b2Color& color);
    void DrawString(int x, int y, const char* string);
    void DrawAABB(b2AABB* aabb, const b2Color& color);
    sf::Color B2SFColor(const b2Color &color, int alpha);
    void DrawMouseJoint(b2Vec2& p1, b2Vec2& p2, const b2Color &boxColor, const b2Color &lineColor);

private:
    std::shared_ptr<sf::RenderWindow> m_Window;
    sf::Font m_Font;
};
