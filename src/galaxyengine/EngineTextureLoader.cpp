#include "EngineTextureLoader.h"

const std::string EngineTextures::ENGINE_LOGO = "engine_logo";

void EngineTextureLoader::loadAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */)
{
    sf::Texture engineLogo;

    // Put all asset loading in this try and ALWAYS throw if it fails
    try
    {
        if (!engineLogo.loadFromFile(assetDir + "GalaxyEngine.png", sf::IntRect(0, 0, 50, 0)))
            throw 0;
    }
    catch (...)
    {
        if (!secondAttempt)
        {
            printf("Unable to find an engine texture, trying another directory...\n");
            loadAssets("../content/", true);
            return;
        }
    }

    AddTexture(textures::ENGINE_LOGO, engineLogo);
}