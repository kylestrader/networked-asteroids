// Do not use this in your game, it is purely for the engine

#pragma once

#include "TextureLoader.h"

class EngineTextures
{
public:
    static const std::string ENGINE_LOGO;
};

class EngineTextureLoader : public TextureLoader
{
public:
    typedef EngineTextures textures;

protected:
    virtual void loadAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */) override;
};