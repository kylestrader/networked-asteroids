#include "Event.h"

using namespace Events;

/*   Purpose:  Constructor
 *       Pre:  EventType type
 *      Post:  None
 ****************************************************************/
Event::Event(Events::EventType type)
{
	m_Type = type;
}

/*   Purpose:  Destructor
 *       Pre:  None
 *      Post:  None
 ****************************************************************/
Event::~Event()
{
	Dispose();
}

/*   Purpose:  This will clean up any memory for the Event
 *       Pre:  None
 *      Post:  None
 ****************************************************************/
void Event::Dispose()
{

}