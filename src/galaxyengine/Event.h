#pragma once

#include "IObject.h"
#include "EventType.h"

namespace Events
{

class Event : public IObject
{
protected:
    Events::EventType m_Type;

public:
    Event(Events::EventType type); // Constructor
    ~Event(); // Destructor

    // Accessors
    Events::EventType GetType() const { return m_Type; }

    virtual void Dispose();
};

}