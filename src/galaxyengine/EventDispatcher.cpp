#include "EventDispatcher.h"
#include "Event.h"
#include "Delegate.h"

using namespace Events;

// Global EventDispatcher Variable
EventDispatcher* g_EventDispatcher = nullptr;

/*   Purpose:  Constructor
 *       Pre:  None
 *      Post:  None
 ****************************************************************/
EventDispatcher::EventDispatcher()
{
	
}

/*   Purpose:  Destructor
 *       Pre:  None
 *      Post:  None
 ****************************************************************/
EventDispatcher::~EventDispatcher()
{
	
}

/*   Purpose:  This will clean up any memory associated with the Event System
*       Pre:  None
*      Post:  None
****************************************************************/
void EventDispatcher::Dispose()
{
	std::multimap<EventType, Delegate*>::iterator it;

	for (it = m_Events.begin(); it != m_Events.end(); it++)
	{
		delete it->second;
		it->second = NULL;
	}

	m_Events.clear();
}

/*   Purpose:  This will dispatch the event passed to all listeners for it
 *       Pre:  Event* ev
 *      Post:  None
 ****************************************************************/
void EventDispatcher::dispatch(Event* ev)
{
	std::pair<std::multimap<EventType, Delegate*>::iterator, std::multimap<EventType, Delegate*>::iterator> ret;
	ret = m_Events.equal_range(ev->GetType());

	std::multimap<EventType, Delegate*>::iterator it;

	for (it = ret.first; it != ret.second; it++)
	{
		it->second->Invoke(ev);
	}

	ev->Dispose();
	delete ev;
	ev = NULL;
}

/*   Purpose:  This will dispatch the event passed to all listeners for it
 *       Pre:  Event* ev
 *      Post:  None
 ****************************************************************/
void EventDispatcher::DispatchEvent(Event* ev)
{
	dispatch(ev);
}

/*   Purpose:  This will add an Event Listener to the multimap for the event system
 *       Pre:  EventType type, Delegate* handler
 *      Post:  None
 ****************************************************************/
void EventDispatcher::AddEventListener(EventType type, Delegate* handler)
{
	m_Events.insert(std::pair<EventType, Delegate*>(type, handler));
}

/*   Purpose:  This will remove a specific Event Listener from the event system
 *       Pre:  EventType type, Delegate* handler, bool cleanUpDelegate
 *      Post:  None
 ****************************************************************/
void EventDispatcher::RemoveEventListener(EventType type, Delegate* handler, bool cleanUpDelegate /* = true */)
{
	std::pair<std::multimap<EventType, Delegate*>::iterator, std::multimap<EventType, Delegate*>::iterator> ret;
	ret = m_Events.equal_range(type);

	std::multimap<EventType, Delegate*>::iterator it;

	for (it = ret.first; it != ret.second; it++)
	{
		if (it->second == handler)
		{
			if (cleanUpDelegate)
			{
				delete it->second;
				it->second = NULL;
			}

			m_Events.erase(it);
			break;
		}
	}
}

/*   Purpose:  This will remove all Event Listeners of a specific type
 *       Pre:  EventType type, bool cleanUpDelegate
 *      Post:  None
 ****************************************************************/
void EventDispatcher::RemoveEventListeners(EventType type, bool cleanUpDelegate /* = true */)
{
	std::pair<std::multimap<EventType, Delegate*>::iterator, std::multimap<EventType, Delegate*>::iterator> ret;
	ret = m_Events.equal_range(type);

	std::multimap<EventType, Delegate*>::iterator it;

	if (cleanUpDelegate)
	{
		for (it = ret.first; it != ret.second; it++)
		{
			delete it->second;
			it->second = NULL;
		}
	}

	m_Events.erase(type);
}

/*   Purpose:  This will remove all Event Listeners attached to the specific Delegate
 *       Pre:  Delegate* handler, bool cleanUpDelegate
 *      Post:  None
 ****************************************************************/
void EventDispatcher::RemoveEventListenerAll(Delegate* handler, bool cleanUpDelegate /* = true */)
{
	std::multimap<EventType, Delegate*>::iterator it;
	
	for (it = m_Events.begin(); it != m_Events.end(); it++)
	{
		if (it->second == handler)
		{
			if (cleanUpDelegate)
			{
				delete it->second;
				it->second = NULL;
			}

			m_Events.erase(it);
		}
	}
}