#pragma once

#include <map>
#include "IObject.h"
#include "EventType.h"

namespace Events
{
class Event;
class Delegate;

class EventDispatcher : public IObject
{
protected:
    std::multimap<Events::EventType, Events::Delegate*> m_Events;

    void dispatch(Events::Event* ev);

public:
    EventDispatcher(); // Constructor
    ~EventDispatcher(); // Destructor

    // Functionality
    void DispatchEvent(Events::Event* ev);
    void AddEventListener(Events::EventType type, Events::Delegate* handler);
    void RemoveEventListener(Events::EventType type, Events::Delegate* handler, bool cleanUpDelegate = true);
    void RemoveEventListeners(Events::EventType type, bool cleanUpDelegate = true);
    void RemoveEventListenerAll(Events::Delegate* handler, bool cleanUpDelegate = true);

    virtual void Dispose();
};

extern EventDispatcher* g_EventDispatcher;
}

extern Events::EventDispatcher* g_EventDispatcher;