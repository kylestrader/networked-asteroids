#pragma once

#include <string>

namespace Events
{

typedef std::string EventType;

const EventType INVALID_EVENT_TYPE = "INVALID_EVENT_TYPE";
const EventType KEY_PRESSED = "KEY_PRESSED";
const EventType KEY_RELEASED = "KEY_RELEASED";
const EventType MOUSE_CLICK = "MOUSE_CLICK";
const EventType MOUSE_RELEASED = "MOUSE_RELEASED";
const EventType MACRO_EVENT = "MACRO_EVENT";
const EventType NETWORK_EVENT = "NETWORK_EVENT";

}