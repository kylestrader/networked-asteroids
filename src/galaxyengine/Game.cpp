#include "Game.h"
#include "GameInterface.h"
#include "Timer.h"
#include "NetworkInterface.h"
#include "GameInterface.h"
#include <array>
#include <iostream>

Game::Game()
{
    SetFPS(60.0);
    SetRakNetFrameDelay(1);
}

Game::~Game()
{
    
}

void Game::InitializeClient(int args, char** argv, std::shared_ptr<GameInterface> gameInterface)
{
    NetworkInterface::Initialize();

    std::array<char, 512> str;
    std::array<char, 512> str2;
    std::array<char, 512> str3;
    bool useArguments = false;

    if (args == 1)
        useArguments = false;
    else if (args == 4)
    {
        NetworkInterface::ConnectAsClient(argv[2], sizeof(argv[2]), atoi(argv[3]), atoi(argv[1]));
        useArguments = true;
    }
    else
    {
        printf("Usage: client\n");
        printf("Usage: client <ClientPortNumber> <ServerAddress> <SeverPortNumber>\n");
        useArguments = false;
    }

    if (!useArguments)
    {
        printf("Enter server IP or hit enter for default: 127.0.0.1\n");
        gets_s(str.data(), str.size());

        printf("Enter server port or hit enter for default: %i\n", SERVER_PORT);
        gets_s(str2.data(), str2.size());

        printf("Enter client port or hit enter for default: %i\n", CLIENT_PORT);
        gets_s(str3.data(), str3.size());

        NetworkInterface::ConnectAsClient(str.data(), str.size(), atoi(str2.data()), atoi(str3.data()));
    }

    initialize(gameInterface);
}

void Game::InitializeServer(int args, char** argv, std::shared_ptr<GameInterface> gameInterface)
{
    NetworkInterface::Initialize();

    std::array<char, 512> str;
    bool useArguments = false;

    if (args == 1)
        useArguments = false;
    else if (args == 2)
    {
        NetworkInterface::ConnectAsServer(atoi(argv[1]));
        useArguments = true;
    }
    else
    {
        printf("Usage: server\n");
        printf("Usage: server <ServerPortNumber>\n");
        useArguments = false;
    }

    if (!useArguments)
    {
        printf("Enter server port or hit enter for default: %i\n", SERVER_PORT);
        gets_s(str.data(), str.size());

        NetworkInterface::ConnectAsServer(atoi(str.data()));
    }

    initialize(gameInterface);
}

void Game::initialize(std::shared_ptr<GameInterface> gameInterface)
{
    srand((unsigned int)time(NULL));

    m_Interface = gameInterface;
    m_Interface->Initialize(NetworkInterface::IsServer());
    
    m_Timer = std::make_unique<Timer>();
}

void Game::SetFPS(double fps)
{
    if (fps <= 0)
        return;

    m_FPS = fps;
    m_SEC_PER_FRAME = TimerHelper::MSToSec(1000.0 / m_FPS);
}

void Game::SetRakNetFrameDelay(int delay)
{
    m_RAKNET_UPDATE_FRAME_DELAY = delay;
}

void Game::Update()
{
    double elapsed = 0.0;

    // FPS counter
    // Source: http://gamedev.stackexchange.com/questions/83159/simple-framerate-counter
    double fps = 0.0;
    int fpsCounter = 0;
    double fpsElapsed = 0.0;

    // Raknet
    int raknetCounter = 0;

    while (!m_Interface->ShouldExitGame())
    {
        m_Timer->Start();
        fpsCounter++;

        // Update the fps every quarter second to be more responsive
        if (fpsElapsed > 0.25 && fpsCounter > 10)
        {
            fps = (double)fpsCounter / fpsElapsed;
            fpsCounter = 0;
            fpsElapsed = 0.0;
            m_Interface->SetFPS(fps);
        }

        // Receive incoming packets, process them as needed
        if (raknetCounter++ >= m_RAKNET_UPDATE_FRAME_DELAY)
        {
            raknetCounter = 0;
            NetworkInterface::UpdatePacketCheck();
        }

        // Update the game
        m_Interface->UpdateGame(elapsed);

        m_Timer->Stop();
        elapsed = m_Timer->GetTotalTime_s();

        // If we have extra time, sleep for the remainder. we want to keep a constant 60fps
        if (elapsed < m_SEC_PER_FRAME)
            elapsed += m_Timer->Sleep_s(m_SEC_PER_FRAME - elapsed);
        fpsElapsed += elapsed;
    }

    m_Timer->Stop();
}

void Game::Dispose()
{
    m_Timer.reset();
    m_Timer = nullptr;

    m_Interface.reset();
    m_Interface = nullptr;

    NetworkInterface::DestroyPeerInstance();
}