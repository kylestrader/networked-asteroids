#pragma once

#include <memory>

class GameInterface;
class Timer;

class Game
{
private:
    std::unique_ptr<Timer> m_Timer;
    std::shared_ptr<GameInterface> m_Interface;
    double m_FPS; // Do not touch, call SetFPS instead
    double m_SEC_PER_FRAME; // Do not touch
    int m_RAKNET_UPDATE_FRAME_DELAY;

private:
    void initialize(std::shared_ptr<GameInterface> gameInterface);

public:
    Game();
    ~Game();

    void SetFPS(double fps);
    void SetRakNetFrameDelay(int delay);

    void InitializeServer(int args, char** argv, std::shared_ptr<GameInterface> gameInterface);
    void InitializeClient(int args, char** argv, std::shared_ptr<GameInterface> gameInterface);

    void Update();
    void Dispose();
};