#include "GameInterface.h"
#include "NetworkInterface.h"
#include "Utils.h"
#include "SoundManager.h"
#include "Event.h"
#include "NetworkEvent.h"
#include "Delegate.h"
#include "EventDispatcher.h"
#include "EngineTextureLoader.h"
#include "NetworkedGame.h"
#include <iostream>
#include "NetworkMessageHelper.h"
#include "NetworkedGame.h"
#include "NetworkedGameObject.h"
#include "asteroids/GameMessages.h"

GameInterface::GameInterface()
    : COUNT_DOWN_TIME(3.0f)
{
    m_ExitGame = false;
    m_Window = nullptr;
    m_Game = nullptr;
    m_IsServer = false;
    m_NetworkEventDelegate = nullptr;
    m_PreviousDeltaT = 0.0;

    m_IsEngineLogoFading = false;
    m_ShowEngineLogo = true;
    m_CanSendNetworkedMessages = false;

    SetThreshold(5.0);

    //Source Multiplayer Networking: https://developer.valvesoftware.com/wiki/Source_Multiplayer_Networking
    SetUpdateInterval(.05);

    m_NextUpdateCounter = UPDATE_INTERVAL;

    m_SoundManager = std::make_unique<SoundManager>();
}

GameInterface::~GameInterface()
{
    if (g_EventDispatcher != nullptr)
    {
        g_EventDispatcher->Dispose();
        delete g_EventDispatcher;
        g_EventDispatcher = nullptr;
    }
}

void GameInterface::Initialize(bool isServer)
{
    g_EventDispatcher = new Events::EventDispatcher();

    m_IsServer = isServer;
    // Setup our event listener for network events
    m_NetworkEventDelegate = Events::Delegate::Create<GameInterface, &GameInterface::onNetworkEvent>(this);
    g_NetworkEventDispatcher->AddEventListener(Events::NETWORK_EVENT, m_NetworkEventDelegate);
    OpenWindow();
}

void GameInterface::SetFPS(double fps)
{
    m_FPS = (int)std::ceil(fps);
    updateTitle();
}

void GameInterface::setNetGameObjectVelocityViaInput(const sf::Vector2i& dir, network_id id, float go_speed)
{
    sf::Vector2f newInput(dir);
    Utils::normalize(newInput.x, newInput.y);

    m_Game->GetNetworkObject(id)->SetVelocity(newInput.x * go_speed, newInput.y * go_speed);
}

void GameInterface::UpdateGame(double deltaT)
{
    m_PreviousDeltaT = deltaT; // We store the deltaT so that events can still use it

    if (m_Window && m_Game)
    {
        if (m_Window->isOpen())
        {
            /*
            This will help the GameInterface know if it can send messages, if it should wait to
            send
            */
            if (m_NextUpdateCounter  > 0)
            {
                m_NextUpdateCounter -= deltaT;

                if (m_NextUpdateCounter <= 0)
                {
                    m_CanSendNetworkedMessages = true;
                    m_NextUpdateCounter = UPDATE_INTERVAL;
                }
                else
                    m_CanSendNetworkedMessages = false;
            }

            //Input Logging (networked/local Input logic)
            sf::Event event;
            while (m_Window->pollEvent(event))
            {
                switch (event.type)
                {
                case sf::Event::Closed:
                    CloseWindow();
                    return;
                    break;
                case sf::Event::MouseButtonPressed:
                    checkMousePress(event.mouseButton.button, deltaT);
                    break;
                case sf::Event::MouseButtonReleased:
                    checkMouseRelease(event.mouseButton.button, deltaT);
                    break;
                case sf::Event::KeyReleased:
                    checkKeyRelease(event.key.code, deltaT);
                    break;
                case sf::Event::KeyPressed:
                    checkKeyPress(event.key.code, deltaT);
                    break;
                }
            }

            checkMousePos(deltaT);

            // Show the logo when the window opens (hold & fading out)
            if (m_ShowEngineLogo)
            {
                if (m_StartCountdown > 0)
                {
                    m_StartCountdown -= deltaT;

                    if (m_IsEngineLogoFading)
                    {
                        sf::Color color = m_EngineLogo.getColor();
                        color.a = (int)Utils::map(m_StartCountdown / (COUNT_DOWN_TIME / 2), 0.0, 1.0, 0.0, 255.0);
                        m_EngineLogo.setColor(color);
                    }

                    if (m_StartCountdown <= 0)
                    {
                        m_StartCountdown = 0;

                        if (m_IsEngineLogoFading)
                        {
                            m_ShowEngineLogo = false;
                        }
                        else
                        {
                            m_IsEngineLogoFading = true;
                            m_StartCountdown = COUNT_DOWN_TIME / 2;
                        }
                    }
                }
            }

            if (m_CanSendNetworkedMessages)
                checkDeadReckoning();

            m_Game->Update(deltaT);
            update(deltaT);

            m_Window->clear();

            if (m_ShowEngineLogo)
                m_Window->draw(m_EngineLogo);
            else
            {
                preDraw();
                m_Game->Draw();
                postDraw();
            }

            m_Window->display();
        }
    }
}

void GameInterface::OpenWindow()
{
    updateTitle();

    m_ExitGame = false;

    initializeTextures();
    loadAssets();
    loadEngineAssets();
    m_EngineLogo.setTexture(EngineTextureLoader::GetTexture(EngineTextures::ENGINE_LOGO), true);
    m_EngineLogo.setPosition(0.f, 0.f);

    m_Window = std::make_shared<sf::RenderWindow>();

    m_Window->create(sf::VideoMode(1280, 720), m_Title, sf::Style::Titlebar | sf::Style::Close);

    createGame();
    m_Game->Initialize(m_Window);

    m_StartCountdown = COUNT_DOWN_TIME;

    initializeGame();
}

void GameInterface::CloseWindow()
{
    m_Window->close();
    m_Window.reset();
    m_Window = nullptr;

    EngineTextureLoader::ClearTextures();

    disposeGame();

    m_ExitGame = true;
    m_IsServer = false;

    NetworkInterface::SendMessageId((NetworkInterface::msg_id)GameMessages::ID_PLAYER_QUIT_MESSAGE, NetworkInterface::GetSessionGuid(), true);
    NetworkInterface::CloseConnection();
}

void GameInterface::RestartGame()
{
    disposeGame();

    createGame();
    m_Game->Initialize(m_Window);
    initializeGame();
}

void GameInterface::onNetworkEvent(Events::Event* e)
{
    Events::NetworkEvent* ev = (Events::NetworkEvent*)e;

    if (ev->GetGUID() == NetworkInterface::GetSessionGuid())
        return;

    switch (ev->GetMsgId())
    {
    case (uchar)GameMessages::ID_UPDATE_NETWORK_OBJECTS_MESSAGE:
        //if (!m_IsServer)
        {
            //printf("UPDATING NETWORK OBJECTS\n");
            updateNetworkedObjects(ev->GetMsg(), ev->GetSize());

            if (m_IsServer)
                NetworkInterface::SendMessage((const char*)ev->GetMsg(), ev->GetSize(), ev->GetGUID(), true);
        }
        break;
    case (uchar)GameMessages::ID_CREATE_NETWORK_OBJECT_MESSAGE:
        //if (!m_IsServer)
        {
            CreateNetworkObjectMessage msg;
            getCreateMessage(ev->GetMsg(), ev->GetSize(), msg);

            m_Game->CreateNetworkObject(msg, (float)UPDATE_INTERVAL);
            printf("Creating Network Object %i\n", msg.netId);
        }
        break;
    case (uchar)GameMessages::ID_DELETE_NETWORK_OBJECT_MESSAGE:
        if (!m_IsServer)
        {
            DeleteNetworkObjectMessage msg;
            getDeleteMessage(ev->GetMsg(), ev->GetSize(), msg);

            m_Game->DeleteNetworkObject(msg.netId);
            printf("Deleting Network Object %i\n", msg.netId);
        }
        break;
    case (uchar)GameMessages::ID_DELETE_MULTIPLE_NETWORK_OBJECTS_MESSAGE:
        if (!m_IsServer)
        {
            DeleteNetworkObjectMessage msg;
            std::vector<network_id> ids;
            getMultipleDeleteMessage(ev->GetMsg(), ev->GetSize(), msg, ids);

            for (size_t i = 0; i < ids.size(); i++)
            {
                m_Game->DeleteNetworkObject(ids[i]);
            }

            printf("Deleting %i Network Objects\n", (int)ids.size());
        }
    }
}

void GameInterface::checkMouseRelease(sf::Mouse::Button button, double deltaT)
{
    if (m_ShowEngineLogo)
        m_ShowEngineLogo = false;
}

void GameInterface::checkMousePress(sf::Mouse::Button button, double deltaT)
{

}

void GameInterface::checkKeyRelease(sf::Keyboard::Key key, double deltaT)
{
    if (m_ShowEngineLogo)
        m_ShowEngineLogo = false;
}

void GameInterface::checkKeyPress(sf::Keyboard::Key key, double deltaT)
{

}

void GameInterface::checkMousePos(double deltaT)
{
    
}

void GameInterface::preDraw()
{

}

void GameInterface::postDraw()
{
    
}

void GameInterface::update(double deltaT)
{

}

void GameInterface::disposeGame()
{
    m_Game.reset();
    m_Game = nullptr;
}

void GameInterface::loadEngineAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */)
{
    // Put all asset loading in this try and ALWAYS throw if it fails
    try
    {
        // Fonts
        if (!m_DefaultFont.loadFromFile(assetDir + "GALACTIC VANGUARDIAN NCV.ttf"))
            throw 0;
    }
    catch (...)
    {
        if (!secondAttempt)
        {
            printf("Unable to find an engine asset, trying another directory...\n");
            loadEngineAssets("../content/", true);
            return;
        }
        else
        {
            printf("Error: Unable to find a engine asset on second attempt.\n\a");
            return;
        }
    }
}

void GameInterface::initializeTextures()
{
    EngineTextureLoader textures;
    textures.LoadTextures();
}

void GameInterface::initializeGame()
{

}

double GameInterface::GetPreviousDeltaT() const
{
    return m_PreviousDeltaT;
}

const sf::Font& GameInterface::GetDefaultFont() const
{
    return m_DefaultFont;
}

bool GameInterface::IsServer() const
{
    return m_IsServer;
}

void GameInterface::SetGameName(std::string name)
{
    m_GameName = name;
    updateTitle();
}

void GameInterface::SetThreshold(double pixels)
{
    THRESHOLD = pixels;
}

void GameInterface::SetUpdateInterval(double value)
{
    UPDATE_INTERVAL = value;
}

void GameInterface::updateTitle()
{
    m_Title = m_GameName;

    if (m_IsServer)
        m_Title += " - Server";
    else
        m_Title += " - Client";

    m_Title += " FPS: " + std::to_string(m_FPS);

    if (m_Window && m_Window->isOpen())
        m_Window->setTitle(m_Title);
}

void GameInterface::getCreateMessage(uchar* msg, uint size, CreateNetworkObjectMessage& obj)
{
    NetworkMessageReader reader(msg, size);

    obj.useTimeStamp = ID_TIMESTAMP;
    obj.timeStamp = reader.GetTime();
    reader.GetId(obj.MsgId);
    reader.GetData(obj.netId);
    reader.GetData(obj.type);
    reader.GetData(obj.guid);
    reader.GetData(obj.x);
    reader.GetData(obj.y);
    reader.GetData(obj.velocityX);
    reader.GetData(obj.velocityY);
    reader.GetData(obj.angularVelocity);
}

void GameInterface::sendCreateMessage(network_id id, int type, guid owner, float x, float y, float velX, float velY, float rot, float angVel)
{
    NetworkMessageBuilder builder;
    builder.AddId((uchar)GameMessages::ID_CREATE_NETWORK_OBJECT_MESSAGE);
    builder.AddData(id);
    builder.AddData(type);
    builder.AddData(owner);
    builder.AddData(x);
    builder.AddData(y);
    builder.AddData(velX);
    builder.AddData(velY);
    builder.AddData(rot);
    builder.AddData(angVel);

    NetworkInterface::SendBroadcast(builder);
}

void GameInterface::getDeleteMessage(uchar* msg, uint size, DeleteNetworkObjectMessage& obj)
{
    NetworkMessageReader reader(msg, size);

    obj.useTimeStamp = ID_TIMESTAMP;
    obj.timeStamp = reader.GetTime();
    reader.GetId(obj.MsgId);
    reader.GetData(obj.netId);
}

void GameInterface::getMultipleDeleteMessage(uchar* msg, uint size, DeleteNetworkObjectMessage& obj, std::vector<network_id>& ids)
{
    NetworkMessageReader reader(msg, size);

    obj.useTimeStamp = ID_TIMESTAMP;
    obj.timeStamp = reader.GetTime();

    uchar msgId;
    uint listSize;
    network_id id;
    reader.GetData(msgId);
    reader.GetData(listSize);

    for (uint i = 0; i < listSize; i++)
    {
        reader.GetData(id);
        ids.push_back(id);
    }
}

void GameInterface::getNetworkedGameObjectID(uchar* msg, uint size, PassNetworkObjectMessage& obj)
{
    NetworkMessageReader reader(msg, size);

    reader.GetData(obj.MsgId);
    reader.GetData(obj.netId);
    reader.GetData(obj.type);
}

void GameInterface::sendDeleteMessage(network_id id)
{
    NetworkMessageBuilder builder;
    builder.AddId((uchar)GameMessages::ID_DELETE_NETWORK_OBJECT_MESSAGE);
    builder.AddData(id);

    NetworkInterface::SendBroadcast(builder);
}

void GameInterface::sendMultipleDeleteMessage(const std::vector<network_id>& ids)
{
    NetworkMessageBuilder builder;
    builder.AddId((uchar)GameMessages::ID_DELETE_MULTIPLE_NETWORK_OBJECTS_MESSAGE);
    builder.AddData((uint)ids.size());

    for (size_t i = 0; i < ids.size(); i++)
    {
        builder.AddData(ids[i]);
    }

    NetworkInterface::SendBroadcast(builder);
}

void GameInterface::updateNetworkedObjects(uchar* msg, uint size)
{
    NetworkMessageReader reader(msg, size);

    uchar id;
    int arraySize;
    reader.GetId(id);
    reader.GetData(arraySize);

    for (int i = 0; i < arraySize; i++)
    {
        UpdateNetworkObjectsMessage delta;
        delta.useTimeStamp = ID_TIMESTAMP;
        delta.timeStamp = reader.GetTime();
        reader.GetData(delta.lerp);
        reader.GetData(delta.netId);
        reader.GetData(delta.type);
        reader.GetData(delta.guid);
        reader.GetData(delta.x);
        reader.GetData(delta.y);
        reader.GetData(delta.velocityX);
        reader.GetData(delta.velocityY);
        reader.GetData(delta.rotation);
        reader.GetData(delta.angularVelocity);

        m_Game->UpdateNetworkObject(delta, (float)UPDATE_INTERVAL);
    }
}

void GameInterface::sendUpdateMessage()
{
    NetworkedGame::network_map::iterator it = m_Game->GetNetworkMapBegin();
    NetworkedGame::network_map::iterator itEnd = m_Game->GetNetworkMapEnd();
    int size = m_Game->GetNetworkMapSize();

    std::vector <std::pair<std::shared_ptr<NetworkedGameObject>, bool>> tempDataHolder;

    for (; it != itEnd; it++)
    {
        tempDataHolder.push_back(std::pair<std::shared_ptr<NetworkedGameObject>, bool>(it->second, false));
    }

    sendUpdateMessage(tempDataHolder);
}

void GameInterface::sendUpdateMessage(std::vector<std::pair<std::shared_ptr<NetworkedGameObject>, bool>> ids)
{
    NetworkMessageBuilder builder;
    builder.AddId((uchar)GameMessages::ID_UPDATE_NETWORK_OBJECTS_MESSAGE);

    builder.AddData(ids.size()); // Add array size so we know how much data to pull out

    for (unsigned int i = 0; i < ids.size(); i++)
    {
        sf::Vector2f curPos = ids[i].first->GetPosition();
        sf::Vector2f curVel = ids[i].first->GetVelocity();

        builder.AddData(ids[i].second);
        builder.AddData(ids[i].first->GetNetworkID());
        builder.AddData(ids[i].first->GetObjectType());
        builder.AddData(ids[i].first->GetOwner());
        builder.AddData(curPos.x);
        builder.AddData(curPos.y);
        builder.AddData(curVel.x);
        builder.AddData(curVel.y);
        builder.AddData(ids[i].first->GetBox2DRotationRads());
        builder.AddData(ids[i].first->GetAngularVelocity());
    }
    ids.clear();

    NetworkInterface::SendBroadcast(builder);
}

void GameInterface::sendNetworkedGameObjectID(network_id id, int type, RakNet::RakNetGUID guid, bool broadcast /* = "false/" */)
{
    NetworkMessageBuilder builder;
    builder.AddId((uchar)GameMessages::ID_SEND_NETWORK_OBJECT_ID_MESSAGE);
    builder.AddData(id);
    builder.AddData(type);

    NetworkInterface::SendMessage(builder, guid, broadcast);
}

void GameInterface::checkDeadReckoning()
{
    std::vector<std::pair<std::shared_ptr<NetworkedGameObject>, bool>> idsToUpdate = m_Game->CheckDeadReckoning((float)THRESHOLD);

    sendUpdateMessage(idsToUpdate);
}