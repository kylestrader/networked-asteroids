#pragma once

#include <memory>
#include <vector>
#include <list>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <RakNetTypes.h>

class SoundManager;
class NetworkedGame;
struct CreateNetworkObjectMessage;
struct DeleteNetworkObjectMessage;
struct PassNetworkObjectMessage;
class NetworkedGameObject;

namespace Events
{
    class Delegate;
    class Event;
}

struct InputState
{
    InputState()
    {
        W_KEY_PRESSED = false;
        A_KEY_PRESSED = false;
        S_KEY_PRESSED = false;
        D_KEY_PRESSED = false;
        ENTER_KEY_PRESSED = false;
        INSERT_KEY_PRESSED = false;
        LEFT_CLICK_PRESSED = false;
    }

    bool W_KEY_PRESSED;
    bool A_KEY_PRESSED;
    bool S_KEY_PRESSED;
    bool D_KEY_PRESSED;
    bool ENTER_KEY_PRESSED;
    bool INSERT_KEY_PRESSED;
    bool LEFT_CLICK_PRESSED;
};

class GameInterface
{
public:
    typedef RakNet::RakNetGUID guid;
    typedef unsigned int network_id;
    typedef unsigned char uchar;
    typedef unsigned int uint;

private:
    const double COUNT_DOWN_TIME; // In Seconds
    double UPDATE_INTERVAL; // In Seconds
    double THRESHOLD; // In Pixels

    bool m_IsServer;
    bool m_ExitGame;
    sf::Sprite m_EngineLogo;
    int m_FPS;
    std::string m_Title;
    std::string m_GameName;

    sf::Font m_DefaultFont;

    bool m_CanSendNetworkedMessages;

    double m_StartCountdown; // For showing the engine logo
    double m_NextUpdateCounter;

    bool m_ShowEngineLogo;
    bool m_IsEngineLogoFading;

    double m_PreviousDeltaT;
    Events::Delegate* m_NetworkEventDelegate;

protected:
    std::shared_ptr<NetworkedGame> m_Game;
    std::shared_ptr<sf::RenderWindow> m_Window;
    std::unique_ptr<SoundManager> m_SoundManager;

    float m_PlayerSpeed;

    InputState m_InputState;

private:
    void updateTitle();
    void updateNetworkedObjects(uchar* msg, uint size);
    void loadEngineAssets(std::string assetDir = "content/", bool secondAttempt = false);

    void checkDeadReckoning();

protected:
    virtual void initializeTextures(); // Setup the texture loader here
    virtual void createGame() = 0; // Instantiate m_Game here
    virtual void initializeGame(); // Initialize all of your game related stuff here
    virtual void loadAssets(std::string assetDir = "content/", bool secondAttempt = false) = 0;

    virtual void disposeGame(); // Dispose of your game resources with the exception of m_Game

    virtual void checkMousePress(sf::Mouse::Button button, double deltaT);
    virtual void checkMouseRelease(sf::Mouse::Button button, double deltaT);
    virtual void checkKeyRelease(sf::Keyboard::Key key, double deltaT);
    virtual void checkKeyPress(sf::Keyboard::Key key, double deltaT);
    virtual void checkMousePos(double deltaT);

    // This handles Update, Create and Delete NetworkedGameObject messages
    virtual void onNetworkEvent(Events::Event* e);

    virtual void update(double deltaT); // Update game logic that isn't in m_Game
    virtual void preDraw(); // Happens before m_Game draws
    virtual void postDraw(); // Happens after m_Game draws

    double GetPreviousDeltaT() const;
    const sf::Font& GetDefaultFont() const;
    
    template<class T>
    std::shared_ptr<T> GetGame() const
    {
        return std::dynamic_pointer_cast<T>(m_Game);
    }

    void sendUpdateMessage();
    void sendUpdateMessage(std::vector<std::pair<std::shared_ptr<NetworkedGameObject>, bool>> ids);
    void sendCreateMessage(network_id id, int type, guid owner, float x, float y, float velX, float velY, float rot, float angVel);
    void sendDeleteMessage(network_id id);
    void sendMultipleDeleteMessage(const std::vector<network_id>& ids);
    void sendNetworkedGameObjectID(network_id id, int type, RakNet::RakNetGUID guid, bool broadcast = false);
    void getCreateMessage(uchar* msg, uint size, CreateNetworkObjectMessage& obj);
    void getDeleteMessage(uchar* msg, uint size, DeleteNetworkObjectMessage& obj);
    void getMultipleDeleteMessage(uchar* msg, uint size, DeleteNetworkObjectMessage& obj, std::vector<network_id>& ids);
    void getNetworkedGameObjectID(uchar* msg, uint size, PassNetworkObjectMessage& obj);
    void setNetGameObjectVelocityViaInput(const sf::Vector2i& dir, network_id id, float go_speed);

public:
    GameInterface();
    ~GameInterface();

    inline bool ShouldExitGame() { return m_ExitGame; }

    void SetFPS(double fps);
    void SetGameName(std::string name);
    void SetThreshold(double pixels);
    void SetUpdateInterval(double value);

    bool IsServer() const;

    void Initialize(bool isServer);
    void UpdateGame(double deltaT); // deltaT is in seconds
    void OpenWindow();
    void CloseWindow();
    void RestartGame();
};