#include "GameObject.h"
#include "galaxyengine/Utils.h"

GameObject::GameObject()
{
    m_FlagKill = false;
}

GameObject::~GameObject()
{
	m_Sprite.reset();

    if (m_Body)
        m_Body->GetWorld()->DestroyBody(m_Body);
}

void GameObject::Draw(std::shared_ptr<sf::RenderWindow> window)
{
	window->draw(*m_Sprite);
}

void GameObject::Update(double deltaT)
{
	b2Vec2 pos = m_Body->GetPosition();
	sf::Vector2f texturePos;
	sf::IntRect textRect = m_Sprite->getTextureRect();
    sf::Vector2f origin = m_Sprite->getOrigin();

    texturePos.x = Utils::b2_MetersToPixels(pos.x) + origin.x - textRect.width / 2.0f;
    texturePos.y = Utils::b2_MetersToPixels(pos.y) + origin.y - textRect.height / 2.0f;

	m_Sprite->setPosition(texturePos);
    m_Sprite->setRotation(Utils::toDegreesf(m_Body->GetAngle()));
}

void GameObject::UpdateBox2DBody()
{
	sf::Vector2f pos = GetCenterPos();
    sf::Vector2f origin = m_Sprite->getOrigin();
	m_Body->SetTransform(b2Vec2(Utils::b2_PixelsToMeters(pos.x - origin.x), Utils::b2_PixelsToMeters(pos.y - origin.y)), m_Body->GetAngle());
}

void GameObject::Initialize(b2World* world, const B2D_BodyMetaData& body, const GO_MetaDeta& object)
{
    if (body.m_ShapeType == (int)B2ShapeType::BOX)
        m_B2ShapeType = B2ShapeType::BOX;
    else if (body.m_ShapeType == (int)B2ShapeType::CIRCLE)
        m_B2ShapeType = B2ShapeType::CIRCLE;

    initializeSfmlSprite(object);
    initializeBox2D(world, body, object.m_CustomDimensions, object.m_Dimensions);
}

void GameObject::initializeBox2D(b2World* world, const B2D_BodyMetaData& body, bool customDimensions, const sf::Vector2i& dimensions /* = sf::Vector2i(0, 0) */)
{
    sf::Vector2f pos = GetCenterPos();
    sf::Vector2f origin = m_Sprite->getOrigin();

	b2BodyDef bodyDef;
	bodyDef.type = body.m_BodyType; // We want kinematic so the ball doesn't affect the paddle but the paddle affects the ball
	bodyDef.position.Set(Utils::b2_PixelsToMeters(pos.x - origin.x), Utils::b2_PixelsToMeters(pos.y - origin.y));
    bodyDef.bullet = body.m_IsBullet;

	m_Body = world->CreateBody(&bodyDef);
	m_Sprite->setRotation(m_Body->GetAngle());

    b2FixtureDef fixtureDef;
    sf::IntRect textRect = m_Sprite->getTextureRect();

    fixtureDef.density = body.m_Density;
    fixtureDef.friction = body.m_Friction;
    fixtureDef.restitution = body.m_Restitution;
    fixtureDef.filter = body.m_Filter;
    fixtureDef.isSensor = body.m_IsSensor;

    if (body.m_ShapeType == (int)B2ShapeType::BOX)
    {
        b2PolygonShape dynamicBox;

        if (customDimensions)
            dynamicBox.SetAsBox(Utils::b2_PixelsToMeters(dimensions.x / 2.f), Utils::b2_PixelsToMeters(dimensions.y / 2.f));
        else
            dynamicBox.SetAsBox(Utils::b2_PixelsToMeters(textRect.width / 2.f), Utils::b2_PixelsToMeters(textRect.height / 2.f));

        fixtureDef.shape = &dynamicBox;
        m_Body->CreateFixture(&fixtureDef);
    }
    else if (body.m_ShapeType == (int)B2ShapeType::CIRCLE)
    {
        b2CircleShape dynamicBox;
        
        if (customDimensions)
            dynamicBox.m_radius = Utils::b2_PixelsToMeters(dimensions.x / 2.f);
        else
            dynamicBox.m_radius = Utils::b2_PixelsToMeters(textRect.width / 2.f);
        
        fixtureDef.shape = &dynamicBox;
        m_Body->CreateFixture(&fixtureDef);
    }	
}

void GameObject::initializeSfmlSprite(const GO_MetaDeta& object)
{
    m_Sprite.reset();
    m_Sprite = std::make_unique<sf::Sprite>();
    m_Sprite->setTexture(*object.m_Texture, !object.m_CustomDimensions);
    sf::IntRect rect = sf::IntRect((int)object.m_Position.x, (int)object.m_Position.y, object.m_Dimensions.x, object.m_Dimensions.y);

    if (object.m_CustomDimensions)
        m_Sprite->setTextureRect(rect);

    m_Sprite->setPosition(object.m_Position);

    if (object.m_RelativeOrigin)
    {
        sf::Vector2f origin;

        if (object.m_CustomDimensions)
        {
            origin.x = rect.left + (object.m_Origin.x * rect.width);
            origin.y = rect.top + (object.m_Origin.y * rect.height);
        }
        else
        {
            rect = m_Sprite->getTextureRect();
            origin.x = rect.left + (object.m_Origin.x * rect.width);
            origin.y = rect.top + (object.m_Origin.y * rect.height);
        }

        m_Sprite->setOrigin(origin);
    }
    else
        m_Sprite->setOrigin(object.m_Origin);
}

void GameObject::SetTexture(const sf::Texture& tex, bool resetTexRect)
{
	m_Sprite->setTexture(tex, resetTexRect);
}

void GameObject::SetTextureRect(sf::IntRect rect)
{
	m_Sprite->setTextureRect(rect);
}

void GameObject::Move(const sf::Vector2f& offset)
{
	m_Body->SetLinearVelocity(b2Vec2(offset.x, offset.y));
}

void GameObject::Move(float offsetX, float offsetY)
{
	m_Body->SetLinearVelocity(b2Vec2(offsetX, offsetY));
}

void GameObject::SetPosition(const sf::Vector2f& pos)
{
	m_Sprite->setPosition(pos);
    UpdateBox2DBody();
}

void GameObject::SetPosition(float x, float y)
{
	m_Sprite->setPosition(x, y);
    UpdateBox2DBody();
}

void GameObject::SetVelocity(const sf::Vector2f& vel)
{
    m_Body->SetLinearVelocity(b2Vec2(vel.x, vel.y));
}

void GameObject::SetVelocity(float velX, float velY)
{
    m_Body->SetLinearVelocity(b2Vec2(velX, velY));
}

void GameObject::SetAngularVelocity(float vel)
{
    m_Body->SetAngularVelocity(vel);
}

void GameObject::SetRotationDegrees(float rot)
{
    m_Sprite->setRotation(rot);
    UpdateBox2DBody();
}

void GameObject::SetRotationRads(float rads)
{
    m_Sprite->setRotation(Utils::toDegreesf(rads));
    UpdateBox2DBody();
}

void GameObject::SetBox2DRotationDegrees(float rot)
{
    m_Body->SetTransform(m_Body->GetPosition(), Utils::toRadiansf(rot));
}

void GameObject::SetBox2DRotationRads(float rads)
{
    m_Body->SetTransform(m_Body->GetPosition(), rads);
}

float GameObject::GetRotationRads() const
{
	return Utils::toRadiansf(m_Sprite->getRotation());
}

float GameObject::GetRotationDegrees() const
{
	return m_Sprite->getRotation();
}

float GameObject::GetBox2DRotationDegrees() const
{
    return Utils::toDegreesf(m_Body->GetAngle());
}

float GameObject::GetBox2DRotationRads() const
{
    return m_Body->GetAngle();
}

const sf::Vector2f& GameObject::GetPosition() const
{
	return m_Sprite->getPosition();
}

sf::Vector2f GameObject::GetCenterPos() const
{
	sf::Vector2f pos = GetPosition();
	sf::IntRect textRect = m_Sprite->getTextureRect();

	return sf::Vector2f(pos.x + textRect.width / 2.f, pos.y + textRect.height / 2.f);
}

sf::Vector2f GameObject::GetVelocity() const
{
    b2Vec2 vel = m_Body->GetLinearVelocity();
    
    return sf::Vector2f(vel.x, vel.y);
}

float GameObject::GetAngularVelocity() const
{
    float32 vel = m_Body->GetAngularVelocity();
    return vel;
}

sf::Vector2f GameObject::GetOrigin() const
{
    return m_Sprite->getOrigin();
}

bool GameObject::GetFlagKill() const
{
    return m_FlagKill;
}

void GameObject::Kill()
{
    m_FlagKill = true;
}

sf::IntRect GameObject::GetTextureRect() const
{
    return m_Sprite->getTextureRect();
}