#pragma once

#include <memory>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics.hpp>

#include "Box2D/Box2D.h"

#include "galaxyengine/IObject.h"

enum GO_Types
{
	PADDLE = 0,
	BALL,
};

enum B2ShapeType
{
    BOX = 0,
    CIRCLE
};

struct B2D_BodyMetaData
{
public:
    typedef int b2ShapeType; // Use B2ShapeType enum

    B2D_BodyMetaData(b2BodyType bodyType = b2_dynamicBody, b2ShapeType shapeType = 0, float density = 1.0f, float friction = 0.3f, float restitution = 0.0f, b2Filter filter = b2Filter(), bool isSensor = false, bool isBullet = false)
	{
		m_BodyType = bodyType;
        m_ShapeType = shapeType;
		m_Density = density;
		m_Friction = friction;
        m_Restitution = restitution;
        m_Filter = filter;
        m_IsSensor = isSensor;
        m_IsBullet = isBullet;
	}

    b2Filter m_Filter;
	b2BodyType m_BodyType;
    b2ShapeType m_ShapeType;
	float m_Density;
	float m_Friction;
    float m_Restitution;
    bool m_IsSensor;
    bool m_IsBullet;
};

struct GO_MetaDeta
{
public:

    GO_MetaDeta(sf::Texture& text, const sf::Vector2f& position, const sf::Vector2f& origin = sf::Vector2f(0.f, 0.f), bool relativeOrigin = false, bool customDimensions = false, const sf::Vector2i& dimensions = sf::Vector2i(0, 0))
    {
        m_Texture = &text;
        m_Position = position;
        m_Origin = origin;
        m_RelativeOrigin = relativeOrigin;
        m_CustomDimensions = customDimensions;
        m_Dimensions = dimensions;
    }

    sf::Texture* m_Texture;
    sf::Vector2f m_Position;
    sf::Vector2f m_Origin;
    bool m_RelativeOrigin;
    bool m_CustomDimensions;
    sf::Vector2i m_Dimensions;
};

class GameObject: IObject
{
public:
	GameObject();
	~GameObject();

	//Util
	inline virtual void Dispose()=0;
    virtual void Initialize(b2World* world, const B2D_BodyMetaData& body, const GO_MetaDeta& object);
	virtual void Draw(std::shared_ptr<sf::RenderWindow> window);
	virtual void Update(double deltaT);
	virtual void Move(const sf::Vector2f& offset);
	virtual void Move(float offsetX, float offsetY);

	//Getters
	const sf::Vector2f& GetPosition() const;
	float GetRotationDegrees() const;
	float GetRotationRads() const;
    float GetBox2DRotationDegrees() const;
    float GetBox2DRotationRads() const;
	sf::Vector2f GetCenterPos() const;
    sf::Vector2f GetVelocity() const; // In meters per second
    float GetAngularVelocity() const; // In meters per second
    bool GetFlagKill() const;
    sf::IntRect GetTextureRect() const;
    sf::Vector2f GetOrigin() const;

    void Kill();

	//Setters

    // DEPRECATED Use SetBox2DRotationDegrees
	virtual void SetRotationDegrees(float rot);

    // DEPRECATED Use SetBox2DRotationRads
	virtual void SetRotationRads(float rads);

    virtual void SetBox2DRotationDegrees(float rot);
    virtual void SetBox2DRotationRads(float rads);
	virtual void SetPosition(const sf::Vector2f& pos);
	virtual void SetPosition(float x, float y);
	virtual void SetTexture(const sf::Texture& tex, bool resetTexRect);
	virtual void SetTextureRect(sf::IntRect rect);
    virtual void SetVelocity(const sf::Vector2f& vel); // In meters per second
    virtual void SetVelocity(float velX, float velY); // In meters per second
    virtual void SetAngularVelocity(float vel);

    bool operator==(b2Body* body)
    {
        if (body == m_Body)
            return true;
        else
            return false;
    }

protected:
	b2Body* m_Body;
	std::unique_ptr<sf::Sprite> m_Sprite;
    B2ShapeType m_B2ShapeType;
    bool m_FlagKill;

	virtual void UpdateBox2DBody();

private:
    void initializeSfmlSprite(const GO_MetaDeta& object);
    void initializeBox2D(b2World* world, const B2D_BodyMetaData& body, bool customDimensions, const sf::Vector2i& dimensions = sf::Vector2i(0, 0));
};
