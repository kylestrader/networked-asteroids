#pragma once

#include <memory>
#include <SFML/Graphics.hpp>

class IGame
{
public:
    virtual void Initialize(std::shared_ptr<sf::RenderWindow> window) = 0;
    virtual void Update(double deltaT) = 0;
    virtual void Draw() = 0;
    virtual void Reset() = 0;
};