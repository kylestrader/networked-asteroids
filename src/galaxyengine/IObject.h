#pragma once

// This interface is meant to be inherited by classes that want to have a dispose function.
// In most cases, it is good to inherit from IObject, but not necessarily in all cases.

class IObject
{
public:
	virtual void Dispose() = 0;
};