#include "NetworkEvent.h"
#include <RakNetTypes.h>

using namespace Events;

NetworkEvent::NetworkEvent(uchar msgId, uchar* msg, uint size, guid* aGuid)
    :Event(Events::NETWORK_EVENT)
{
    m_MsgId = msgId;
    m_Msg = msg;
    m_Size = size;
    m_GUID = aGuid;
}

NetworkEvent::~NetworkEvent()
{
    
}

NetworkEvent::uchar NetworkEvent::GetMsgId() const
{
    return m_MsgId;
}

NetworkEvent::uchar* NetworkEvent::GetMsg() const
{
    return m_Msg;
}

NetworkEvent::uint NetworkEvent::GetSize() const
{
    return m_Size;
}

const NetworkEvent::guid NetworkEvent::GetGUID() const
{
    return *m_GUID;
}

void NetworkEvent::Dispose()
{
    if (m_Msg != nullptr)
    {
        delete m_Msg;
        m_Msg = nullptr;
    }

    if (m_GUID != nullptr)
    {
        delete m_GUID;
        m_GUID = nullptr;
    }
}