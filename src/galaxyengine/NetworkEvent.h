#pragma once

#include "Event.h"

namespace RakNet
{
    struct RakNetGUID;
}

namespace Events
{

class NetworkEvent : public Event
{
public:
    typedef unsigned char uchar;
    typedef unsigned int uint;
    typedef RakNet::RakNetGUID guid;

private:
    uchar m_MsgId;
    uchar* m_Msg;
    uint m_Size;
    guid* m_GUID;

public:
    NetworkEvent(uchar msgId, uchar* msg, uint size, guid* aGuid);
    ~NetworkEvent();

    uchar GetMsgId() const;
    uchar* GetMsg() const;
    uint GetSize() const;
    const guid GetGUID() const;

    virtual void Dispose();
};

}