#include "NetworkInterface.h"
#include "NetworkEvent.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include "NetworkMessageHelper.h"

#include "EventDispatcher.h"
#include <RakPeerInterface.h>
#include <BitStream.h>
#include <RakNetTypes.h>
#include <GetTime.h>

#undef SendMessage

Events::EventDispatcher* g_NetworkEventDispatcher = nullptr;
RakNet::RakPeerInterface* NetworkInterface::m_Peer = nullptr;
RakNet::Packet* NetworkInterface::m_Packet = nullptr;
bool NetworkInterface::m_IsServer = false;

void NetworkInterface::Initialize()
{
    if (g_NetworkEventDispatcher == nullptr)
        g_NetworkEventDispatcher = new Events::EventDispatcher();
}

void NetworkInterface::ConnectAsServer(int serverPort /* = SERVER_PORT */)
{
    if (serverPort == 0)
        serverPort = SERVER_PORT;

    m_Peer = RakNet::RakPeerInterface::GetInstance();
    RakNet::SocketDescriptor sd(serverPort, 0);
    m_Peer->Startup(MAX_CLIENTS, &sd, 1);
    m_IsServer = true;

    printf("Starting the server...\n");
    m_Peer->SetMaximumIncomingConnections(MAX_CLIENTS);
    m_Peer->SetTimeoutTime(TIME_OUT_TIME, RakNet::UNASSIGNED_SYSTEM_ADDRESS);

    printf("\nMy IP addresses:\n");
    for (unsigned int i = 0; i < m_Peer->GetNumberOfAddresses(); i++)
    {
        RakNet::SystemAddress sa = m_Peer->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
        printf("%i. %s (LAN=%i)\n", i + 1, sa.ToString(false), sa.IsLANAddress());
    }
}

void NetworkInterface::ConnectAsClient(char* serverAddress, int size, int serverPort /* = SERVER_PORT */, int clientPort /* = CLIENT_PORT */)
{
    if (clientPort == 0)
        clientPort = CLIENT_PORT;

    m_Peer = RakNet::RakPeerInterface::GetInstance();
    RakNet::SocketDescriptor sd(clientPort, 0);
    m_Peer->Startup(1, &sd, 1);
    m_IsServer = false;

    if (serverPort == 0)
        serverPort = SERVER_PORT;

    if (serverAddress[0] == 0 && size > 10)
    {
        strcpy_s(serverAddress, 10, "127.0.0.1");
    }

    printf("Starting the client.\n");
    m_Peer->Connect(serverAddress, serverPort, 0, 0);
}

void NetworkInterface::CloseConnection()
{
    m_Peer->Shutdown(300, (uchar)'\000', HIGH_PRIORITY);
}

void NetworkInterface::DestroyPeerInstance()
{
    g_NetworkEventDispatcher->Dispose();
    delete g_NetworkEventDispatcher;
    g_NetworkEventDispatcher = nullptr;

    RakNet::RakPeerInterface::DestroyInstance(m_Peer);
}

NetworkInterface::uchar NetworkInterface::getPacketID(uchar* msg)
{
    if (msg[0] == ID_TIMESTAMP)
        return msg[sizeof(uchar) + sizeof(timestamp)];
    else
        return msg[0];
}

void NetworkInterface::UpdatePacketCheck()
{
    for (m_Packet = m_Peer->Receive(); m_Packet; m_Peer->DeallocatePacket(m_Packet), m_Packet = m_Peer->Receive())
    {
        uchar id = getPacketID(m_Packet->data);
        switch (id)
        {   
            case ID_CONNECTION_LOST:
                printf("Another client has lost connection.");
                dispatchNetworkMessage((uchar)GameMessages::ID_PLAYER_DISCONNECTED_MESSAGE, m_Packet->guid);
                break;
            case ID_REMOTE_DISCONNECTION_NOTIFICATION:
                printf("Another client has disconnected\n");
                dispatchNetworkMessage((uchar)GameMessages::ID_PLAYER_DISCONNECTED_MESSAGE, m_Packet->guid);
                break;

            case ID_REMOTE_CONNECTION_LOST:
                printf("Another client has lost the connection\n");
                dispatchNetworkMessage((uchar)GameMessages::ID_PLAYER_DISCONNECTED_MESSAGE, m_Packet->guid);
                break;

            case ID_REMOTE_NEW_INCOMING_CONNECTION:
                printf("Another client has connected\n");
                dispatchNetworkMessage((uchar)GameMessages::ID_PLAYER_CONNECTED_MESSAGE, m_Packet->guid);
                break;

            case ID_CONNECTION_REQUEST_ACCEPTED:
                printf("We have joined the server.\n");
                dispatchNetworkMessage((uchar)GameMessages::ID_JOINED_SERVER_MESSAGE, m_Packet->guid);
                break;

            case ID_NEW_INCOMING_CONNECTION:
                printf("A person has joined the server.\n");
                dispatchNetworkMessage((uchar)GameMessages::ID_PLAYER_CONNECTED_MESSAGE, m_Packet->guid);
                break;

            case ID_NO_FREE_INCOMING_CONNECTIONS:
                printf("The server is full.\n");
                break;

            case ID_DISCONNECTION_NOTIFICATION:
                if (m_IsServer)
                {
                    printf("A client has disconnected.\n");
                    dispatchNetworkMessage((uchar)GameMessages::ID_PLAYER_DISCONNECTED_MESSAGE, m_Packet->guid);
                }
                else
                {
                    printf("We have been disconnected.\n");
                }
                break;

            default:
                dispatchNetworkMessage(id, m_Packet->guid, m_Packet->data, m_Packet->length);
                break;
        }
    }
}

void NetworkInterface::SendMessageId(msg_id msgId, guid aGuid, bool isBroadcast /* = false */)
{
    RakNet::BitStream bsOut;
    bsOut.Write(msgId);

    m_Peer->Send(&bsOut, PacketPriority::HIGH_PRIORITY, PacketReliability::RELIABLE_ORDERED, 0, aGuid, isBroadcast);
}

void NetworkInterface::SendMessage(const char* msg, int size, guid aGuid, bool isBroadcast /* = false */)
{
    m_Peer->Send(msg, size, PacketPriority::HIGH_PRIORITY, PacketReliability::RELIABLE_ORDERED, 0, aGuid, isBroadcast);
}

void NetworkInterface::SendMessage(const NetworkMessageBuilder& msg, guid aGuid, bool isBroadcast /* = false */)
{
    m_Peer->Send(msg.GetBitStream(), PacketPriority::HIGH_PRIORITY, PacketReliability::RELIABLE_ORDERED, 0, aGuid, isBroadcast);
}

//void NetworkInterface::SendMessageId(msg_id msgId, bool isBroadcast /* = false */)
//{
//    if (isBroadcast)
//        SendMessageId(msgId, m_Peer->GetMyGUID(), true);
//    else
//        SendMessageId(msgId, m_Peer->GetGUIDFromIndex(0), false);
//}
//
//void NetworkInterface::SendMessage(const char* msg, int size, bool isBroadcast /* = false */)
//{
//    if (isBroadcast)
//        SendMessage(msg, size, m_Peer->GetMyGUID(), true);
//    else
//        SendMessage(msg, size, m_Peer->GetGUIDFromIndex(0), false);
//}

void NetworkInterface::SendBroadcastId(msg_id msgId)
{
    RakNet::BitStream bsOut;
    bsOut.Write(msgId);
    m_Peer->Send(&bsOut, PacketPriority::HIGH_PRIORITY, PacketReliability::UNRELIABLE_SEQUENCED, 0, m_Peer->GetMyGUID(), true);
}

void NetworkInterface::SendBroadcast(const char* msg, int size)
{
    m_Peer->Send(msg, size, PacketPriority::HIGH_PRIORITY, PacketReliability::UNRELIABLE_SEQUENCED, 0, m_Peer->GetMyGUID(), true);
}

void NetworkInterface::SendBroadcast(const NetworkMessageBuilder& msg)
{
    m_Peer->Send(msg.GetBitStream(), PacketPriority::HIGH_PRIORITY, PacketReliability::UNRELIABLE_SEQUENCED, 0, m_Peer->GetMyGUID(), true);
}

bool NetworkInterface::IsServer()
{
    return m_IsServer;
}

void NetworkInterface::dispatchNetworkMessage(uchar msgId, guid aGuid, uchar* msg /* = nullptr */, uint size /* = -1 */)
{
    uchar* newMsg = nullptr;
    guid* guidCopy = nullptr;

    // We make a copy to avoid any problems later. the event will automatically delete the message when it is destroyed
    if (msg != nullptr && size != 0)
    {
        newMsg = new uchar[size];
        memcpy(newMsg, msg, size);
    }

    guidCopy = new guid(aGuid); // We need to make a copy for the network event to store and keep

    Events::NetworkEvent* ev = new Events::NetworkEvent(msgId, newMsg, size, guidCopy);
    g_NetworkEventDispatcher->DispatchEvent(ev);
}

NetworkInterface::guid NetworkInterface::GetSessionGuid()
{ 
	return m_Peer->GetMyGUID(); 
}

NetworkInterface::timestamp NetworkInterface::GetTime()
{
    return RakNet::GetTime();
}