#pragma once

#include <RakNetTime.h>
#include <MessageIdentifiers.h>
#include <RakNetTypes.h>

#define MAX_CLIENTS 2
#define SERVER_PORT 60000
#define CLIENT_PORT 60001
#define INVALID_IP_ADDRESS -1
#define TIME_OUT_TIME 3000

#undef SendMessage

// Supplies just the very base game related messages
enum GameMessages
{
    ID_PLAYER_CONNECTED_MESSAGE = ID_USER_PACKET_ENUM + 1,
    ID_PLAYER_DISCONNECTED_MESSAGE,
    ID_PLAYER_QUIT_MESSAGE,
    ID_UPDATE_NETWORK_OBJECTS_MESSAGE,
    ID_CREATE_NETWORK_OBJECT_MESSAGE,
    ID_DELETE_NETWORK_OBJECT_MESSAGE,
    ID_DELETE_MULTIPLE_NETWORK_OBJECTS_MESSAGE,
    ID_SEND_NETWORK_OBJECT_ID_MESSAGE,
    ID_JOINED_SERVER_MESSAGE,
    NUM_MESSAGES
};

// We just use this struct to store the information from the message after the fact
// since we send it through the bitstream
struct UpdateNetworkObjectsMessage
{
    unsigned char useTimeStamp;
    RakNet::Time timeStamp;
    bool lerp;
    int netId;
    int type;
    RakNet::RakNetGUID guid;
    float x;
    float y;
    float velocityX;
    float velocityY;
    float rotation;
    float angularVelocity;
};

struct CreateNetworkObjectMessage
{
    unsigned char useTimeStamp;
    RakNet::Time timeStamp;
    unsigned char MsgId;
    int netId;
    int type;
    RakNet::RakNetGUID guid;
    float x;
    float y;
    float velocityX;
    float velocityY;
    float rotation;
    float angularVelocity;
};

struct DeleteNetworkObjectMessage
{
    unsigned char useTimeStamp;
    RakNet::Time timeStamp;
    unsigned char MsgId;
    int netId;
};

struct PassNetworkObjectMessage
{
    unsigned char MsgId;
    int netId;
    int type;
};

namespace RakNet
{
    class RakPeerInterface;
    struct Packet;
    struct RakNetGUID;
}

namespace Events
{
    class EventDispatcher;
}

class NetworkMessageBuilder;

class NetworkInterface
{
public:
	typedef RakNet::RakNetGUID guid;
	typedef unsigned char uchar;
	typedef unsigned int uint;
    typedef unsigned long ulong;
	typedef uchar msg_id;
    typedef RakNet::Time timestamp;

private:
	static RakNet::RakPeerInterface* m_Peer;
	static RakNet::Packet* m_Packet;
	static bool m_IsServer;

public:
    static void Initialize(); // This initializes the global network event dispatcher for you, make sure to call it
    static bool IsServer();
    static void ConnectAsServer(int serverPort = SERVER_PORT);
    static void ConnectAsClient(char* serverAddress, int size, int serverPort = SERVER_PORT, int clientPort = CLIENT_PORT);
    static void DestroyPeerInstance();
    static void UpdatePacketCheck();
    static void CloseConnection();
    static void SendMessageId(msg_id msgId, guid aGuid, bool isBroadcast = false);
    static void SendMessage(const char* msg, int size, guid aGuid, bool isBroadcast = false);
    static void SendMessage(const NetworkMessageBuilder& msg, guid aGuid, bool isBroadcast = false);
    static void SendBroadcastId(msg_id msgId);
    static void SendBroadcast(const char* msg, int size);
    static void SendBroadcast(const NetworkMessageBuilder& msg);
    static timestamp GetTime();

    template <typename T>
    static void SendMessage(const T& msg, guid aGuid, bool isBroadcast = false)
    {
        SendMessage((const char*)&msg, sizeof(T), aGuid, isBroadcast);
    }

    template <typename T>
    static void SendBroadcast(const T& msg)
    {
        SendBroadcast((const char*)&msg, sizeof(T));
    }

	//Getters
	static guid GetSessionGuid();

private:
    static void dispatchNetworkMessage(uchar msgId, guid aGuid, uchar* msg = nullptr, uint size = -1);
    static uchar getPacketID(uchar* msg);
};

extern Events::EventDispatcher* g_NetworkEventDispatcher; // All network interface events are sent through the global network event dispatcher