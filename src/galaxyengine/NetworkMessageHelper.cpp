#include "NetworkMessageHelper.h"
#include <MessageIdentifiers.h>
#include "NetworkInterface.h"

NetworkMessageBuilder::NetworkMessageBuilder()
{
    m_BitStream.Write((uchar)ID_TIMESTAMP);
    m_BitStream.Write(NetworkInterface::GetTime());
}

NetworkMessageBuilder::~NetworkMessageBuilder()
{

}

const RakNet::BitStream* NetworkMessageBuilder::GetBitStream() const
{
    return &m_BitStream;
}

void NetworkMessageBuilder::AddId(msg_id msgId)
{
    m_BitStream.Write(msgId);
}

NetworkMessageReader::NetworkMessageReader(uchar* msg, uint size)
    : m_BitStream(msg, size, true)
{
    uchar whatever;
    m_BitStream.Read(whatever);
    m_BitStream.Read(m_TimeStamp);
}

NetworkMessageReader::~NetworkMessageReader()
{

}

NetworkMessageReader::uchar* NetworkMessageReader::GetRawData() const
{
    return m_BitStream.GetData();
}

bool NetworkMessageReader::GetId(msg_id& out)
{
    return m_BitStream.Read(out);
}

NetworkMessageReader::timestamp NetworkMessageReader::GetTime() const
{
    return m_TimeStamp;
}