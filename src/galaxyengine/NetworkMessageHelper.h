#pragma once

#include <BitStream.h>
#include <RakNetTime.h>

class NetworkMessageBuilder
{
public:
    typedef unsigned int    uint;
    typedef unsigned char   uchar;
    typedef uchar           msg_id;
    typedef RakNet::Time    timestamp;

public:
    NetworkMessageBuilder();
    ~NetworkMessageBuilder();

    const RakNet::BitStream* GetBitStream() const;

    void AddId(msg_id msgId);

    template <typename T>
    void AddData(const T& data)
    {
        m_BitStream.Write(data);
    }

private:
    RakNet::BitStream m_BitStream;
};

class NetworkMessageReader
{
public:
    typedef unsigned int    uint;
    typedef unsigned char   uchar;
    typedef uchar           msg_id;
    typedef RakNet::Time    timestamp;

public:
    NetworkMessageReader(uchar* msg, uint size);
    ~NetworkMessageReader();

    uchar* GetRawData() const;

    bool GetId(msg_id& out);
    timestamp GetTime() const;

    template <typename T>
    bool GetData(T& out)
    {
        return m_BitStream.Read(out);
    }

private:
    RakNet::BitStream m_BitStream;
    timestamp m_TimeStamp;
};