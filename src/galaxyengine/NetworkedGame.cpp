#include "NetworkedGame.h"
#include "Box2D/Box2D.h"
#include "NetworkedGameObject.h"
#include "DebugDraw.h"
#include "asteroids/GameObjectFactory.h"
#include "Utils.h"

int NetworkedGame::m_NetworkID = 0;

NetworkedGame::NetworkedGame()
    :BOX2D_TIME_STEP(1.f / 60.f),
    BOX2D_VELOCITY_ITERATIONS(6),
    BOX2D_POSITION_ITERATIONS(2)
{
    m_DebugMode = false;
}

NetworkedGame::~NetworkedGame()
{
    for (network_map::iterator it = m_NetworkedObjects.begin(); it != m_NetworkedObjects.end(); it++)
    {
        it->second.reset();
        it->second = nullptr;
    }

    m_NetworkedObjects.clear();

    delete m_Box2DDebugDrawer;
    m_Box2DDebugDrawer = nullptr;

    // Clean up our world and due to Box2D implementation, it will clean up all of its parts for us :)
    delete m_World;
    m_World = nullptr;
}

void NetworkedGame::Initialize(std::shared_ptr<sf::RenderWindow> window)
{
    m_Window = window;

    // Create our box2d world
    createBox2DWorld();

    //GameObjectFactory::CreateNetworkedGameObjectByType((int)GameObjectTypes::PLAYER_1, 100000, m_World);
}

void NetworkedGame::Update(double deltaT)
{
    m_World->Step(BOX2D_TIME_STEP, BOX2D_VELOCITY_ITERATIONS, BOX2D_POSITION_ITERATIONS);

    for (network_map::iterator it = m_NetworkedObjects.begin(); it != m_NetworkedObjects.end(); it++)
    {
        if (it->second == nullptr)
            continue;

        it->second->Update(deltaT);
        if (it->second->GetFlagKill())
        {
            m_ObjectsToDelete.push_back(it->first);
        }
    }

    if (m_ObjectsToDelete.size() > 0)
    {
        for (unsigned int i = 0; i < m_ObjectsToDelete.size(); i++)
        {
            m_NetworkedObjects.erase(m_ObjectsToDelete[i]);
            m_ObjectsDeleted.push_back(m_ObjectsToDelete[i]);
        }

        m_ObjectsToDelete.clear();
    }
}

void NetworkedGame::Draw()
{
    for (network_map::iterator it = m_NetworkedObjects.begin(); it != m_NetworkedObjects.end(); it++)
    {
        it->second->Draw(m_Window);
    }

    if (m_DebugMode)
        m_World->DrawDebugData();
}

void NetworkedGame::Reset()
{

}

void NetworkedGame::UpdateNetworkObject(const UpdateNetworkObjectsMessage& delta, float updateInterval)
{
	updateNetworkObject(delta.netId, delta.type, delta.guid, delta.x, delta.y, delta.velocityX, delta.velocityY, delta.rotation, delta.angularVelocity, delta.timeStamp, updateInterval, delta.lerp);
}

void NetworkedGame::CreateNetworkObject(const CreateNetworkObjectMessage& obj, float updateInterval)
{
	updateNetworkObject(obj.netId, obj.type, obj.guid, obj.x, obj.y, obj.velocityX, obj.velocityY, obj.rotation, obj.angularVelocity, obj.timeStamp, updateInterval, false);
}

void NetworkedGame::DeleteNetworkObject(network_id id)
{
    m_ObjectsDeleted.push_back(id);
    m_NetworkedObjects.erase(id);
    onDeleteNetworkObject(id);
}

std::vector<std::pair<std::shared_ptr<NetworkedGameObject>, bool>> NetworkedGame::CheckDeadReckoning(float threshold)
{
    network_map::iterator it = m_NetworkedObjects.begin();
    guid us = NetworkInterface::GetSessionGuid(); // USA! USA! USA!
    std::vector<std::pair<std::shared_ptr<NetworkedGameObject>, bool>> idsToUpdate;

    for (; it != m_NetworkedObjects.end(); it++)
    {
        if (it->second->GetOwner() == us)
        {
            if (it->second->GetSpawnedFlag())
            {
                idsToUpdate.push_back(std::pair<std::shared_ptr<NetworkedGameObject>, bool>(it->second, false));
                it->second->SetSpawnedFlag(false);
                continue;
            }

            int result = checkDeadReckoning(it->second, threshold);

            if (result != NONE)
            {
                it->second->SaveState();
                idsToUpdate.push_back(std::pair<std::shared_ptr<NetworkedGameObject>, bool>(it->second, result == LERP ? true : false));
            }
        }
    }

    return idsToUpdate;
}

int NetworkedGame::checkDeadReckoning(std::shared_ptr<NetworkedGameObject> obj, const float THRESHOLD)
{
    /*
    Dead Reckoning : Latency Hiding for Networked Games:
    http://www.gamasutra.com/view/feature/131638/dead_reckoning_latency_hiding_for_.php
    */
    //1) Get estimated Dead Reckoning position.
    //2) If the DR position is far enough away from our actual position, update out network object's position.

    //1)
    NetworkInterface::timestamp currentTime = NetworkInterface::GetTime();
    NetworkInterface::timestamp drTime = currentTime - obj->GetPreviousState().m_TimeStamp;
    sf::Vector2f drVel = obj->GetPreviousState().m_Velocity;
    sf::Vector2f drPos = obj->GetPreviousState().m_Position;
    drPos += sf::Vector2f(Utils::b2_MetersToPixels(drVel.x * ((float)drTime / 1000.0f)), Utils::b2_MetersToPixels(drVel.y * ((float)drTime / 1000.0f)));

    //2)
    sf::Vector2f diff = drPos - obj->GetPosition();
    diff.x = std::abs(diff.x);
    diff.y = std::abs(diff.y);

    /*Create a temporary threshold for this GameObject. Because speeds vary so much between the
    --different networked objects it is impossible to create a threshold that works well in all
    --cases. To remedy this we create a personalized threshold for each GameObject on each update.*/

    //float GO_tickThreshold = THRESHOLD * sqrt(pow(drVel.x, 2) + pow(drVel.y, 2));
    float GO_tickThreshold = THRESHOLD;

    // Snap if we are way too far from the threshold
    if (diff.x > GO_tickThreshold * 2 || diff.y > GO_tickThreshold * 2)
        return SNAP;
    // Otherwise lerp to it
    else if (diff.x > GO_tickThreshold || diff.y > GO_tickThreshold)
        return LERP;
    return NONE;
}

void NetworkedGame::createBox2DWorld()
{
    b2Vec2 gravity(0.0f, 0.0f); // We don't want any gravity
    m_World = new b2World(gravity);

    uint32 flags = b2Draw::e_shapeBit;
    //flags += b2DebugDraw::e_jointBit;
    //flags += b2DebugDraw::e_aabbBit;
    //flags += b2DebugDraw::e_pairBit;
    //flags += b2DebugDraw::e_centerOfMassBit;

    m_Box2DDebugDrawer = new DebugDraw(m_Window, m_Font);
    m_Box2DDebugDrawer->SetFlags(flags);
    m_World->SetDebugDraw(m_Box2DDebugDrawer);
}

void NetworkedGame::onDeleteNetworkObject(network_id id)
{

}

bool NetworkedGame::IsNetIdNew(network_id n_id)
{
    network_map::iterator it = m_NetworkedObjects.find(n_id);
    if (it != m_NetworkedObjects.end())
        return false;
    else
        return true;
}

bool NetworkedGame::IsServer() const
{
    return NetworkInterface::IsServer();
}

bool NetworkedGame::IsDeletedObjectsEmpty() const
{
    return (m_ObjectsDeleted.size() == 0) ? true : false;
}

std::shared_ptr<NetworkedGameObject> NetworkedGame::GetNetworkObject(network_id id)
{
    network_map::iterator it = m_NetworkedObjects.find(id);
    if (it != m_NetworkedObjects.end())
        return m_NetworkedObjects[id];
    else
        return nullptr;
}

std::shared_ptr<NetworkedGameObject> NetworkedGame::GetNetworkObject(b2Body* body)
{
    network_map::iterator it = m_NetworkedObjects.begin();

    for (; it != m_NetworkedObjects.end(); it++)
    {
        if (*it->second == body)
        {
            return it->second;
        }
    }

    return nullptr;
}

void NetworkedGame::ClearNetworkID()
{
    m_NetworkID = 0;
}

void NetworkedGame::SetNetworkID(network_id value)
{
    m_NetworkID = value;
}

void NetworkedGame::ClearDeletedObjects()
{
    m_ObjectsDeleted.clear();
}

int NetworkedGame::GetNetworkMapSize() const
{
    return m_NetworkedObjects.size();
}

NetworkedGame::network_map::iterator NetworkedGame::GetNetworkMapBegin()
{
    return m_NetworkedObjects.begin();
}

NetworkedGame::network_map::iterator NetworkedGame::GetNetworkMapEnd()
{
    return m_NetworkedObjects.end();
}

bool NetworkedGame::NetworkMapContains(network_id id)
{
    network_map::iterator it = m_NetworkedObjects.find(id);
    if (it != m_NetworkedObjects.end())
        return true;
    else
        return false;
}

bool NetworkedGame::NetworkMapContains(b2Body* body)
{
    network_map::iterator it = m_NetworkedObjects.begin();

    for (; it != m_NetworkedObjects.end(); it++)
    {
        if (*it->second == body)
        {
            return true;
        }
    }

    return false;
}

int NetworkedGame::GetBox2DVelocityIterations() const
{
    return BOX2D_VELOCITY_ITERATIONS;
}

int NetworkedGame::GetBox2DPositionIterations() const
{
    return BOX2D_POSITION_ITERATIONS;
}

float NetworkedGame::GetBox2DTimeStep() const
{
    return BOX2D_TIME_STEP;
}

b2World* NetworkedGame::GetBox2DWorld() const
{
    return m_World;
}

bool NetworkedGame::GetDebugMode() const
{
    return m_DebugMode;
}

int NetworkedGame::GetNewID()
{
    return m_NetworkID++;
}

const sf::Font& NetworkedGame::GetFont() const
{
    return m_Font;
}

const std::vector<NetworkedGame::network_id>& NetworkedGame::GetDeletedObjects() const
{
    return m_ObjectsDeleted;
}

void NetworkedGame::SetBox2DVelocityIterations(int value)
{
    BOX2D_VELOCITY_ITERATIONS = value;
}

void NetworkedGame::SetBox2DPositionIterations(int value)
{
    BOX2D_POSITION_ITERATIONS = value;
}

void NetworkedGame::SetBox2DTimeStep(float value)
{
    BOX2D_TIME_STEP = value;
}

void NetworkedGame::SetDebugMode(bool value)
{
    m_DebugMode = value;
}

void NetworkedGame::SetFont(const sf::Font& font)
{
    m_Font = font;
}

void NetworkedGame::SetNetworkObject(network_id id, std::shared_ptr<NetworkedGameObject> object)
{
    m_NetworkedObjects[id] = object;
}