#pragma once

#include "IGame.h"
#include <map>
#include <vector>
#include <SFML/Graphics/Font.hpp>
#include "galaxyengine/NetworkInterface.h"
#include <RakNetTypes.h>

struct CreateNetworkObjectMessage;
struct UpdateNetworkObjectsMessage;

class b2World;
class b2Body;
class DebugDraw;
class NetworkedGameObject;

class NetworkedGame : public IGame
{
public:
    typedef unsigned int network_id;
    typedef std::map<network_id, std::shared_ptr<NetworkedGameObject>> network_map;
    typedef RakNet::RakNetGUID guid;

private:

    enum { SNAP = 0, LERP, NONE };

    static int m_NetworkID;
    float BOX2D_TIME_STEP;
    int BOX2D_VELOCITY_ITERATIONS;
    int BOX2D_POSITION_ITERATIONS;

    b2World* m_World; // The Box2D world
    DebugDraw* m_Box2DDebugDrawer;
    sf::Font m_Font;

    bool m_DebugMode;

    network_map m_NetworkedObjects;
    std::vector<network_id> m_ObjectsToDelete; // A list of objects which require deletion
    std::vector<network_id> m_ObjectsDeleted; // A list of objects which were recently deleted

protected:
    std::shared_ptr<sf::RenderWindow> m_Window; // Just a ref if needed

protected:
    int GetNewID();

    virtual void onDeleteNetworkObject(network_id id);
    virtual void updateNetworkObject(network_id netId, int type, guid owner, float x, float y, float velocityX, float velocityY, float rotation, float angularVelocity, NetworkInterface::timestamp srv_timestamp, float updateInterval, bool lerp) = 0;

private:
    void createBox2DWorld();
    int checkDeadReckoning(std::shared_ptr<NetworkedGameObject> obj, const float THRESHOLD);

public:
    NetworkedGame();
    ~NetworkedGame();

    // Getters
    int GetBox2DVelocityIterations() const;
    int GetBox2DPositionIterations() const;
    float GetBox2DTimeStep() const;
    b2World* GetBox2DWorld() const;
    bool GetDebugMode() const;
    std::shared_ptr<NetworkedGameObject> GetNetworkObject(network_id id);
    std::shared_ptr<NetworkedGameObject> GetNetworkObject(b2Body* body);
    int GetNetworkMapSize() const;
    bool NetworkMapContains(network_id id);
    bool NetworkMapContains(b2Body* body);
    network_map::iterator GetNetworkMapBegin();
    network_map::iterator GetNetworkMapEnd();
    const sf::Font& GetFont() const;
    const std::vector<network_id>& GetDeletedObjects() const;

    // Setters
    void SetBox2DVelocityIterations(int value);
    void SetBox2DPositionIterations(int value);
    void SetBox2DTimeStep(float value);
    void SetDebugMode(bool value);
    void SetFont(const sf::Font& font);
    void SetNetworkObject(network_id id, std::shared_ptr<NetworkedGameObject> object);

    void ClearNetworkID();
    void SetNetworkID(network_id value);
    void ClearDeletedObjects();

    bool IsNetIdNew(network_id n_id);
    bool IsServer() const;
    bool IsDeletedObjectsEmpty() const;

    std::vector<std::pair<std::shared_ptr<NetworkedGameObject>, bool>> CheckDeadReckoning(float threshold);

    void CreateNetworkObject(const CreateNetworkObjectMessage& obj, float updateInterval);
    void UpdateNetworkObject(const UpdateNetworkObjectsMessage& delta, float updateInterval);
    void DeleteNetworkObject(network_id id);

    virtual void Initialize(std::shared_ptr<sf::RenderWindow> window) override;
    virtual void Update(double deltaT) override;
    virtual void Draw() override;
    virtual void Reset() override;
};