#include "NetworkedGameObject.h"
#include "Utils.h"
#include "NetworkInterface.h"

NetworkedGameObject::NetworkedGameObject(network_id n_id, object_type type, guid owner)
    : GameObject()
{
    m_NetworkID = n_id;
    m_Type = type;
    m_Owner = owner;
	m_NeedsLerp = false;
    m_IsLerping = false;
    m_SpawnedFlag = true;
}

void NetworkedGameObject::Update(double deltaT)
{
    if (m_IsLerping)
    {
		//printf("In Lerp\n");
        m_DeltaTimeAccumulation += (float)deltaT;

        float lerpAmt = Utils::clamp(m_DeltaTimeAccumulation / m_TimeToCompleteLerp, 0.f, 1.f);
		//printf("Time Step: %f\n", lerpAmt);
        printf("Time Step %f\n", m_TimeToCompleteLerp);
        float new_x = Utils::lerp(m_StartPos.x, m_LerpTarget.x, lerpAmt);
        float new_y = Utils::lerp(m_StartPos.y, m_LerpTarget.y, lerpAmt);

        float new_velX = Utils::lerp(m_StartVel.x, m_PickUpVelocity.x, lerpAmt);
        float new_velY = Utils::lerp(m_StartVel.y, m_PickUpVelocity.y, lerpAmt);

        if (m_DeltaTimeAccumulation >= m_TimeToCompleteLerp)
        {
            SetPosition(m_LerpTarget);
            SetVelocity(m_PickUpVelocity);
            m_IsLerping = false;

            m_TimeToCompleteLerp = 0.f;
            m_DeltaTimeAccumulation = 0.f;
            m_PickUpVelocity = sf::Vector2f(0, 0);
            m_LerpTarget = sf::Vector2f(0, 0);
            return;
        }

        SetVelocity(new_velX, new_velY);
        SetPosition(new_x, new_y);
        m_Sprite->setRotation(Utils::toDegreesf(m_Body->GetAngle()));
    }
    else
    {
        Super::Update(deltaT);
    }
}

void NetworkedGameObject::BeginLerp(sf::Vector2f target, double timeToComplete, sf::Vector2f pickUpVelocity)
{
	m_IsLerping = true;
    m_StartPos = GetPosition();
	m_LerpTarget = target;
    m_StartVel = GetVelocity();
	m_TimeToCompleteLerp = (float)timeToComplete;
	m_PickUpVelocity = pickUpVelocity;
	m_DeltaTimeAccumulation = 0.f;
}

NetworkedGameObject::network_id NetworkedGameObject::GetNetworkID() const
{ 
    return m_NetworkID; 
}

void NetworkedGameObject::SetNetworkID(network_id id)
{
    m_NetworkID = id;
}

NetworkedGameObject::object_type NetworkedGameObject::GetObjectType() const
{
    return m_Type;
}

void NetworkedGameObject::SetObjectType(object_type type)
{
    m_Type = type;
}

NetworkedGameObject::guid NetworkedGameObject::GetOwner() const
{
    return m_Owner;
}

void NetworkedGameObject::SetOwner(guid owner)
{
    m_Owner = owner;
}

const NGO_MetaData& NetworkedGameObject::GetPreviousState() const
{
    return m_PreviousState;
}

void NetworkedGameObject::SaveState()
{
    m_PreviousState.m_Position = GetPosition();
    m_PreviousState.m_Velocity = GetVelocity();
    m_PreviousState.m_TimeStamp = NetworkInterface::GetTime();
}

bool NetworkedGameObject::GetSpawnedFlag() const
{
    return m_SpawnedFlag;
}

void NetworkedGameObject::SetSpawnedFlag(bool value)
{
    m_SpawnedFlag = value;
}