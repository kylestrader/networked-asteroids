#pragma once

#include "GameObject.h"
#include <RakNetTypes.h>

struct NGO_MetaData
{
    NGO_MetaData(const sf::Vector2f& pos = sf::Vector2f(0, 0), const sf::Vector2f& vel = sf::Vector2f(0, 0), const RakNet::Time& timeStamp = 0.0)
    {
        m_Position = pos;
        m_Velocity = vel;
        m_TimeStamp = timeStamp;
    }

    sf::Vector2f m_Position;
    sf::Vector2f m_Velocity;
    RakNet::Time m_TimeStamp;
};

class  NetworkedGameObject: public GameObject
{
public:
	typedef unsigned int network_id;
	typedef int object_type;
	typedef GameObject Super;
    typedef RakNet::RakNetGUID guid;

    NetworkedGameObject(network_id n_id, object_type type, guid owner);

	virtual void Update(double deltaT) override;

	void BeginLerp(sf::Vector2f target, double timeToComplete, sf::Vector2f pickUpVelocity);

    network_id GetNetworkID() const;
    void SetNetworkID(network_id id);

    const NGO_MetaData& GetPreviousState() const;
    void SaveState();

    bool GetSpawnedFlag() const;
    void SetSpawnedFlag(bool value);

    guid GetOwner() const;
    void SetOwner(guid owner);

    object_type GetObjectType() const;
    void SetObjectType(object_type type);

private:
	network_id m_NetworkID;
    object_type m_Type;
	bool m_NeedsLerp;
    guid m_Owner;

    bool m_SpawnedFlag;
    NGO_MetaData m_PreviousState;

	bool m_IsLerping;
	sf::Vector2f m_LerpTarget;
    sf::Vector2f m_StartPos;
	float m_TimeToCompleteLerp;
    sf::Vector2f m_StartVel;
	sf::Vector2f m_PickUpVelocity;
	float m_DeltaTimeAccumulation;
};