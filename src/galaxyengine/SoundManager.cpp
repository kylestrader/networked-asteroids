#include "SoundManager.h"

SoundManager::SoundManager()
{
	m_Sound = new sf::Sound();
    m_Sound2 = new sf::Sound();
}

SoundManager::~SoundManager()
{
	delete m_Sound;
    delete m_Sound2;

	ClearSoundMap();
}

void SoundManager::ClearSoundMap()
{
	std::map<std::string, sf::SoundBuffer*>::iterator iter;

	for (iter = m_SoundFiles.begin(); iter != m_SoundFiles.end(); ++iter)
	{
		if (iter->second != NULL)
			delete m_SoundFiles[iter->first];
	}

	m_SoundFiles.clear();
}

bool SoundManager::InitializeSound(std::string sysUrl, std::string name)
{
	sf::SoundBuffer* soundBuffer = new sf::SoundBuffer();
	if (!soundBuffer->loadFromFile(sysUrl))
	{
		printf("Couldn't load sound at: %s", sysUrl.c_str());
		return false;
	}
	else
		m_SoundFiles[name] = soundBuffer;

	return true;
}

void SoundManager::playSound(std::string soundName)
{
	m_Sound->setBuffer(*m_SoundFiles[soundName]);
	m_Sound->play();
}

void SoundManager::playSound2(std::string soundName)
{
    m_Sound2->setBuffer(*m_SoundFiles[soundName]);
    m_Sound2->play();
}