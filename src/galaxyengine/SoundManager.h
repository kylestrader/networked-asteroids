#pragma once

#include <map>
#include <SFML\Audio.hpp>

class SoundManager
{
private:
	std::map<std::string, sf::SoundBuffer*> m_SoundFiles;
	sf::Sound* m_Sound;
    sf::Sound* m_Sound2;

public:
	SoundManager();
	~SoundManager();

	bool InitializeSound(std::string sysUrl, std::string name);
	void ClearSoundMap();

	void playSound(std::string soundName);
    void playSound2(std::string soundName);
};