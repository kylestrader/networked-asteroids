#include "TextureLoader.h"

TextureLoader::texture_map TextureLoader::m_Textures;

TextureLoader::TextureLoader()
{

}

TextureLoader::~TextureLoader()
{

}

void TextureLoader::LoadTextures()
{
    AddTexture("empty", sf::Texture());
    loadAssets();
}

void TextureLoader::AddTexture(std::string key, const sf::Texture& texture)
{
    m_Textures[key] = sf::Texture(texture);
}

sf::Texture& TextureLoader::GetTexture(std::string key)
{
    texture_map::iterator it = m_Textures.find(key);
    if (it == m_Textures.end())
    {
        printf("There is no texture with key %s\n", key.c_str());
        return m_Textures["empty"];
    }

    return m_Textures[key];
}

void TextureLoader::ClearTextures()
{
    m_Textures.clear();
}