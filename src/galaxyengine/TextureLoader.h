#pragma once

#include <map>
#include <string>
#include "SFML/Graphics/Texture.hpp"

class TextureLoader
{
public:
    typedef std::map<std::string, sf::Texture> texture_map;

private:
    static texture_map m_Textures;

protected:
    // Second Attempt is used to make sure this function doesn't infinitely call itself
    // This function should just be called without setting either parameter, it'll handle itself.
    virtual void loadAssets(std::string assetDir = "content/", bool secondAttempt = false) = 0;

public:
    TextureLoader();
    ~TextureLoader();

    void LoadTextures();
    static void AddTexture(std::string key, const sf::Texture& texture);
    static sf::Texture& GetTexture(std::string key);
    static void ClearTextures();

    sf::Texture& operator[](std::string key)
    {
        return GetTexture(key);
    }
};