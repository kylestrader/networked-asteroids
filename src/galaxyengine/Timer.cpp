#include "Timer.h"

double TimerHelper::MSToSec(double ms)
{
    return ms / 1000.0;
}

double TimerHelper::SecToMS(double sec)
{
    return sec * 1000.0;
}

void Timer::Start()
{
    m_StartTime = ch_timer::now();
    m_CurrentTime = m_StartTime;
    m_PreviousTime = m_StartTime;
}

void Timer::Stop()
{
    m_PreviousTime = m_CurrentTime;
    m_CurrentTime = ch_timer::now();
}

double Timer::Sleep_ms(double timeInMs)
{
    return sleep<ch_milliseconds>(timeInMs);
}

double Timer::Sleep_s(double timeInSec)
{
    return TimerHelper::MSToSec(sleep<ch_milliseconds>(TimerHelper::SecToMS(timeInSec)));
}

double Timer::GetElapsedTime_ms()
{
    return getElapsedTime<ch_milliseconds>();
}

double Timer::GetElapsedTime_s()
{
    return TimerHelper::MSToSec(getElapsedTime<ch_milliseconds>());
}

double Timer::GetTotalTime_ms()
{
    return getTotalTime<ch_milliseconds>();
}

double Timer::GetTotalTime_s()
{
    return TimerHelper::MSToSec(getTotalTime<ch_milliseconds>());
}