#pragma once

#include <chrono>

class TimerHelper
{
public:
    static double MSToSec(double ms);
    static double SecToMS(double sec);
};

// Wrapper for the chrono timer library. Documentation on it can be found here: http://www.cplusplus.com/reference/chrono/
class Timer
{
public:
    typedef std::chrono::high_resolution_clock  ch_timer;
    typedef ch_timer::time_point                ch_timepoint;
    typedef std::chrono::seconds                ch_seconds;
    typedef std::chrono::milliseconds           ch_milliseconds;

public:
    void Start();
    double Sleep_s(double timeInSec); // i.e. 0.016 seconds is 16 milliseconds
    double Sleep_ms(double timeInMs); // i.e. 500 milliseconds is 0.5 seconds
    void Stop();

    double GetElapsedTime_ms();
    double GetElapsedTime_s();
    double GetTotalTime_ms();
    double GetTotalTime_s();

private:
    // Dur is of type ch_seconds or ch_milliseconds
    // timeToWait is the time to wait in the units of Dur
    template <class Dur>
    double sleep(double timeToWait)
    {
        ch_timepoint start = ch_timer::now();
        ch_timepoint now;
        Dur elapsed;

        // Sleep until the desired time is reached
        while (true)
        {
            now = ch_timer::now();
            elapsed = std::chrono::duration_cast<Dur>(now - start);
            if (elapsed.count() >= timeToWait)
                return static_cast<double>(elapsed.count());
        }
    }

    // Dur is of type ch_seconds or ch_milliseconds
    template <class Dur>
    double getElapsedTime()
    {
        m_PreviousTime = m_CurrentTime;
        m_CurrentTime = ch_timer::now();
        Dur elapsed = std::chrono::duration_cast<Dur>(m_CurrentTime - m_PreviousTime);

        return static_cast<double>(elapsed.count());
    }

    // Dur is of type ch_seconds or ch_milliseconds
    template <class Dur>
    double getTotalTime()
    {
        m_CurrentTime = ch_timer::now();
        Dur elapsed = std::chrono::duration_cast<Dur>(m_CurrentTime - m_StartTime);

        return static_cast<double>(elapsed.count());
    }

private:
    ch_timepoint m_PreviousTime;
    ch_timepoint m_CurrentTime;
    ch_timepoint m_StartTime;
};