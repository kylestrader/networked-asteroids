#include "Ball.h"
#include "Box2D/Box2D.h"
#include "galaxyengine/Utils.h"
#include "GameObjectFactory.h"
#include "BallScoreEvent.h"
#include "galaxyengine/EventDispatcher.h"

Ball::Ball(network_id n_id)
    :NetworkedGameObject(n_id, (int)GameObjectTypes::BALL)
{
    m_Started = false;
}

Ball::~Ball()
{

}

void Ball::StartMovingServer(float& velocityX, float& velocityY)
{
    float32 directionX = Utils::generateNumber<float32>(0.0f, 2.0f);
    float32 directionY = Utils::generateNumber<float32>(0.0f, 2.0f);

    if (directionX < 0.5f)
        directionX = -1.0f;
    else
        directionX = 1.0f;

    if (directionY < 0.5f)
        directionY = -1.0f;
    else
        directionY = 1.0f;

    velocityX = directionX * Utils::generateNumber<float32>(10.0f, 20.0f);
    velocityY = directionY * Utils::generateNumber<float32>(0.0f, 10.0f);

    StartMovingClient(velocityX, velocityY);
}

void Ball::StartMovingClient(float velocityX, float velocityY)
{
    SetVelocity(velocityX, velocityY);

    m_Started = true;
}

void Ball::Update(double deltaT)
{
    Super::Update(deltaT);

    if (m_Started)
    {
        b2Vec2 vel = m_Body->GetLinearVelocity();
        if (vel.x > -5.f && vel.x < 0)
            vel.x = -5.f;
        if (vel.x < 5.f && vel.x >= 0)
            vel.x = 5.f;
        m_Body->SetLinearVelocity(vel);
    }

    if (!m_FlagKill)
    {
        sf::Vector2f pos = GetPosition();
        if (pos.x < 0)
        {
            Events::BallScoreEvent* ev = new Events::BallScoreEvent(GetNetworkID(), (int)GameObjectTypes::RIGHT_PLAYER);
            g_EventDispatcher->DispatchEvent(ev);
            m_FlagKill = true;
        }
        else if (pos.x > 1280)
        {
            Events::BallScoreEvent* ev = new Events::BallScoreEvent(GetNetworkID(), (int)GameObjectTypes::LEFT_PLAYER);
            g_EventDispatcher->DispatchEvent(ev);
            m_FlagKill = true;
        }
    }
}