#pragma once

#include "galaxyengine/NetworkedGameObject.h"
class b2Body;
class b2World;

class Ball : public NetworkedGameObject 
{
public:
    typedef unsigned int network_id;
    typedef NetworkedGameObject Super;

private:
    bool m_Started;

public:

    Ball(network_id n_id);
    ~Ball();

    void StartMovingServer(float& velocityX, float& velocityY);
    void StartMovingClient(float velocityX, float velocityY);

    virtual void Update(double deltaT) override;

	virtual void Dispose() override {};
};