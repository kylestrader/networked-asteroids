#include "BallScoreEvent.h"

using namespace Events;

const EventType BallScoreEvent::PLAYER_SCORED = "PLAYER_SCORED";

BallScoreEvent::BallScoreEvent(network_id id, int player)
    : Event(PLAYER_SCORED)
{
    m_NetID = id;
    m_Player = player;
}

BallScoreEvent::~BallScoreEvent()
{

}

int BallScoreEvent::GetPlayer() const
{
    return m_Player;
}

BallScoreEvent::network_id BallScoreEvent::GetNetworkID() const
{
    return m_NetID;
}

void BallScoreEvent::Dispose()
{

}