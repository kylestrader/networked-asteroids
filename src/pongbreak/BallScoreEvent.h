#pragma once

#include "galaxyengine/Event.h"

namespace Events
{
    
class BallScoreEvent : public Event
{
public:
    typedef int network_id;

private:
    int m_Player;
    int m_NetID;

public:
    BallScoreEvent(network_id id, int player); // Use GameObjectTypes
    ~BallScoreEvent();

    int GetPlayer() const;
    network_id GetNetworkID() const;

    virtual void Dispose();

    static const EventType PLAYER_SCORED;
};

}