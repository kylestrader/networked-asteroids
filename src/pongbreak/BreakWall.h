#pragma once

#include "galaxyengine/NetworkedGameObject.h"

class BreakWall : public NetworkedGameObject
{
public:
    typedef unsigned int network_id;
    typedef NetworkedGameObject Super;
    typedef int object_type;

public:
    BreakWall(network_id n_id, object_type type);
    ~BreakWall();

    virtual void Dispose() override {};
};