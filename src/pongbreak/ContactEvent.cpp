#include "ContactEvent.h"

using namespace Events;

const EventType ContactEvent::DELETE_WALL = "DELETE_WALL";

ContactEvent::ContactEvent(b2Body* body)
    : Event(DELETE_WALL)
{
    m_Body = body;
}

ContactEvent::~ContactEvent()
{

}

void ContactEvent::Dispose()
{

}

b2Body* ContactEvent::GetBody() const
{
    return m_Body;
}