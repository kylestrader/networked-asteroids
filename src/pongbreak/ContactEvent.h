#pragma once

#include "galaxyengine/Event.h"

class b2Body;

namespace Events
{

class ContactEvent : public Event
{
private:
    b2Body* m_Body;

public:
    ContactEvent(b2Body* body);
    ~ContactEvent();

    b2Body* GetBody() const;

    virtual void Dispose();

    static const EventType DELETE_WALL;
};

}