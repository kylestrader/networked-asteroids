#include "DeleteEvent.h"

using namespace Events;

const EventType DeleteEvent::DELETE_NETWORK_OBJECT = "DELETE_NETWORK_OBJECT";

DeleteEvent::DeleteEvent(network_id id)
    : Event(DELETE_NETWORK_OBJECT)
{
    m_NetworkID = id;
}

DeleteEvent::~DeleteEvent()
{

}

void DeleteEvent::Dispose()
{

}

DeleteEvent::network_id DeleteEvent::GetNetworkID() const
{
    return m_NetworkID;
}