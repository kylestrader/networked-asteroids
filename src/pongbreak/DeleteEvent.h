#pragma once

#include "galaxyengine/Event.h"

namespace Events
{

class DeleteEvent : public Event
{
public:
    typedef unsigned int network_id;

private:
    network_id m_NetworkID;

public:
    DeleteEvent(network_id id);
    ~DeleteEvent();

    network_id GetNetworkID() const;

    virtual void Dispose();

    static const EventType DELETE_NETWORK_OBJECT;
};

}