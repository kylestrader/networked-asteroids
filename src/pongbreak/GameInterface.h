#pragma once

#include <memory>
#include <vector>
#include <list>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <RakNetTypes.h>
#include "GameMessages.h"

#include "galaxyengine/NetworkedGameObject.h"

class SoundManager;
class PongBreak;
class NetworkMessageReader;

namespace Events
{
    class Delegate;
    class Event;
}

enum ControllerInput
{
	ZERO = 0,
	UP = 1,
	DOWN = 2
};

enum GameState
{
    GAME_ENGINE_LOGO = 0,
    GAME_WAITING_TO_START,
    GAME_START,
    GAME_PLAY,
	GAME_OVER,
    GAME_RESET
};

class GameInterface
{
public:
    typedef RakNet::RakNetGUID network_guid;
	typedef int network_id;

private:
    const double COUNT_DOWN_TIME; // In Seconds
    const double UPDATE_INTERVAL; // In Seconds
	const double THRESHOLD; // In Pixels

    GameState m_GameState;

    std::shared_ptr<sf::RenderWindow> m_Window;
    std::unique_ptr<PongBreak> m_Game;
	std::unique_ptr<SoundManager> m_SoundManager;

    bool m_IsServer;
    bool m_ExitGame;
    sf::Sprite m_Backgroud;
    int m_FPS;
    std::string m_Title;
    sf::Font m_Font;
    sf::Text m_Player1ScoreText;
    sf::Text m_Player2ScoreText;
    sf::Text m_CountDownText;

	// Input
	bool m_HoldingUp;
	bool m_HoldingDown;

    // Server
    bool m_Player1Assigned;
    bool m_Player2Assigned;
    network_guid m_Player1GUID;
    network_guid m_Player2GUID;
    int m_Player1NID;
    int m_Player2NID; // NID = Network ID
    int m_BallNID;
    double m_StartCountdown;
    double m_NextUpdateCounter;

    bool m_IsEngineLogoFading;

    double m_PreviousDeltaT;

	bool m_CanSendNetworkedMessages;

    int m_Player1Points;
    int m_Player2Points;

    Events::Delegate* m_NetworkEventDelegate;
    Events::Delegate* m_BallScoreDelegate;
    Events::Delegate* m_DeleteNetworkObjectDelegate;

private:
    void checkMousePress(sf::Mouse::Button button, double deltaT);
    void checkMouseRelease(sf::Mouse::Button button, double deltaT);
    void checkKeyRelease(sf::Keyboard::Key key, double deltaT);
    void checkKeyPress(sf::Keyboard::Key key, double deltaT);
    void loadAssets(std::string assetDir = "content/", bool secondAttempt = false);

    void onBallScoreEvent(Events::Event* e);
    void onNetworkEvent(Events::Event* e);
    void onDeleteNetworkObjectEvent(Events::Event* e);

    void restartGame();
    void setState(GameState state);

    void sendMoveMessage(ControllerInput input);
    void sendStartCountdownMessage();
    void sendStartMessage();
    void sendUpdateMessage();
    void sendCreateBallMessage();

    void resetBackground();
    void createWalls();
    void resetGame();

	void updateNetworkedObjects(unsigned char* msg, unsigned int size);
    void updateScoreText();
    void updateCountdownText();

public:
    GameInterface();
    ~GameInterface();

    inline bool ShouldExitGame() { return m_ExitGame; }

    void SetFPS(double fps);

    void Initialize(bool isServer);
    void UpdateGame(double deltaT); // deltaT is in seconds
    void OpenWindow();
    void CloseWindow();
};