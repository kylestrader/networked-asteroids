#pragma once

#include <MessageIdentifiers.h>
#include "galaxyengine/NetworkInterface.h"

// An extension of GameMessages which provides messages specific to this game
enum GameMessagesEx
{
    ID_GAME_MESSAGE_1 = GameMessages::NUM_MESSAGES + 1,
    ID_ASSIGN_PLAYER_MESSAGE,
    ID_PLAYER_MOVE_MESSAGE,
    ID_START_COUNTDOWN_MESSAGE,
    ID_START_GAME_MESSAGE,
    ID_UPDATE_NETWORK_OBJECTS_MESSAGE,
    ID_CREATE_NETWORK_OBJECT_MESSAGE,
    ID_DELETE_NETWORK_OBJECT_MESSAGE,
    ID_GAME_SCORE_MESSAGE,
    ID_GAME_RESET_MESSAGE
};

struct AssignPlayerMessage
{
    unsigned char MsgId;
    int player;
    int netId;
};

struct PlayerMoveMessage
{
    unsigned char MsgId;
    int netId;
	int dir;
};

struct StartGameMessage
{
    unsigned char MsgId;
    int ballId;
    float velocityX;
    float velocityY;
};

// We just use this struct to store the information from the message after the fact
// since we send it through the bitstream
struct UpdateNetworkObjectsMessage
{
    int netId;
    int type;
    float x;
    float y;
    float velocityX;
    float velocityY;
};

struct CreateNetworkObjectMessage
{
    unsigned char MsgId;
    int netId;
    int type;
    float x;
    float y;
    float velocityX;
    float velocityY;
};

struct DeleteNetworkObjectMessage
{
    unsigned char MsgId;
    int netId;
};

struct GameScoreMessage
{
    unsigned char MsgId;
    int player1Score;
    int player2Score;
};