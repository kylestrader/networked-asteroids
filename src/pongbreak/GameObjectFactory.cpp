#include "GameObjectFactory.h"
#include "galaxyengine/NetworkedGameObject.h"
#include "Paddle.h"
#include "Box2D/Box2D.h"
#include "Ball.h"
#include "PongBreakTextureLoader.h"
#include "BreakWall.h"

std::shared_ptr<NetworkedGameObject> GameObjectFactory::CreateNetworkedGameObjectByType(object_type id, network_id n_id, class b2World* world)
{
    std::shared_ptr<NetworkedGameObject> object = nullptr;

    switch (id)
    {
        case (int)GameObjectTypes::LEFT_PLAYER:
        {
            object = std::make_shared<Paddle>(n_id, (int)GameObjectTypes::LEFT_PLAYER);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_kinematicBody, (int)B2ShapeType::BOX, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)GameObjectTypes::LEFT_PLAYER;
            object->Initialize(world, p_body, PongBreakTextureLoader::GetTexture(PongBreakTextures::LEFT_PLAYER_PADDLE), sf::Vector2f(0.0f, 0.0f));
            break;
        }
        case (int)GameObjectTypes::RIGHT_PLAYER:
        {
            object = std::make_shared<Paddle>(n_id, (int)GameObjectTypes::RIGHT_PLAYER);
            B2D_BodyMetaData p_body = B2D_BodyMetaData(b2BodyType::b2_kinematicBody, (int)B2ShapeType::BOX, 1.f, 0.3f);
            p_body.m_Filter.groupIndex = (int16)GameObjectTypes::RIGHT_PLAYER;
            object->Initialize(world, p_body, PongBreakTextureLoader::GetTexture(PongBreakTextures::LEFT_PLAYER_PADDLE), sf::Vector2f(0.0f, 0.0f));
            break;
        }
        case (int)GameObjectTypes::BALL:
        {
            object = std::make_shared<Ball>(n_id);
            B2D_BodyMetaData b_body = B2D_BodyMetaData(b2BodyType::b2_dynamicBody, (int)B2ShapeType::CIRCLE, 1.f, 0.3f, 1.0f);
            b_body.m_Filter.groupIndex = (int16)GameObjectTypes::BALL;
            object->Initialize(world, b_body, PongBreakTextureLoader::GetTexture(PongBreakTextures::BALL), sf::Vector2f(640.f, 300.f));
            break;
        }
        case (int)GameObjectTypes::LEFT_WALL:
        {
            object = std::make_shared<BreakWall>(n_id, (int)GameObjectTypes::LEFT_WALL);
            B2D_BodyMetaData w_body = B2D_BodyMetaData(b2BodyType::b2_kinematicBody);
            w_body.m_Filter.groupIndex = (int16)GameObjectTypes::LEFT_WALL;
            object->Initialize(world, w_body, PongBreakTextureLoader::GetTexture(PongBreakTextures::LEFT_WALL), sf::Vector2f(0.0f, 0.0f));
            break;
        }
        case (int)GameObjectTypes::RIGHT_WALL:
        {
            object = std::make_shared<BreakWall>(n_id, (int)GameObjectTypes::RIGHT_WALL);
            B2D_BodyMetaData w_body = B2D_BodyMetaData(b2BodyType::b2_kinematicBody);
            w_body.m_Filter.groupIndex = (int16)GameObjectTypes::RIGHT_WALL;
            object->Initialize(world, w_body, PongBreakTextureLoader::GetTexture(PongBreakTextures::RIGHT_WALL), sf::Vector2f(0.0f, 0.0f));
            break;
        }
        default:
            break;
    }

    return object;
}