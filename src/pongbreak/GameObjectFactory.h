#include <memory>

enum class GameObjectTypes
{
    LEFT_PLAYER = 1,
    RIGHT_PLAYER,
    BALL,
    LEFT_WALL,
    RIGHT_WALL
};

class GameObjectFactory
{
public:
    typedef int object_type;
    typedef int network_id;

public:
	//static std::shared_ptr<GameObject> CreateGameObjectByType();
    static std::shared_ptr<class NetworkedGameObject> CreateNetworkedGameObjectByType(object_type id, network_id n_id, class b2World* world); // Use GameObjectTypes

private:
    GameObjectFactory(){};
    ~GameObjectFactory(){};
};