#include "Paddle.h"
#include "Box2D/Box2D.h"
#include "galaxyengine/Utils.h"

Paddle::Paddle(network_id n_id, object_type type)
    : NetworkedGameObject(n_id, type)
{

}

Paddle::~Paddle()
{

}

void Paddle::checkBounds()
{
    sf::Vector2f pos = GetPosition();
    if (pos.y < 104)
    {
        pos.y = 104;
        SetPosition(pos);
        m_Body->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
    }
    else if (pos.y > 533)
    {
        pos.y = 533;
        SetPosition(pos);
        m_Body->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
    }
}

void Paddle::Move(const sf::Vector2f& offset)
{
    Super::Move(offset);
    checkBounds();
}

void Paddle::Move(float offsetX, float offsetY)
{
    Super::Move(offsetX, offsetY);
    checkBounds();
}

void Paddle::Update(double deltaT)
{
    Super::Update(deltaT);

    checkBounds();
}