#pragma once

#include "galaxyengine/NetworkedGameObject.h"

class Paddle : public NetworkedGameObject
{
public:
    typedef unsigned int network_id;
    typedef NetworkedGameObject Super;
    typedef int object_type;

private:
    void checkBounds(); // Sadly we need bounds checking since the paddle needs to be a kinematic body

public:
    Paddle(network_id n_id, object_type type);
    ~Paddle();

	virtual void Dispose() override {};
    virtual void Move(float offsetX, float offsetY) override;
    virtual void Move(const sf::Vector2f& offset) override;
    virtual void Update(double deltaT) override;
};