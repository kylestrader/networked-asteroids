#include "PongBreak.h"
#include "Ball.h"
#include "Paddle.h"
#include "Box2D/Box2D.h"
#include "galaxyengine/Utils.h"
#include "galaxyengine/DebugDraw.h"
#include "galaxyengine/GameObject.h"
#include "galaxyengine/NetworkedGameObject.h"
#include "GameMessages.h"
#include "PongBreakContactListener.h"
#include "galaxyengine/EventDispatcher.h"
#include "galaxyengine/Delegate.h"
#include "ContactEvent.h"
#include "DeleteEvent.h"
#include "GameObjectFactory.h"

int PongBreak::m_NetworkID = 0;

PongBreak::PongBreak()
    :PADDLE_SPEED(30.f),
    BOX2D_TIME_STEP(1.0f / 60.0f),
    BOX2D_VELOCITY_ITERATIONS(6),
    BOX2D_POSITION_ITERATIONS(2)
{
    m_DebugMode = false;
}

PongBreak::~PongBreak()
{
    g_EventDispatcher->RemoveEventListener(Events::ContactEvent::DELETE_WALL, m_OnContactDelegate);

    for (network_map::iterator it = m_NetworkedObjects.begin(); it != m_NetworkedObjects.end(); it++)
    {
        it->second.reset();
        it->second = nullptr;
    }

    m_NetworkedObjects.clear();
    
    m_MyPaddle.reset();
    m_MyPaddle = nullptr;

    delete m_Box2DDebugDrawer;
    m_Box2DDebugDrawer = nullptr;

    // Clean up our world and due to Box2D implementation, it will clean up all of its parts for us :)
    delete m_World;
    m_World = nullptr;
}

void PongBreak::Initialize(std::shared_ptr<sf::RenderWindow> window)
{
    m_OnContactDelegate = Events::Delegate::Create<PongBreak, &PongBreak::onContactEvent>(this);
    g_EventDispatcher->AddEventListener(Events::ContactEvent::DELETE_WALL, m_OnContactDelegate);

    m_Window = window;

    m_MyPaddle = nullptr;
    m_PlayerID = -1;

    // Create our box2d world
    createBox2DWorld();
}

void PongBreak::Update(double deltaT)
{
    m_World->Step(BOX2D_TIME_STEP, BOX2D_VELOCITY_ITERATIONS, BOX2D_POSITION_ITERATIONS);

    for (network_map::iterator it = m_NetworkedObjects.begin(); it != m_NetworkedObjects.end(); it++)
    {
        if (it->second == nullptr)
            continue;

        it->second->Update(deltaT);
        if (it->second->GetFlagKill())
        {
            m_ObjectsToDelete.push_back(it->first);
        }
    }

    if (m_ObjectsToDelete.size() > 0)
    {
        for (unsigned int i = 0; i < m_ObjectsToDelete.size(); i++)
        {
            m_NetworkedObjects.erase(m_ObjectsToDelete[i]);
        }

        m_ObjectsToDelete.clear();
    }
}

void PongBreak::Draw()
{
    for (network_map::iterator it = m_NetworkedObjects.begin(); it != m_NetworkedObjects.end(); it++)
    {
        it->second->Draw(m_Window);
    }

    if (m_DebugMode)
        m_World->DrawDebugData();
}

void PongBreak::Reset()
{

}

int PongBreak::GetNetworkMapSize() const
{
    return m_NetworkedObjects.size();
}

void PongBreak::ClearNetworkID()
{
    m_NetworkID = 0;
}

PongBreak::network_map::iterator PongBreak::GetNetworkMapBegin()
{
    return m_NetworkedObjects.begin();
}

PongBreak::network_map::iterator PongBreak::GetNetworkMapEnd()
{
    return m_NetworkedObjects.end();
}

PongBreak::network_id PongBreak::CreateWall(network_id id, int type)
{
    std::shared_ptr<NetworkedGameObject> wall = nullptr;
    switch (type)
    {
        case (int)GameObjectTypes::LEFT_WALL:
            wall = GameObjectFactory::CreateNetworkedGameObjectByType((int)GameObjectTypes::LEFT_WALL, id, m_World);
            break;
        case (int)GameObjectTypes::RIGHT_WALL:
            wall = GameObjectFactory::CreateNetworkedGameObjectByType((int)GameObjectTypes::RIGHT_WALL, id, m_World);
            break;
        default:
            break;
    }

    m_NetworkedObjects[id] = wall;
    return id;
}

PongBreak::network_id PongBreak::CreatePlayer(PlayerPaddle paddle)
{
    return CreatePlayer(paddle, m_NetworkID++);
}

PongBreak::network_id PongBreak::CreatePlayer(PlayerPaddle paddle, network_id n_id)
{
    std::shared_ptr<NetworkedGameObject> player = nullptr;
    switch (paddle)
    {
        case LEFT:
            player = GameObjectFactory::CreateNetworkedGameObjectByType((int)GameObjectTypes::LEFT_PLAYER, n_id, m_World);
            player->SetPosition(220.f, 300.f);
            break;
        case RIGHT:
            player = GameObjectFactory::CreateNetworkedGameObjectByType((int)GameObjectTypes::RIGHT_PLAYER, n_id, m_World);
            player->SetPosition(1040.f, 300.f);
            break;
        default:
            return -1;
    }

    m_NetworkedObjects[n_id] = player;
    return n_id;
}

PongBreak::network_id PongBreak::CreateBall()
{
    return CreateBall(m_NetworkID++);
}

PongBreak::network_id PongBreak::CreateBall(network_id n_id)
{
    std::shared_ptr<NetworkedGameObject> ball = nullptr;

    ball = GameObjectFactory::CreateNetworkedGameObjectByType((int)GameObjectTypes::BALL, n_id, m_World);
    m_NetworkedObjects[n_id] = ball;
    return n_id;
}

void PongBreak::CreatePlayerWall(int type)
{
    switch (type)
    {
        case (int)GameObjectTypes::LEFT_WALL:
        {
            float startX = 168.f;
            float startY = 107.f;
            float xDiff = 0.f;
            float yDiff = 36.f;

            // Create back wall
            createWalls((int)GameObjectTypes::LEFT_WALL, 14, startX, startY, xDiff, yDiff);

            // Create forward wall
            createWalls((int)GameObjectTypes::LEFT_WALL, 14, startX + 16.f, startY, xDiff, yDiff);
            break;
        }
        case (int)GameObjectTypes::RIGHT_WALL:
        {
            float startX = 1080.f;
            float startY = 107.f;
            float xDiff = 0.f;
            float yDiff = 36.f;

            // Create forward wall
            createWalls((int)GameObjectTypes::RIGHT_WALL, 14, startX, startY, xDiff, yDiff);

            // Create back wall
            createWalls((int)GameObjectTypes::RIGHT_WALL, 14, startX + 16.f, startY, xDiff, yDiff);
            break;
        }
    }
}

void PongBreak::createWalls(int wallType, int num, float startX, float startY, float xDiff, float yDiff)
{
    float x = startX;
    float y = startY;

    for (int i = 0; i < num; i++)
    {
        std::shared_ptr<NetworkedGameObject> wall = GameObjectFactory::CreateNetworkedGameObjectByType(wallType, m_NetworkID, m_World);
        wall->SetPosition(x, y);
        m_NetworkedObjects[m_NetworkID++] = wall;

        x += xDiff;
        y += yDiff;
    }
}

void PongBreak::StartBallClient(network_id n_id, float velocityX, float velocityY)
{
    ((Ball*)m_NetworkedObjects[n_id].get())->StartMovingClient(velocityX, velocityY);
}

void PongBreak::StartBallServer(network_id n_id, float& velocityX, float& velocityY)
{
    ((Ball*)m_NetworkedObjects[n_id].get())->StartMovingServer(velocityX, velocityY);
}

bool PongBreak::GetDebugMode() const
{
    return m_DebugMode;
}

void PongBreak::SetFont(sf::Font font)
{
    m_Font = font;
}

void PongBreak::SetDebugMode(bool value)
{
    m_DebugMode = value;
}

void PongBreak::CreateNetworkObject(const CreateNetworkObjectMessage& obj, float updateInterval, const float THRESHOLD)
{
    updateNetworkObject(obj.netId, obj.type, obj.x, obj.y, obj.velocityX, obj.velocityY, updateInterval, THRESHOLD);
}

void PongBreak::DeleteNetworkObject(network_id id)
{
    m_NetworkedObjects.erase(id);
}

std::shared_ptr<NetworkedGameObject> PongBreak::GetNetworkObject(network_id id)
{
    network_map::iterator it = m_NetworkedObjects.find(id);
    if (it != m_NetworkedObjects.end())
        return m_NetworkedObjects[id];
    else
        return nullptr;
}

//DEPRECATED: See declaration!
//void PongBreak::MoveUp(double deltaT)
//{
//    m_MyPaddle->Move(0.f, -PADDLE_SPEED * (float)deltaT);
//}

//DEPRECATED: See declaration!
//void PongBreak::MoveDown(double deltaT)
//{
//    m_MyPaddle->Move(0.f, PADDLE_SPEED * (float)deltaT);
//}

//DEPRECATED: See declaration!
//void PongBreak::StopMoving()
//{
//    m_MyPaddle->Move(0.f, 0.f);
//}

void PongBreak::SetPlayer(network_id n_id)
{
    m_PlayerID = n_id;
    m_MyPaddle = std::dynamic_pointer_cast<Paddle>(m_NetworkedObjects[n_id]);
}

int PongBreak::GetPlayerID() const
{
    return m_PlayerID;
}

sf::Vector2f PongBreak::GetLocalPaddlePosition()
{
    return m_MyPaddle->GetPosition();
}

sf::Vector2f PongBreak::GetLocalPaddleVelocity()
{
	return m_MyPaddle->GetVelocity();
}

void PongBreak::SetLocalPaddlePosition(sf::Vector2f pos)
{
	m_MyPaddle->SetPosition(pos);
}

void PongBreak::SetLocalPaddleVelocity(sf::Vector2f vec)
{
	m_MyPaddle->SetVelocity(vec);
}

void PongBreak::MovePlayer(network_id n_id, float x, float y)
{
    m_NetworkedObjects[n_id]->SetPosition(x, y);
}

void PongBreak::SetPaddleDirection(network_id n_id, ControllerInput input)
{
	sf::Vector2i v;
	switch (input)
	{
	case ControllerInput::ZERO:
		v = sf::Vector2i(0, 0);
		break;
	case ControllerInput::UP:
		v = sf::Vector2i(0, -1);
		break;
	case ControllerInput::DOWN:
		v = sf::Vector2i(0, 1);
		break;
	}

	m_NetworkedObjects[n_id]->SetVelocity(v.x * PADDLE_SPEED, v.y * PADDLE_SPEED);
}

void PongBreak::SetLocalPaddleDirection(ControllerInput input)
{
	sf::Vector2i v;
	switch (input)
	{
	case ControllerInput::ZERO:
		v = sf::Vector2i(0, 0);
		break;
	case ControllerInput::UP:
		v = sf::Vector2i(0, -1);
		break;
	case ControllerInput::DOWN:
		v = sf::Vector2i(0, 1);
		break;
	}

	SetLocalPaddleVelocity(sf::Vector2f(v.x * PADDLE_SPEED, v.y * PADDLE_SPEED));
}

void PongBreak::createBox2DWorld()
{
    b2Vec2 gravity(0.0f, 0.0f); // We don't want any gravity
    m_World = new b2World(gravity);

    uint32 flags = b2Draw::e_shapeBit;
    //flags += b2DebugDraw::e_jointBit;
    //flags += b2DebugDraw::e_aabbBit;
    //flags += b2DebugDraw::e_pairBit;
    //flags += b2DebugDraw::e_centerOfMassBit;

    m_Box2DDebugDrawer = new DebugDraw(m_Window, m_Font);
    m_Box2DDebugDrawer->SetFlags(flags);
    m_World->SetDebugDraw(m_Box2DDebugDrawer);

    float x = 0.0f,
          y = 0.0f,
          middleX = (float)m_Window->getSize().x / 2;

    Utils::b2_PixelsToMeters(middleX, 99, x, y);

    b2BodyDef topWallDef;
    topWallDef.position.Set(x, y);
    b2PolygonShape wallBox;
    wallBox.SetAsBox(Utils::b2_PixelsToMeters(middleX), Utils::b2_PixelsToMeters(10 / 2));

    m_TopWall = m_World->CreateBody(&topWallDef);
    m_TopWall->CreateFixture(&wallBox, 0.0f);

    Utils::b2_PixelsToMeters(middleX, 618, x, y);

    b2BodyDef botWallDef;
    botWallDef.position.Set(x, y);

    m_BottomWall = m_World->CreateBody(&botWallDef);
    m_BottomWall->CreateFixture(&wallBox, 0.0f);

    m_World->SetContactListener(&m_ContactListener);
}

void PongBreak::UpdateNetworkObject(const UpdateNetworkObjectsMessage& delta, float updateInterval, const float THRESHOLD)
{
    updateNetworkObject(delta.netId, delta.type, delta.x, delta.y, delta.velocityX, delta.velocityY, updateInterval, THRESHOLD);
}

void PongBreak::updateNetworkObject(int netId, int type, float x, float y, float velocityX, float velocityY, float updateInterval, const float THRESHOLD)
{
    // Create the object if it doesn't exist
    if (isNetIdNew(netId))
    {
        switch (type)
        {
        case (int)GameObjectTypes::LEFT_PLAYER:
            CreatePlayer(PlayerPaddle::LEFT, netId);
            break;
        case (int)GameObjectTypes::RIGHT_PLAYER:
            CreatePlayer(PlayerPaddle::RIGHT, netId);
            break;
        case (int)GameObjectTypes::BALL:
            CreateBall(netId);
            break;
        case (int)GameObjectTypes::LEFT_WALL:
        case (int)GameObjectTypes::RIGHT_WALL:
            CreateWall(netId, type);
            break;
        }
    }

    /*
    Dead Reckoning : Latency Hiding for Networked Games:
    http://www.gamasutra.com/view/feature/131638/dead_reckoning_latency_hiding_for_.php
    */
    //1) Get estimated Dead Reckoning position.
    //2) If the DR position is far enough away from our actual position, update out network object's position.

    //1)
    sf::Vector2f drVel = m_NetworkedObjects[netId]->GetVelocity();
    sf::Vector2f drPos = m_NetworkedObjects[netId]->GetPosition() + sf::Vector2f(Utils::b2_MetersToPixels(drVel.x * updateInterval), Utils::b2_MetersToPixels(drVel.y * updateInterval));

    //2)
    sf::Vector2f diff = drPos - sf::Vector2f(x, y);
    diff.x = std::abs(diff.x);
    diff.y = std::abs(diff.y);

    if (diff.x > THRESHOLD || diff.y > THRESHOLD)
	    m_NetworkedObjects[netId]->BeginLerp(sf::Vector2f(x, y), updateInterval, sf::Vector2f(velocityX, velocityY));
}

bool PongBreak::isNetIdNew(network_id n_id)
{
    network_map::iterator it = m_NetworkedObjects.find(n_id);
    if (it != m_NetworkedObjects.end())
        return false;
    else
        return true;
}

void PongBreak::onContactEvent(Events::Event* e)
{
    Events::ContactEvent* ev = (Events::ContactEvent*)e;

    network_map::iterator it = m_NetworkedObjects.begin();

    for (; it != m_NetworkedObjects.end(); it++)
    {
        if (*it->second == ev->GetBody())
        {
            Events::DeleteEvent* nev = new Events::DeleteEvent(it->second->GetNetworkID());
            g_EventDispatcher->DispatchEvent(nev);

            it->second->Kill();
            break;
        }
    }
}