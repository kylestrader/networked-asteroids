#pragma once

#include "galaxyengine/IGame.h"
#include "GameInterface.h"
#include "PongBreakContactListener.h"
#include <map>
#include <vector>
class Paddle;
class Ball;
class b2World;
class b2Body;
class DebugDraw;
class NetworkedGameObject;
struct UpdateNetworkObjectsMessage;
struct CreateNetworkObjectMessage;

namespace Events
{
    class Delegate;
}

enum PlayerPaddle
{
    NONE = -1,
    LEFT,
    RIGHT
};

class PongBreak : public IGame
{
public:
    typedef unsigned int network_id;
    typedef std::map<network_id, std::shared_ptr<NetworkedGameObject>> network_map;

private:
    const float PADDLE_SPEED;
    const float BOX2D_TIME_STEP;
    const int BOX2D_VELOCITY_ITERATIONS;
    const int BOX2D_POSITION_ITERATIONS;

    static int m_NetworkID;

    sf::Font m_Font;

    b2World* m_World; // The Box2D world
    b2Body* m_TopWall; // The top wall of the arena
    b2Body* m_BottomWall; // The bottom wall of the arena
    PongBreakContactListener m_ContactListener;
    DebugDraw* m_Box2DDebugDrawer;

    Events::Delegate* m_OnContactDelegate;

    bool m_DebugMode;

    std::shared_ptr<sf::RenderWindow> m_Window; // Just a ref if needed
    network_map m_NetworkedObjects;

    std::vector<network_id> m_ObjectsToDelete; // A list of objects which require deletion

    std::shared_ptr<Paddle> m_MyPaddle; // We keep a reference to either the left or right paddle here to keep the code simpler
    int m_PlayerID;

private:
    void createBox2DWorld();
    bool isNetIdNew(network_id n_id);
	void updateNetworkObject(int netId, int type, float x, float y, float velocityX, float velocityY, float updateInterval, const float THRESHOLD);

    void createWalls(int wallType, int num, float startX, float startY, float xDiff, float yDiff);

    void onContactEvent(Events::Event* e);

public:
    PongBreak();
    ~PongBreak();

    void ClearNetworkID();

    int GetNetworkMapSize() const;
    network_map::iterator GetNetworkMapBegin();
    network_map::iterator GetNetworkMapEnd();

    void StartBallClient(network_id n_id, float velocityX, float velocityY);
    void StartBallServer(network_id n_id, float& velocityX, float& velocityY);

    void CreatePlayerWall(int type); // Use GameObjectTypes
    network_id CreateWall(network_id id, int type); // Use GameObjectTypes

    network_id CreatePlayer(PlayerPaddle paddle);
    network_id CreatePlayer(PlayerPaddle paddle, network_id n_id);
    network_id CreateBall();
    network_id CreateBall(network_id n_id);

    void CreateNetworkObject(const CreateNetworkObjectMessage& obj, float updateInterval, const float THRESHOLD);
    void UpdateNetworkObject(const UpdateNetworkObjectsMessage& delta, float updateInterval, const float THRESHOLD);
    void DeleteNetworkObject(network_id id);
    std::shared_ptr<NetworkedGameObject> GetNetworkObject(network_id id);

    bool GetDebugMode() const;
    void SetDebugMode(bool value);
    void SetFont(sf::Font font);

    void SetPlayer(network_id n_id);
    int GetPlayerID() const;

	sf::Vector2f GetLocalPaddlePosition();
	sf::Vector2f GetLocalPaddleVelocity();

	void SetLocalPaddlePosition(sf::Vector2f pos);
	void SetLocalPaddleVelocity(sf::Vector2f vel);

    //void MoveUp(double deltaT); //Deprecated due to implementation of Dead Reckoning
    //void MoveDown(double deltaT); //Deprecated due to implementation of Dead Reckoning
    //void StopMoving();

    void MovePlayer(network_id n_id, float x, float y); // Used primarily by server
	void SetPaddleDirection(network_id n_id, ControllerInput input); // Used primarily by server
	void SetLocalPaddleDirection(ControllerInput input); // Used primarily by client locally

    virtual void Initialize(std::shared_ptr<sf::RenderWindow> window) override;
    virtual void Update(double deltaT) override;
    virtual void Draw() override;
    virtual void Reset() override;
};