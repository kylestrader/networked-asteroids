#include "PongBreakContactListener.h"
#include "Box2D/Box2D.h"
#include "GameObjectFactory.h"
#include "ContactEvent.h"
#include "galaxyengine/EventDispatcher.h"
#include "galaxyengine/NetworkInterface.h"

PongBreakContactListener::PongBreakContactListener()
{

}

PongBreakContactListener::~PongBreakContactListener()
{

}

void PongBreakContactListener::EndContact(b2Contact* contact)
{
    b2Fixture* fixtureA = contact->GetFixtureA();
    b2Fixture* fixtureB = contact->GetFixtureB();

    if (fixtureA->GetFilterData().groupIndex == (int16)GameObjectTypes::BALL)
    {
        if (fixtureB->GetFilterData().groupIndex == (int16)GameObjectTypes::LEFT_WALL ||
            fixtureB->GetFilterData().groupIndex == (int16)GameObjectTypes::RIGHT_WALL)
        {
            if (NetworkInterface::IsServer())
            {
                printf("Collision between Ball and Wall detected\n");
                Events::ContactEvent* ev = new Events::ContactEvent(fixtureB->GetBody());
                g_EventDispatcher->DispatchEvent(ev);
            }
        }
    }
    else if (fixtureB->GetFilterData().groupIndex == (int16)GameObjectTypes::BALL)
    {
        if (fixtureA->GetFilterData().groupIndex == (int16)GameObjectTypes::LEFT_WALL ||
            fixtureA->GetFilterData().groupIndex == (int16)GameObjectTypes::RIGHT_WALL)
        {
            if (NetworkInterface::IsServer())
            {
                printf("Collision between Ball and Wall detected\n");
                Events::ContactEvent* ev = new Events::ContactEvent(fixtureA->GetBody());
                g_EventDispatcher->DispatchEvent(ev);
            }
        }
    }
}