#pragma once

#include "Box2D/Dynamics/b2WorldCallbacks.h"

class PongBreakContactListener : public b2ContactListener
{
public:
    PongBreakContactListener();
    ~PongBreakContactListener();

    virtual void EndContact(b2Contact* contact) override;
};