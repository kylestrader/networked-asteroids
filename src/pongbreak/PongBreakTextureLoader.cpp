#include "PongBreakTextureLoader.h"

const std::string PongBreakTextures::LEFT_PLAYER_PADDLE = "left_player_paddle";
const std::string PongBreakTextures::RIGHT_PLAYER_PADDLE = "right_player_paddle";
const std::string PongBreakTextures::LEFT_WALL = "left_wall";
const std::string PongBreakTextures::RIGHT_WALL = "right_wall";
const std::string PongBreakTextures::BALL = "ball";
const std::string PongBreakTextures::BACKGROUND = "background";
const std::string PongBreakTextures::ENGINE_LOGO = "engine_logo";

void PongBreakTextureLoader::loadAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */)
{
    sf::Texture leftPlayer,
                rightPlayer,
                ball,
                background,
                engineLogo,
                leftWall,
                rightWall;

    // Put all asset loading in this try and ALWAYS throw if it fails
    try
    {
        if (!leftPlayer.loadFromFile(assetDir + "left_paddle.png", sf::IntRect(0, 0, 20, 80)))
            throw 0;
        if (!rightPlayer.loadFromFile(assetDir + "right_paddle.png", sf::IntRect(0, 0, 20, 80)))
            throw 0;
        if (!leftWall.loadFromFile(assetDir + "left_wall.png", sf::IntRect(0, 0, 16, 34)))
            throw 0;
        if (!rightWall.loadFromFile(assetDir + "right_wall.png", sf::IntRect(0, 0, 16, 34)))
            throw 0;
        if (!ball.loadFromFile(assetDir + "ball.png", sf::IntRect(0, 0, 20, 80)))
            throw 0;
        if (!background.loadFromFile(assetDir + "board.png", sf::IntRect(0, 0, 50, 0)))
            throw 0;
        if (!engineLogo.loadFromFile(assetDir + "GalaxyEngine.png", sf::IntRect(0, 0, 50, 0)))
            throw 0;
    }
    catch (...)
    {
        if (!secondAttempt)
        {
            printf("Unable to find a texture, trying another directory...\n");
            loadAssets("../content/", true);
            return;
        }
    }

    AddTexture(textures::LEFT_PLAYER_PADDLE, leftPlayer);
    AddTexture(textures::RIGHT_PLAYER_PADDLE, rightPlayer);
    AddTexture(textures::LEFT_WALL, leftWall);
    AddTexture(textures::RIGHT_WALL, rightWall);
    AddTexture(textures::BALL, ball);
    AddTexture(textures::BACKGROUND, background);
    AddTexture(textures::ENGINE_LOGO, engineLogo);
}