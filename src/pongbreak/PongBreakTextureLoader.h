#include "galaxyengine/TextureLoader.h"

class PongBreakTextures
{
public:
    static const std::string LEFT_PLAYER_PADDLE;
    static const std::string RIGHT_PLAYER_PADDLE;
    static const std::string LEFT_WALL;
    static const std::string RIGHT_WALL;
    static const std::string BALL;
    static const std::string BACKGROUND;
    static const std::string ENGINE_LOGO;
};

class PongBreakTextureLoader : public TextureLoader
{
public:
    typedef PongBreakTextures textures;

protected:
    virtual void loadAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */) override;
};