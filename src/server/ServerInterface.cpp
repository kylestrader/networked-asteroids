#include "ServerInterface.h"
#include "asteroids/Asteroids.h"
#include "galaxyengine/NetworkedGameObject.h"
#include "asteroids/AsteroidsTextureLoader.h"
#include "asteroids/BreakableAsteroid.h"
#include "asteroids/GameObjectFactory.h"
#include "galaxyengine/Event.h"
#include "galaxyengine/NetworkEvent.h"
#include "galaxyengine/Delegate.h"
#include "galaxyengine/EventDispatcher.h"
#include "galaxyengine/Utils.h"
#include "asteroids/GameMessages.h"
#include "asteroids/PlayerEvent.h"

ServerInterface::ServerInterface()
    : COUNT_DOWN_TIME(3.0)
{
    
}

ServerInterface::~ServerInterface()
{

}

void ServerInterface::initializeTextures()
{
    Super::initializeTextures();

    AsteroidsTextureLoader textures;
    textures.LoadTextures();
}

void ServerInterface::createGame()
{
    m_Game = std::make_shared<Asteroids>();
    m_Game->SetFont(GetDefaultFont());
    m_Game->SetDebugMode(true);
    m_Game->ClearNetworkID();
    //SetThreshold(4.66);
    SetThreshold(10.0);
}

void ServerInterface::initializeGame()
{
    Super::initializeGame();

    m_PlayerScreenOffset = 200;

    m_PlayerIDList.clear();

    m_KillPlayerDelegate = Events::Delegate::Create<ServerInterface, &ServerInterface::onKillPlayer>(this);
    g_EventDispatcher->AddEventListener(PlayerEvent::KILL_PLAYER, m_KillPlayerDelegate);

    m_CountDownText.setFont(GetDefaultFont());
    m_CountDownText.setCharacterSize(75);
    m_CountDownText.setPosition(640.f, 20.f);
    m_CountDownText.setColor(sf::Color::White);
    updateCountdownText();

    m_LoseBackground.setTexture(AsteroidsTextureLoader::GetTexture(AsteroidsTextures::GAME_OVER_BACKGROUND));
    m_LoseBackground.setPosition(0.f, 0.f);

    m_RetryText.setFont(GetDefaultFont());
    m_RetryText.setCharacterSize(24);
    m_RetryText.setColor(sf::Color::White);

    m_LoserText.setFont(GetDefaultFont());
    m_LoserText.setCharacterSize(36);
    m_LoserText.setColor(sf::Color(0, 255, 246, 255));

    m_WinnerText.setFont(GetDefaultFont());
    m_WinnerText.setCharacterSize(36);
    m_WinnerText.setColor(sf::Color(0, 255, 246, 255));

    m_InputDirection = sf::Vector2i(0, 0);

    m_PlayerSpeed = 6.f;

    m_StartCountdown = 0.0;
    m_GameLost = false;
    m_IsPlayer1Dead = false;
    m_IsPlayer2Dead = false;
    m_WantsRematch = false;
    m_PlayerDC = false;
}

void ServerInterface::loadAssets(std::string assetDir /* = "content/" */, bool secondAttempt /* = false */)
{

}

void ServerInterface::disposeGame()
{
    Super::disposeGame();

    g_EventDispatcher->RemoveEventListener(PlayerEvent::KILL_PLAYER, m_KillPlayerDelegate);
}

void ServerInterface::checkMousePress(Button button, double deltaT)
{
    Super::checkMousePress(button, deltaT);
}

void ServerInterface::checkMouseRelease(Button button, double deltaT)
{
    Super::checkMouseRelease(button, deltaT);

    // Shoot from the center of the screen in the direction of the mouse
    if (button == Button::Left || button == Button::Right)
    {
        int type;

        if (button == Button::Left)
            type = (int)GameObjectTypes::PLAYER_1_PROJECTILE;
        else if (button == Button::Right)
            type = (int)GameObjectTypes::PLAYER_2_PROJECTILE;

        sf::Vector2i mousePos = sf::Mouse::getPosition(*m_Window);
        const float SPEED = 15.f;

        // Calculate a direction vector
        sf::Vector2f dirVec;
        dirVec.x = (float)(mousePos.x - 640);
        dirVec.y = (float)(mousePos.y - 360);
        Utils::normalize(dirVec.x, dirVec.y);

        // Calculate a velocity
        sf::Vector2f velVec;
        velVec.x = SPEED * dirVec.x;
        velVec.y = SPEED * dirVec.y;

        GetGame<Asteroids>()->CreateProjectile(type, NetworkInterface::GetSessionGuid(), 640.f, 360.f,  velVec.x, velVec.y);
    }
}

void ServerInterface::checkKeyRelease(Key key, double deltaT)
{
    Super::checkKeyRelease(key, deltaT);

    switch (key)
    {
        // Destroy a random asteroid
        case Key::R:
        {
            network_id id = GetGame<Asteroids>()->DestroyRandomAsteroid();

            if (id != -1)
                sendDeleteMessage(id);
            break;
        }
        // Destroy all asteroids
        case Key::T:
        {
            std::vector<network_id> ids = GetGame<Asteroids>()->DestroyAllAsteroids();
            sendMultipleDeleteMessage(ids);

            break;
        }
        // Start Next Level
        case Key::S:
        {
            // Clear the existing asteroids
            std::vector<network_id> ids = GetGame<Asteroids>()->RemoveAllAsteroids();
            sendMultipleDeleteMessage(ids);

            // Spawn the next level
            GetGame<Asteroids>()->StartNextLevel();
            break;
        }
        // Kill all players
        case Key::E:
        {
            g_EventDispatcher->DispatchEvent(new PlayerEvent(1));
            g_EventDispatcher->DispatchEvent(new PlayerEvent(2));
            break;
        }
        default:
            break;
    }
}

void ServerInterface::checkKeyPress(Key key, double deltaT)
{
    Super::checkKeyPress(key, deltaT);
}

void ServerInterface::onNetworkEvent(Event* e)
{
    Super::onNetworkEvent(e);
    Events::NetworkEvent* ev = (Events::NetworkEvent*)e;

    switch (ev->GetMsgId())
    {
    case GameMessages::ID_PLAYER_CONNECTED_MESSAGE:
    {
        if (IsServer())
        {
            int numPlayers = m_PlayerIDList.size();
            if (numPlayers < 2)
            {
                NetworkedGame::network_id newPlayerID;
                switch (numPlayers)
                {
                case 0:
                    newPlayerID = GetGame<Asteroids>()->CreatePlayer((int)GameObjectTypes::PLAYER_1, ev->GetGUID(), (float)((m_Window->getSize().x / 2) - m_PlayerScreenOffset), (float)(m_Window->getSize().y / 2));
                    sendNetworkedGameObjectID(newPlayerID, (int)GameObjectTypes::PLAYER_1, ev->GetGUID());
                    m_PlayerIDList[ev->GetGUID()] = newPlayerID;
                    m_P1GUID = ev->GetGUID();
                    break;
                case 1:
                    newPlayerID = GetGame<Asteroids>()->CreatePlayer((int)GameObjectTypes::PLAYER_2, ev->GetGUID(), (float)((m_Window->getSize().x / 2) + m_PlayerScreenOffset), (float)(m_Window->getSize().y / 2));
                    sendNetworkedGameObjectID(newPlayerID, (int)GameObjectTypes::PLAYER_2, ev->GetGUID());
                    m_PlayerIDList[ev->GetGUID()] = newPlayerID;
                    m_P2GUID = ev->GetGUID();
                    break;
                default:
                    // Do nothing, this player will be a spectator
                    break;
                }

                sendUpdateMessage();

                if (m_PlayerIDList.size() == 2)
                {
                    m_StartCountdown = COUNT_DOWN_TIME;
                    sendStartCountdownMessage();
                }
            }

        }
        break;
    }
    case GameMessages::ID_PLAYER_DISCONNECTED_MESSAGE:
    {
        if ((ev->GetGUID() == m_P1GUID && !m_IsPlayer1Dead) ||
            (ev->GetGUID() == m_P2GUID && !m_IsPlayer2Dead))
        {
            m_PlayerDC = true;

            m_Game->GetNetworkObject(m_PlayerIDList[ev->GetGUID()])->Kill();
        }

        if (ev->GetGUID() == m_P1GUID)
            m_IsPlayer1Dead = true;
        else if (ev->GetGUID() == m_P2GUID)
            m_IsPlayer2Dead = true;

        m_PlayerIDList.erase(ev->GetGUID());

        if (m_PlayerIDList.size() == 0)
        {
            RestartGame();
        }

        break;
    }
    case GameMessagesEx::ID_UPDATE_PLAYER_ROTATION_MESSAGE:
    {
        UpdatePlayerRotationMessage msg = *(UpdatePlayerRotationMessage*)ev->GetMsg();

        if (m_P1GUID == ev->GetGUID())
            NetworkInterface::SendMessage(msg, m_P2GUID);
        else if (m_P2GUID == ev->GetGUID())
            NetworkInterface::SendMessage(msg, m_P1GUID);

        if (m_PlayerIDList[ev->GetGUID()] == -1)
            break;

        m_Game->GetNetworkObject(m_PlayerIDList[ev->GetGUID()])->SetBox2DRotationRads(msg.rotation);
        break;
    }
    case GameMessagesEx::ID_REMATCH_MESSAGE:
    {
        if (m_WantsRematch)
        {
            RestartGame();
            NetworkInterface::SendBroadcastId((NetworkInterface::msg_id)GameMessagesEx::ID_RESTART_GAME_MESSAGE);

            NetworkedGame::network_id newPlayerID;

            newPlayerID = GetGame<Asteroids>()->CreatePlayer((int)GameObjectTypes::PLAYER_1, m_P1GUID, (float)((m_Window->getSize().x / 2) - m_PlayerScreenOffset), (float)(m_Window->getSize().y / 2));
            sendNetworkedGameObjectID(newPlayerID, (int)GameObjectTypes::PLAYER_1, m_P1GUID);
            m_PlayerIDList[m_P1GUID] = newPlayerID;

            newPlayerID = GetGame<Asteroids>()->CreatePlayer((int)GameObjectTypes::PLAYER_2, m_P2GUID, (float)((m_Window->getSize().x / 2) + m_PlayerScreenOffset), (float)(m_Window->getSize().y / 2));
            sendNetworkedGameObjectID(newPlayerID, (int)GameObjectTypes::PLAYER_2, m_P2GUID);
            m_PlayerIDList[m_P2GUID] = newPlayerID;

            m_StartCountdown = COUNT_DOWN_TIME;
            sendStartCountdownMessage();
        }
        else
        {
            m_WantsRematch = true;

            if (m_PlayerDC)
            {
                RestartGame();
                NetworkInterface::SendBroadcastId((NetworkInterface::msg_id)GameMessagesEx::ID_RESTART_GAME_MESSAGE);

                NetworkedGame::network_id newPlayerID;

                newPlayerID = GetGame<Asteroids>()->CreatePlayer((int)GameObjectTypes::PLAYER_1, ev->GetGUID(), (float)((m_Window->getSize().x / 2) - m_PlayerScreenOffset), (float)(m_Window->getSize().y / 2));
                sendNetworkedGameObjectID(newPlayerID, (int)GameObjectTypes::PLAYER_1, ev->GetGUID());
                m_PlayerIDList[m_P1GUID] = newPlayerID;
                return;
            }

            if (m_P1GUID == ev->GetGUID())
            {
                updateRetryText("Waiting on Player 2...", sf::Vector2f(14.f, 0.f));
                NetworkInterface::SendMessageId((NetworkInterface::msg_id)GameMessagesEx::ID_REMATCH_MESSAGE, m_P2GUID);
            }
            else if (m_P2GUID == ev->GetGUID())
            {
                updateRetryText("Waiting on Player 1...", sf::Vector2f(14.f, 0.f));
                NetworkInterface::SendMessageId((NetworkInterface::msg_id)GameMessagesEx::ID_REMATCH_MESSAGE, m_P1GUID);
            }
        }
        break;
    }
    }
}

void ServerInterface::update(double deltaT)
{
    Super::update(deltaT);

    if (!m_Game->IsDeletedObjectsEmpty())
    {
        std::vector<network_id> ids = m_Game->GetDeletedObjects();
        sendMultipleDeleteMessage(ids);
        m_Game->ClearDeletedObjects();
    }

    if (m_StartCountdown > 0)
    {
        m_StartCountdown -= deltaT;
        printf("Time until start: %f\n", m_StartCountdown);

        if (m_StartCountdown <= 0)
        {
            m_StartCountdown = 0;

            sendStartMessage();
            GetGame<Asteroids>()->StartNextLevel();
        }

        updateCountdownText();
    }
}

void ServerInterface::preDraw()
{
    Super::preDraw();
}

void ServerInterface::postDraw()
{
    Super::postDraw();

    if (m_CountDownText.getString() != "" && m_CountDownText.getString() != "0.00")
        m_Window->draw(m_CountDownText);

    if (m_GameLost)
    {
        m_Window->draw(m_LoseBackground);
        m_Window->draw(m_RetryText);
        m_Window->draw(m_WinnerText);
        m_Window->draw(m_LoserText);
    }
}

void ServerInterface::sendStartMessage()
{
    NetworkInterface::SendBroadcastId((NetworkInterface::msg_id)GameMessagesEx::ID_START_GAME_MESSAGE);
}

void ServerInterface::updateCountdownText()
{
    if (m_StartCountdown > 0)
    {
        std::string text = std::to_string(m_StartCountdown);
        size_t index = text.find('.');

        text = text.substr(0, index + 3);

        m_CountDownText.setString(text);
        sf::FloatRect rect = m_CountDownText.getGlobalBounds();
        m_CountDownText.setPosition(640.f - rect.width / 2.f, 20.f);
    }
    else
    {
        m_CountDownText.setString("");
    }
}

void ServerInterface::sendStartCountdownMessage()
{
    NetworkInterface::SendBroadcastId((NetworkInterface::msg_id)GameMessagesEx::ID_START_COUNTDOWN_MESSAGE);
}

void ServerInterface::onKillPlayer(Events::Event* e)
{
    PlayerEvent* ev = (PlayerEvent*)e;

    if (ev->GetPlayer() == 1)
    {
        m_IsPlayer1Dead = true;
        m_PlayerIDList[m_P1GUID] = -1;
    }
    else if (ev->GetPlayer() == 2)
    {
        m_IsPlayer2Dead = true;
        m_PlayerIDList[m_P2GUID] = -1;
    }

    if (m_IsPlayer1Dead && m_IsPlayer2Dead)
    {
        m_GameLost = true;
        sendGameOverMessage();
        updateRetryText("Waiting on Clients...", sf::Vector2f(14.f, 0.f));
    }
}

void ServerInterface::sendGameOverMessage()
{
    GameOverMessage msg;
    msg.MsgId = (uchar)GameMessagesEx::ID_GAME_OVER_MESSAGE;
    msg.winner = GetGame<Asteroids>()->GetWinner();
    msg.winnerScore = GetGame<Asteroids>()->GetWinnerScore();
    msg.loser = GetGame<Asteroids>()->GetLoser();
    msg.loserScore = GetGame<Asteroids>()->GetLoserScore();

    updateWinnerText(msg.winner, msg.winnerScore);
    updateLoserText(msg.loser, msg.loserScore);

    NetworkInterface::SendBroadcast(msg);
}

void ServerInterface::updateRetryText(std::string text, const sf::Vector2f& offset)
{
    m_RetryText.setString(text);
    sf::FloatRect rect = m_RetryText.getGlobalBounds();
    m_RetryText.setPosition(640.f - rect.width / 2.f + offset.x, 640.f + offset.y);
}

void ServerInterface::updateLoserText(int loser, int score)
{
    std::string value = "Player " + std::to_string(loser) + " - " + std::to_string(score);
    m_LoserText.setString(value);
    sf::FloatRect rect = m_LoserText.getGlobalBounds();
    m_LoserText.setPosition(640.f - rect.width / 2.f, 172.0f);
}

void ServerInterface::updateWinnerText(int winner, int score)
{
    std::string value = "Player " + std::to_string(winner) + " - " + std::to_string(score);
    m_WinnerText.setString(value);
    sf::FloatRect rect = m_WinnerText.getGlobalBounds();
    m_WinnerText.setPosition(640.f - rect.width / 2.f, 68.0f);
}