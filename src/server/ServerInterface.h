#pragma once

#include <memory>
#include "galaxyengine/GameInterface.h"
#include <SFML/Graphics.hpp>
#include <vector>

class NetworkedGameObject;

namespace Events
{
    class Delegate;
    class Event;
}

class ServerInterface : public GameInterface
{
public:
    typedef GameInterface Super;
    typedef sf::Keyboard::Key Key;
    typedef sf::Mouse::Button Button;
    typedef Events::Event Event;
    typedef unsigned int networkId;

    const double COUNT_DOWN_TIME; // In Seconds

private:
    sf::Text m_CountDownText;
    double m_StartCountdown;

    std::map<RakNet::RakNetGUID, networkId> m_PlayerIDList;
    uint m_PlayerScreenOffset;
    RakNet::RakNetGUID m_P1GUID;
    RakNet::RakNetGUID m_P2GUID;

    Events::Delegate* m_KillPlayerDelegate;
    bool m_IsPlayer1Dead;
    bool m_IsPlayer2Dead;

    bool m_PlayerDC;

    bool m_WantsRematch;
    bool m_GameLost;
    sf::Sprite m_LoseBackground;
    sf::Text m_RetryText;
    sf::Text m_WinnerText;
    sf::Text m_LoserText;
    sf::Vector2i m_InputDirection;

private:
    void sendStartMessage();
    void sendStartCountdownMessage();
    void sendGameOverMessage();
    void updateCountdownText();
    void updateRetryText(std::string text, const sf::Vector2f& offset = sf::Vector2f(0.f, 0.f));
    void updateWinnerText(int winner, int score);
    void updateLoserText(int loser, int score);

    void onKillPlayer(Events::Event* e);

protected:
    virtual void createGame() override; // Instantiate m_Game here
    virtual void initializeTextures() override; // Setup the texture loader here
    virtual void initializeGame() override; // Initialize your game here
    virtual void loadAssets(std::string assetDir = "content/", bool secondAttempt = false) override;

    virtual void disposeGame() override; // Dispose of your game here

    virtual void checkMousePress(Button button, double deltaT) override;
    virtual void checkMouseRelease(Button button, double deltaT) override;
    virtual void checkKeyRelease(Key key, double deltaT) override;
    virtual void checkKeyPress(Key key, double deltaT) override;
    virtual void onNetworkEvent(Event* e) override;

    virtual void update(double deltaT) override;
    virtual void preDraw() override;
    virtual void postDraw() override;

public:
    ServerInterface();
    ~ServerInterface();
};